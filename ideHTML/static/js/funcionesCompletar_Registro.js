/**
* function Registro
* Obtiene los datos del formulario y los manda por ajax
* Si la respuesta es Ok redirecciona a completar-registro
* Si es respuesta incorrecta entonces manda un mensaje de error
*/
$(document).ready(function(){
  $("form#form_completar_registro").submit(function(){
    var nombre = $("#nombre").val();
    var apellidos = $("#apellidos").val();
    var institucion = $("#institucion").val();
    var semestre = $("#semestre").val();
    var expediente = $("#expediente").val();
    var correo = ""
    if(nombre=="" || apellidos=="" || institucion=="" || semestre=="" || expediente=="")
    {
      notificacion('COMPLETAR CAMPOS')
      return
    }
      notificacion("CONFIRMANDO....");
      correo = localStorage.getItem('correoRegistro')
       $.ajax({
          url: 'http://148.220.52.76:8080/registro/completar_registro',
          type: 'GET',
          datatype : "application/json",
          contentType: "application/json; charset=utf-8",
          data:{nombre:nombre, apellidos:apellidos, institucion:institucion, semestre:semestre, expediente:expediente, correo:correo, tipoUser_id:1},
          success: function(data){
              console.log(data);
            if(data.mensaje == "OK"){
              localStorage.removeItem('correoRegistro')
              window.location.href = 'login.html';
            }
            else
            {
              notificacion(data.mensaje);
            }
          },
        });


  });
});
