/**
* function buscarClaveGrupo
* Obtiene la clave del grupo a buscar
* Verificar por medio de un ajax si existe y muestra la informacion
* del grupo asociado con dicha clave
*/
function buscarClaveGrupo(){
  clave = $("#clave_Grupo").val();
  $.ajax({
     url: 'http://148.220.52.76:8080/grupos/detalles_grupo_cve',
     type: 'GET',
     data:{clave:clave},
     success: function(data){
       if(data != "NO EXISTE"){
         cont = '<div class="notif_sup"><img src="../static/img/cancel.svg" onclick="closeNoti();" alt="">'+
                 '<h1>Confirmación</h1></div><div class="notif_cont"><div class="obs_img_inscripcion"></div>'+
                 '<img src="../static/img/loginBackBlur.jpg" alt=""><h1>'+data[0][1]+'</h1>'+
                 '<h2>'+data[0][2]+' '+data[0][3]+'</h2></div><div class="noti_footer"><button onclick=\'agregar_alumno_grupo(\"'+data[0][0]+'\")\'>Confirmar</button>'+
                 '<h4 onclick="closeNoti();">Grupo Incorrecto</h4></div>';
         $("#notificaciones").html(cont);
       }else{
         notificacion("CLAVE NO REGISTRADA");
       }
     },
   });
}

/**
* function agregar_alumno_grupo
* @param idGrupo
* Registra al alumno actual al grupo
* que ingreso con posterioridad la clave
*/
function agregar_alumno_grupo(idGrupo){
    notificacion("Confirmando....");
  expediente = localStorage.getItem("expediente");
  $.ajax({
     url: 'http://148.220.52.76:8080/grupos/agregar_participante_grupo',
     type: 'GET',
     data:{expediente:expediente, grupo:idGrupo},
     success: function(data){
       if(data == "OK"){
         cargar_alumno();
         closeNoti();
         notificacion("REGISTRADO");
       }else if(data == "EXISTE"){
         notificacion("USUARIO YA EN GRUPO");
       }else{
         notificacion("NO EXISTE EL USUARIO");
       }
     },
   });
}

/**
* function cargar_alumno
* Obtiene los grupos que tiene el alumno
* Muestra dichos grupos en el template
*/
function cargar_alumno(){
  cont = '<div class="titulo_cont" id="titulo_cont"><h1>Mis Grupos</h1></div><div class="cont_interno">';
  usuario = localStorage.getItem("idUser");
  $.ajax({
     url: 'http://148.220.52.76:8080/grupos/getGrupos',
     type: 'GET',
     data:{idUsuario:usuario},
     success: function(data){
       if(data != ""){
         for(datos in data){
           cont += '<div class="ficha" id="ficha'+data[datos][0]+'">'+
               '<h1 onclick=\'verTareasGrupo(\"pendiente\",\"'+usuario+'\",\"'+data[datos][0]+'\")\'>'+data[datos][1]+'</h1><h2>'+data[datos][4]+'</h2><div class="footer_ficha"><div class="tareas" onclick=\'verTareasGrupo(\"pendiente\",\"'+usuario+'\",\"'+data[datos][0]+'\")\'>'+
               '<img src="../static/img/alert.svg" alt=""><h1>'+data[datos][2]+' Tareas Pendientes</h1></div>'+
               '<div class="tareas" onclick=\'verTareasGrupo(\"curso\",\"'+usuario+'\",\"'+data[datos][0]+'\")\'>'+
               '<img src="../static/img/alert.svg" alt=""><h1>'+data[datos][3]+' Tareas En Curso</h1></div></div>'+
               '<img src="../static/img/cancel.svg" class="salir_ico" onclick=\'eliminar_alumno_grupo(\"'+usuario+'\",\"'+data[datos][0]+'\")\'><div class="notif_salir">'+
               '<div class="triangle"></div><h1>Salir del Grupo</h1></div></div></div>';
         }
       }else{
         cont += '<div style="height: 300px;"></div>';
       }
       cont += '</div>';
       $("#contenido").html(cont);
     },
   });
   $("#contenido").html(cont);
}

/**
* function eliminar_alumno_grupo
* @param idAlumno
* @param idGrupo
* Elimina a un alumno de un grupo en especifico
* el ser eliminado tambien elimina el grupo del template
*/
function eliminar_alumno_grupo(idAlumno, idGrupo){
    notificacion("Eliminando...");
  $.ajax({
     url: 'http://148.220.52.76:8080/grupos/eliminar_alumno_grupo',
     type: 'GET',
     data:{id_grupo:idGrupo, id_alumno:idAlumno},
     success: function(data){
      if(data == "OK"){
        //cargar_alumno();
        $("#ficha"+idGrupo).css("display","none");
      }else{
        notificacion("Error Eliminar Alumno");
      }
     },
   });
}

/**
* function tareas_alumno
* @param tipo
* Obtiene el tipo de tarea si es pendiente o ya entregada
* Muestra las tareas pendientes y entregadas que obtiene de un ajax
*/
function tareas_alumno(tipo){
  if(tipo == "pendiente"){
      cont = '<div class="subtitle_cont"><h4 class="ico_activo" onclick=\'tareas_alumno(\"pendiente\")\'>Pendientes</h4>'+
          '<h4 onclick=\'tareas_alumno(\"curso\")\'>En Curso</h4>'+
          '<h4 onclick=\'tareas_alumno(\"calificado\")\'>Entregadas</h4></div>';
  }else if(tipo == "calificado"){
      cont = '<div class="subtitle_cont"><h4 onclick=\'tareas_alumno(\"pendiente\")\'>Pendientes</h4>'+
          '<h4 onclick=\'tareas_alumno(\"curso\")\'>En Curso</h4>'+
          '<h4 class="ico_activo" onclick=\'tareas_alumno(\"calificado\")\'>Entregadas</h4></div>';
  }else if(tipo == "curso"){
      cont = '<div class="subtitle_cont"><h4 onclick=\'tareas_alumno(\"pendiente\")\'>Pendientes</h4>'+
          '<h4 class="ico_activo" onclick=\'tareas_alumno(\"curso\")\'>En Curso</h4>'+
          '<h4 onclick=\'tareas_alumno(\"calificado\")\'>Entregadas</h4></div>';
  }
  cont += '<div class="conte_tareas_alum" id="conte_tareas_alum">';
  usuario = localStorage.getItem("idUser");
  $.ajax({
     url: 'http://148.220.52.76:8080/tareas/getTareasByAlumno',
     type: 'GET',
     data:{idAlumno:usuario},
     success: function(data){
       contador = 0;
       for(datos in data){
         if(tipo == "pendiente"){
           if( data[datos][1] == "pendiente"){
             contador++;
             cont += '<div class="tarea_alumno">'+
             '<h1>'+data[datos][2]+'</h1><h2>'+data[datos][3]+'</h2>'+
             '<img src="../static/img/next.svg" onclick=\'realizarTarea(\"'+data[datos][0]+'\", \"'+data[datos][3]+'\", \"'+tipo+'\", \"todas\", \"'+usuario+'\", \"'+data[datos][4]+'\")\'></div>';
           }
         }
         if(tipo == "calificado"){
           if( data[datos][1] == "calificado"){
             contador++;
             cont += '<div class="tarea_alumno">'+
             '<h1>'+data[datos][2]+'</h1><h2>'+data[datos][3]+'</h2>'+
             '<img src="../static/img/next.svg" onclick=\'realizarTarea(\"'+data[datos][0]+'\", \"'+data[datos][3]+'\", \"'+tipo+'\", \"todas\", \"'+usuario+'\", \"'+data[datos][4]+'\")\'></div>';
           }
         }
        if(tipo == "curso"){
           if( data[datos][1] == "curso"){
             contador++;
             cont += '<div class="tarea_alumno">'+
             '<h1>'+data[datos][2]+'</h1><h2>'+data[datos][3]+'</h2>'+
             '<img src="../static/img/next.svg" onclick=\'realizarTarea(\"'+data[datos][0]+'\", \"'+data[datos][3]+'\", \"'+tipo+'\", \"todas\", \"'+usuario+'\", \"'+data[datos][4]+'\")\'></div>';
           }
         }
       }
       if(contador == 0){
         cont += '<div style="height: 150px;"></div>';
       }
       cont += '<div style="height: 50px;"></div>';
       cont += '</div>';
       $("#contenido").html(cont);
     },
   });
}

/**
* function tareas_alumno
* @param tipo
* @param idAlumno
* @param idGrupo
* Obtiene el tipo de tarea si es pendiente o ya entregada por grupos
* Muestra las tareas pendientes y entregadas que obtiene de un ajax por grupos
*/
function verTareasGrupo(tipo, idAlumno, idGrupo){
  if(tipo == "pendiente"){
      cont = '<div class="subtitle_cont"><h4 class="ico_activo" onclick=\'verTareasGrupo(\"pendiente\",\"'+idAlumno+'\",\"'+idGrupo+'\")\'>Pendientes</h4>'+
          '<h4 onclick=\'verTareasGrupo(\"curso\",\"'+idAlumno+'\",\"'+idGrupo+'\")\'>En Curso</h4>'+
          '<h4 onclick=\'verTareasGrupo(\"calificado\",\"'+idAlumno+'\",\"'+idGrupo+'\")\'>Entregadas</h4></div>';
  }else if(tipo == "calificado"){
      cont = '<div class="subtitle_cont"><h4 onclick=\'verTareasGrupo(\"pendiente\",\"'+idAlumno+'\",\"'+idGrupo+'\")\'>Pendientes</h4>'+
          '<h4 onclick=\'verTareasGrupo(\"curso\",\"'+idAlumno+'\",\"'+idGrupo+'\")\'>En Curso</h4>'+
          '<h4 class="ico_activo" onclick=\'verTareasGrupo(\"calificado\",\"'+idAlumno+'\",\"'+idGrupo+'\")\'>Entregadas</h4></div>';
  }else if(tipo == "curso"){
           cont = '<div class="subtitle_cont"><h4 onclick=\'verTareasGrupo(\"pendiente\",\"'+idAlumno+'\",\"'+idGrupo+'\")\'>Pendientes</h4>'+
          '<h4 class="ico_activo" onclick=\'verTareasGrupo(\"curso\",\"'+idAlumno+'\",\"'+idGrupo+'\")\'>En Curso</h4>'+
          '<h4 onclick=\'verTareasGrupo(\"calificado\",\"'+idAlumno+'\",\"'+idGrupo+'\")\'>Entregadas</h4></div>';
  }
  cont += '<div class="conte_tareas_alum" id="conte_tareas_alum">';
  $.ajax({
     url: 'http://148.220.52.76:8080/tareas/get_tareas_alumno_grupo',
     type: 'GET',
     data:{id_grupo:idGrupo, id_alumno:idAlumno},
     success: function(data){
       contador = 0;
       for(datos in data){
         if(tipo == "pendiente"){
           if( data[datos][1] == "pendiente"){
             contador++;
             cont += '<div class="tarea_alumno">'+
             '<h1>'+data[datos][2]+'</h1><h2>'+data[datos][3]+'</h2>'+
             '<img src="../static/img/next.svg" onclick=\'realizarTarea(\"'+data[datos][0]+'\", \"'+data[datos][3]+'\", \"'+tipo+'\", \"grupos\", \"'+idAlumno+'\", \"'+idGrupo+'\")\'></div>';
           }
         }
         if(tipo == "calificado"){
           if( data[datos][1] == "calificado"){
             contador++;
             cont += '<div class="tarea_alumno">'+
             '<h1>'+data[datos][2]+'</h1><h2>'+data[datos][3]+'</h2>'+
             '<img src="../static/img/next.svg" onclick=\'realizarTarea(\"'+data[datos][0]+'\", \"'+data[datos][3]+'\", \"'+tipo+'\", \"grupos\", \"'+idAlumno+'\", \"'+idGrupo+'\")\'></div>';
           }
         }
        if(tipo == "curso"){
           if( data[datos][1] == "curso"){
             contador++;
             cont += '<div class="tarea_alumno">'+
             '<h1>'+data[datos][2]+'</h1><h2>'+data[datos][3]+'</h2>'+
             '<img src="../static/img/next.svg" onclick=\'realizarTarea(\"'+data[datos][0]+'\", \"'+data[datos][3]+'\", \"'+tipo+'\", \"grupos\", \"'+idAlumno+'\", \"'+idGrupo+'\")\'></div>';
           }
         }
       }
       if(contador == 0){
         cont += '<div style="height: 150px;"></div>';
       }
       cont += '<div style="height: 50px;"></div>';
       cont += '</div>';
       $("#contenido").html(cont);
     },
   });
}

/**
* function realizarTarea
* @param idTarea
* @param nombreGrupo
* @param tipo
* @param accion
* @param idAlumno
* @param idGrupo
* Muestra los detalles de las tareas y dependiendo del tipo
* de tarea ("calificado, pendiente") muestra un boton
* para realizar la tarea o ver sus resultados
* los dos ultimos parametros solo son para las tareas de grupos especificos
*/
function realizarTarea(idTarea, nombreGrupo, tipo, accion, idAlumno, idGrupo){
  if(accion == "todas"){
      cont = '<div class="subtitle_cont"><img src="../static/img/next.svg" onclick=\'tareas_alumno(\"pendiente\")\'><h3>Detalles</h3></div><div class="detalles_tarea">';
  }else{
    cont = '<div class="subtitle_cont"><img src="../static/img/next.svg" onclick=\'verTareasGrupo(\"pendiente\",\"'+idAlumno+'\",\"'+idGrupo+'\")\'><h3>Detalles</h3></div><div class="detalles_tarea">';
  }
  $.ajax({
     url: 'http://148.220.52.76:8080/tareas/detalles_tarea',
     type: 'GET',
     data:{idTarea:idTarea, idGrupo:idGrupo},
     success: function(data){
       aux_nom = "";
      for(datos in data){
        aux_nom = data[datos][1];
        cont += '<h1>'+data[datos][1]+'</h1><h3>'+nombreGrupo+'</h3><h4>Fecha Limite:  '+data[datos][3]+'</h4>'+
          '<div class="descr_aux">'+data[datos][2]+'</div><h5>Archivos adjuntos: </h5>';
        cont += '<div class="cont_tareas_archivos">';
        for (archivo in data[datos][4]){
          flag = data[datos][4][archivo];
          cont += '<a href="http://148.220.52.76:8080/static/grupos/'+idGrupo+'/'+idTarea+'/'+flag+'" download><div class="tarea_archivo"><h1>'+flag+'</h1></div></a>';
        }
        cont += '</div>';
      }
      if(tipo == "pendiente"){
        cont += '</div><div class="footer_detalle_tarea"><button onclick=\'mandarDatosIde(\"'+idTarea+'\",\"'+idGrupo+'\");\'>Realizar</button></div>';
      }else if(tipo == "calificado"){
        cont += '</div><div class="footer_detalle_tarea"><button onclick=\'mostrarCalificacionTarea(\"'+idTarea+'\",\"'+nombreGrupo+'\",\"'+tipo+'\",\"'+accion+'\",\"'+idAlumno+'\",\"'+idGrupo+'\", \"'+aux_nom+'\")\'>Ver Resultados</button></div>';
      }else if(tipo == "curso"){
        cont += '</div><div class="footer_detalle_tarea"><button onclick=\'mandarDatosIde(\"'+idTarea+'\",\"'+idGrupo+'\");\'>Reanudar</button></div>';
      }
      $("#contenido").html(cont);
     },
   });

}

function mandarDatosIde(idTarea, idGrupo){
  idUsuario = localStorage.getItem("idUser");
  tema = localStorage.getItem("ide");
  url = 'http://148.220.52.76:8080/api/createproject/'+idUsuario+'/'+idGrupo+'/'+idTarea;
  $.ajax({
     url: url,
     type: 'GET',
     success: function(data){
       url2 = "http://148.220.52.76:8080/ide/main?idUsuario="+idUsuario+"&idGrupo="+idGrupo+"&idTarea="+idTarea+"&idTema="+tema;
       window.open(url2);
     },
   });
}

function mostrarCalificacionTarea(idTarea, nombreGrupo, tipo, accion, idAlumno, idGrupo, nombreTarea){
    cont = '<div class="superior_resultados_tarea"><img onclick=\'realizarTarea(\"'+idTarea+'\",\"'+nombreGrupo+'\",\"'+tipo+'\",\"'+accion+'\",\"'+idAlumno+'\",\"'+idGrupo+'\")\' src="../static/img/next.svg" alt=""><h1>'+nombreTarea+'</h1></div>';
    //ajax obtener tarea detalles
    $.ajax({
       url: 'http://148.220.52.76:8080/tareas/mostrarCalificacionTarea',
       type: 'GET',
       data:{idTarea:idTarea, idGrupo:idGrupo, idAlumno:idAlumno},
       success: function(data){
         //alert(data.metricas[0].categoria)
         cont += '<div class="inferior_resultados_tarea"><h2>Detalles de la Entrega</h2><h3>Fecha de entrega: '+data.calificacionIndividual.fecha_entrega+'</h3>'+
             '<h4>Calificación Tarea: '+data.calificacionIndividual.calificacion+'</h4><h2>Observaciones</h2><p>'+data.calificacionIndividual.observaciones+'</p><div class="tabla_metrica">'+
                 '<table><caption>Codificación</caption><tr><th class="tabla_uno">Métrica</th><th class="tabla_dos">Resultado Individual</th>'+
                     '<th class="tabla_uno">Indicador Alto</th><th class="tabla_dos">Indicador Bajo</th></tr>';

         for(i=0;i<=15;i++){
             cont += '<tr><td class="tabla_uno">'+data.calificacionMetricas[i][2]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][9]+'</td><td class="tabla_uno">'+data.calificacionMetricas[i][4]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][5]+'</td></tr>';
         }

           cont += '</table></div><div class="tabla_metrica"><table><caption>Ejecución</caption><tr>'+
                           '<th class="tabla_uno">Métrica</th><th class="tabla_dos">Resultado Individual</th><th class="tabla_uno">Indicador Alto</th><th class="tabla_dos">Indicador Bajo</th></tr>';

           for(i=16;i<=30;i++){
               cont += '<tr><td class="tabla_uno">'+data.calificacionMetricas[i][2]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][9]+'</td><td class="tabla_uno">'+data.calificacionMetricas[i][4]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][5]+'</td></tr>';
           }
           cont += '</table></div><div class="tabla_metrica"><table><caption>Compilación</caption><tr>'+
                           '<th class="tabla_uno">Métrica</th><th class="tabla_dos">Resultado Individual</th><th class="tabla_uno">Indicador Alto</th><th class="tabla_dos">Indicador Bajo</th></tr>';
           for(i=31;i<=33;i++){
                   cont += '<tr><td class="tabla_uno">'+data.calificacionMetricas[i][2]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][9]+'</td><td class="tabla_uno">'+data.calificacionMetricas[i][4]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][5]+'</td></tr>';
               }
           cont += '</div>';
       $("#contenido").html(cont);
       },
     });
     //fin ajax
}
