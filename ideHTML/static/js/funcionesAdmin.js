function addMaestro(){
    cont = '<div class="notif_sup"><img src="../static/img/cancel.svg" onclick="closeNoti();" alt="">'+
            '<h1>Nuevo Maestro</h1></div><div class="notif_cont">'+
                '<input type="text" id="nombreMaestro" placeholder="Nombre(s)">'+
                '<input type="text" id="apellidoMaestro" placeholder="Apellidos">'+
                '<input type="email" id="emailMaestro" placeholder="Correo Electronico">'+
                '<input type="text" id="expedienteMaestro" placeholder="Expediente">'+
                '<input type="password" id="passwordMaestro" placeholder="Contraseña Provisional">'+
                '<input type="password" id="repasswordMaestro" placeholder="Repetir Contraseña Provisional">'+
            '</div><div class="noti_footer">'+
            '<button onclick="agregarMaestro();">Confirmar</button><h4 onclick="addMaestroCorreo();">¿Maestro Ya Registrado?</h4></div>';
    $("#notificaciones").html(cont);
    desplegarNoti();
}

function addMaestroCorreo(){
    cont = '<div class="notif_sup"><img src="../static/img/cancel.svg" onclick="closeNoti();" alt="">'+
            '<h1>Agregar Maestro Registrado</h1></div><div class="notif_cont">'+
                '<input type="email" id="correoMaestro" placeholder="Email Maestro">'+
            '</div><div class="noti_footer">'+
            '<button onclick="agregarMaestroCorreo();">Confirmar</button><h4 onclick="closeNoti();">cancelar</h4></div>';
    $("#notificaciones").html(cont);
    desplegarNoti();
}

function agregarMaestroCorreo(){
    notificacion("PROCESANDO...");
    email = $("#correoMaestro").val();
    if(email != ""){
        $.ajax({
           url: 'http://148.220.52.76:8080/maestro/registrar_maestro_correo',
           type: 'GET',
           data: {email:email},
           success: function(data){
               if(data == "OK"){
                    closeNoti();
                    maestrosView();
                }else{
                    notificacion("NO EXISTE EL USUARIO");
                }
           },
         });

   }else{
       notificacion("COMPLETAR CAMPOS");
   }
}

function agregarMaestro(){
    nombre = $("#nombreMaestro").val();
    apellido = $("#apellidoMaestro").val();
    email = $("#emailMaestro").val();
    expediente = $("#expedienteMaestro").val();
    password = $("#passwordMaestro").val();
    repassword = $("#repasswordMaestro").val();
    if(nombre != "" && apellido != "" && email != "" && expediente !=  "" && password != "" && repassword != ""){
        if(password == repassword){
            notificacion("PROCESANDO....");
            $.ajax({
               url: 'http://148.220.52.76:8080/maestro/registrar_maestro',
               type: 'GET',
               data: {nombre:nombre, apellido:apellido, email:email, expediente:expediente, password:password},
               success: function(data){
                   if(data == "OK"){
                        closeNoti();
                        maestrosView();
                    }else{
                        notificacion("USUARIO EXISTENTE");
                    }
               },
             });
       }else{
           notificacion("PASSWORD NO COINCIDE");
       }

   }else{
       notificacion("COMPLETAR CAMPOS");
   }
}

function maestrosView(){
    cont = '<div class="superior_maestro_admin"><h1>Maestros</h1>'+
            '<div onclick="addMaestro();"><img src="../static/img/add.svg"><h3>Agregar</h3></div></div>';
    $.ajax({
       url: 'http://148.220.52.76:8080/maestro/obtener_maestros',
       type: 'GET',
       success: function(data){
         for(datos in data){
             cont += '<div class="maestros_grupo"><div class="miembro"><img src="http://148.220.52.76:8080/static/perfil/'+data[datos][4]+'/user.jpg"'+
                 'class="fotoPerfil"><h1>'+data[datos][1]+'</h1>'+
            '<h4>'+data[datos][2]+'</h4><h5>'+data[datos][3]+'</h5><img onclick=\'cambiarMaestro(\"'+data[datos][0]+'\")\'" src="../static/img/cancel.svg" class="del"></div></div>';
         }
        $("#contenido").css("display","block");
        $("#contenido").html(cont);
       },
     });
}

function cambiarMaestro(idMaestro){
    $.ajax({
       url: 'http://148.220.52.76:8080/maestro/cambiar_maestros',
       type: 'GET',
       data: {idMaestro:idMaestro},
       success: function(data){
           if(data == "OK"){
              maestrosView();
            }else{
                notificacion("ERROR");
            }
       },
     });
}
