var gapi
var estadoSesion = false
function handleClientLoad() {
  // Loads the client library and the auth2 library together for efficiency.
  // Loading the auth2 library is optional here since `gapi.client.init` function will load
  // it if not already loaded. Loading it upfront can save one network request.
  gapi.load('client:auth2', initClient);
}

function initClient() {
  // Initialize the client with API key and People API, and initialize OAuth with an
  // OAuth 2.0 client ID and scopes (space delimited string) to request access.
  gapi.client.init({
      apiKey: 'AIzaSyBdqysULGtkt-opP6XR8R8p1AIendC-UnI',
      discoveryDocs: ["https://people.googleapis.com/$discovery/rest?version=v1"],
      clientId: '831518437693-cql6p91gdgioafnjs4lnftl986gbadq6.apps.googleusercontent.com',
      scope: 'profile'
  })
}


function updateSigninStatus(isSignedIn) {
  // When signin status changes, this function is called.
  // If the signin status is changed to signedIn, we make an API call.
  if (isSignedIn) {
    makeApiCall();
  }
  else {
    localStorage.removeItem('estadoSesionGmail')
    localStorage.removeItem('correoRegistro')
    localStorage.removeItem('estadoSesionFace')
    localStorage.removeItem('correoSesion')
    localStorage.removeItem('idUser')
    localStorage.removeItem('tipo')
    localStorage.removeItem('nombre')
    localStorage.removeItem('tipoLogin')
    window.location.href = 'login.html';
  }
}

function handleSignIn(event) {
  // Ideally the button should only show up after gapi.client.init finishes, so that this
  // handler won't be called before OAuth is initialized.
  gapi.auth2.getAuthInstance().signIn();
  updateSigninStatus(true)
  //alert(gapi.auth2.getAuthInstance().isSignedIn.get());
}

function handleSignOut(event) {
  gapi.auth2.getAuthInstance().signOut();
  updateSigninStatus(false)
}


function makeApiCall() {

  auth2 = gapi.auth2.getAuthInstance();
  if (auth2.isSignedIn.get()) {
  var profile = auth2.currentUser.get().getBasicProfile();

  localStorage.setItem('estadoSesionGmail' , 'connected');
  localStorage.setItem('correoRegistro' , profile.getEmail());
  return
  }
}



  function verificarRegistroGmail()
  {
    //verificar si el correo esta registrado en la base de datos
    $.ajax({
      url: 'http://148.220.52.76:8080/login/redSocial',
      type: 'GET',
      data:{email:localStorage.getItem('correoRegistro')},
      success: function(data){
        if(data.mensaje == "Acceso"){ //si ya esta registado, iniciar sesion
          //redirigir a la pantalla principal
          localStorage.setItem('correoSesion' , localStorage.getItem('correoRegistro'))
          localStorage.removeItem('correoRegistro')
          localStorage.setItem('idUser', data.idUser)
          localStorage.setItem('correoSesion', data.correo)
          localStorage.setItem('tipo', data.tipo)
          localStorage.setItem('nombre', data.nombre)
          localStorage.setItem('expediente', data.expediente)
          localStorage.setItem('tipoLogin', 'gmail')
          if(data.tipo == "estudiante")
          {
            window.location.href = 'inicio_alumno.html';
          }
          if(data.tipo == "maestro")
          {
            window.location.href = 'inicio_maestro.html';
          }
          if(data.tipo == "administrador")
          {
            window.location.href = 'inicio_admin.html';
          }
        }
        else //si no esta registrado, terminar el registro
        {
          //registrar el correo en la tabla auth_user
          $.ajax({
             url: 'http://148.220.52.76:8080/registro/normal',
             type: 'GET',
             datatype : "application/json",
             contentType: "application/json; charset=utf-8",
             data:{password:'', email: localStorage.getItem('correoRegistro')},
             success: function(data){
               if(data.mensaje == "Ok"){
                window.location.href = 'completar_registro.html';
               }else{
                 notificacion("DATOS INCORRECTOS");
               }
             },
           });
        }
      },
    });
  }
