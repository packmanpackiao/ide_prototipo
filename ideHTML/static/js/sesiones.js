// Funcion para control de sesiones
$( document ).ready(function() {
  var url = $(this).attr('title');
  if(localStorage.getItem('tipo') == null){
    // var tipo=localStorage.getItem('tipo');
    window.location.href = 'login.html';
  }
  else{
    // window.location.href = 'login.html';
    var tipo=localStorage.getItem('tipo');
  }
  if(url == 'Inicio Alumno' && tipo == 'maestro'){
    window.location.href = 'inicio_maestro.html';
  }else if (url == 'Inicio Maestro' && tipo == 'estudiante') {
    window.location.href = 'inicio_alumno.html';
  }else if (url == 'Inicio Alumno' && tipo == 'admin') {
    window.location.href = 'inicio_admin.html';
  }
});
