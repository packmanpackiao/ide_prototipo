document.write("<script type='text/javascript' src='../static/js/facebookLogin.js'></script>");

$(function(){
    $("#btn_show_registro").click(function(){
        $("#info_cont").css("float","right");
        $("#cont_registro").css("display","block");
        $("#cont_login").css("display","none");
        $("#btn_show_registro").css("display","none");
        $("#btn_show_login").css("display","inline");
    });

    $("#btn_show_login").click(function(){
        $("#info_cont").css("float","left");
        $("#cont_registro").css("display","none");
        $("#cont_login").css("display","block");
        $("#btn_show_registro").css("display","inline");
        $("#btn_show_login").css("display","none");
    });
});

/**
* function Registro
* Obtiene los datos del formulario y los manda por ajax
* Si la respuesta es Ok redirecciona a completar-registro
* Si es respuesta incorrecta entonces manda un mensaje de error
*/
$(document).ready(function(){
  $("form#registro").submit(function(){
    var password = $("#reg_pass").val();
    var email = $("#reg_email").val();
    if(password != "" && email != ""){
      if(password != "" && email != ""){
       $.ajax({
          url: 'http://148.220.52.76:8080/registro/normal',
          type: 'GET',
          datatype : "application/json",
          contentType: "application/json; charset=utf-8",
          data:{password:password, email:email},
          success: function(data){
            if(data.mensaje == "Ok"){
              localStorage.setItem('correoRegistro' , email);
              window.location.href = 'completar_registro.html';
            }else{
              notificacion(data.mensaje);
            }
          },
        });
      }else{
        notificacion("Correo no Valido");
      }
      }else{
      notificacion("Completar Datos");
    }
  });
});

/**
* function Login
* Obtiene los datos del formulario y los manda por ajax
* Si la respuesta es Acceso redirecciona al index
* Si es respuesta incorrecta entonces manda un mensaje de usuario o contraseña incorrectos
*/
$(document).ready(function(){
  $("form#login").submit(function(){
    var password = $("#log_pass").val();
    var email = $("#log_email").val();
    if(password != "" && email != ""){
      if(password != "" && email != ""){
        $.ajax({
          url: 'http://148.220.52.76:8080/login/normal',
          type: 'GET',
          //data:{password:password, email:email, csrfmiddlewaretoken: token},
          data:{password:password, email:email},
          success: function(data){
            if(data.mensaje == "Acceso"){
              localStorage.setItem('idUser', data.idUser)
              localStorage.setItem('correoSesion', data.correo)
              localStorage.setItem('tipo', data.tipo)
              localStorage.setItem('nombre', data.nombre)
              localStorage.setItem('apellidos', data.apellidos)
              localStorage.setItem('expediente' , data.expediente)
              localStorage.setItem('ide' , data.tema)
              localStorage.setItem('tipoLogin', 'normal')
              if(data.tipo == "estudiante")
              {
                window.location.href = 'inicio_alumno.html';
              }
              if(data.tipo == "maestro")
              {
                window.location.href = 'inicio_maestro.html';
              }
              if(data.tipo == "admin")
              {
                window.location.href = 'inicio_admin.html';
              }

            }else{
              notificacion("DATOS INCORRECTOS");
            }
          },
        });
      }else{
        notificacion("CORREO NO VALIDO");
      }
      }else{
      notificacion("COMPLETAR DATOS");
    }
  });
});


/**
* function loginFacebook
* autentifica si esta logeado a facebook
* verifica si ya esta registrado
* si esta registrado, redirigir a la pagina principal
* si no esta registrado, completar el registro
*/
$(document).ready(function(){
  $(".face").click(function(){
    comprobar()
    //si ya esta conectado con su cuenta de facebook
    if(localStorage.getItem('estadoSesionFace') == 'connected')
    {
      verificarRegistroFace()
    }//si no esta conectado con su cuenta de facebook
    else if(localStorage.getItem('estadoSesionFace') =='not-connected')
    {
      conectar()
      if(localStorage.getItem('estadoSesionFace') == 'conection-refused')
      {
        notificacion('INICIO SESION CANCELADO')
      }
      else if (localStorage.getItem('estadoSesionFace') == 'connected') {
        verificarRegistroFace()
      }

    }
  });
});


$(document).ready(function(){
  $(".google").click(function(){
    gapi.auth2.getAuthInstance().signIn();
    updateSigninStatus(true)
    //si ya esta conectado con su cuenta de facebook
    if(localStorage.getItem('estadoSesionGmail') == 'connected')
    {
      verificarRegistroGmail()
    }//si no esta conectado con su cuenta de facebook
    else if(localStorage.getItem('estadoSesionGmail') =='not-connected')
    {
      handleSignIn()

      if (localStorage.getItem('estadoSesionGmail') == 'connected') {
        verificarRegistroGmail()
      }

    }
  });
});




/**
* cerrar sesiones
*/
$(document).ready(function(){
  $("#logoutface").click(function(){
      cerrarSesionFacebook()
  });
});

$(document).ready(function(){
  $("#logoutgmail").click(function(){
      handleSignOut()
  });
});
