
//cargar el SDK de facebook
var FB;
window.fbAsyncInit = function() {
   FB.init({
     appId            : '1288337874617002',
     autoLogAppEvents : true,
     xfbml            : true,
     version          : 'v2.9'
   });
   FB.AppEvents.logPageView();
 };

 (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
//fin cargar el SDK de facebook


function comprobar()
{
  //obtener el estatus del login
   FB.getLoginStatus(function(response) {
     //console.log(response)
     if (response.status === 'connected')
     {
       localStorage.setItem('estadoSesionFace' , 'connected');
       return
     }
     else
     {
       localStorage.setItem('estadoSesionFace' , 'not-connected');
       return
     }
 });

}


function conectar()
{
  FB.login(function(response){
    if(response.status == 'connected')
    {
      //get nombre de facebook
      //get nombre correo de facebook
    FB.api('/me', { locale: 'es_MX', fields: 'name, email' },
      function(response) {
        console.log('email: '+response.email);
        localStorage.setItem('estadoSesionFace' , 'connected');
        localStorage.setItem('correoRegistro' , response.email);
        return
      }
    );

    }
    else
    {
      localStorage.setItem('estadoSesionFace' , 'conection-refused');
      return
    }
  }, {scope: 'email', return_scopes:true});
}


function getCorreoFacebook()
{
      FB.api('/me', { locale: 'es_MX', fields: 'name, email' },
        function(response){
          console.log('email: '+response.email);
        });
  localStorage.setItem('correoRegistro' , response.email);
  return
}

function cerrarSesionFacebook()
{
 FB.logout(function(response){
   localStorage.clear();
   window.location.href = 'login.html';
 });

}



function verificarRegistroFace()
{
  localStorage.getItem('correoRegistro')
  //verificar si el correo esta registrado en la base de datos
  $.ajax({
    url: 'http://148.220.52.76:8080/login/redSocial',
    type: 'GET',
    data:{email:localStorage.getItem('correoRegistro')},
    success: function(data){
      if(data.mensaje == "Acceso"){ //si ya esta registado, iniciar sesion
        //redirigir a la pantalla principal
        localStorage.setItem('correoSesion' , localStorage.getItem('correoRegistro'))
        localStorage.removeItem('correoRegistro')
        localStorage.setItem('idUser', data.idUser)
        localStorage.setItem('correoSesion', data.correo)
        localStorage.setItem('tipo', data.tipo)
        localStorage.setItem('nombre', data.nombre)
        localStorage.setItem('expediente', data.expediente)

        if(data.tipo == "estudiante")
        {
          //window.location.href = 'inicio_alumno.html';
        }
        if(data.tipo == "maestro")
        {
          //window.location.href = 'inicio_maestro.html';
        }
        if(data.tipo == "administrador")
        {
          //window.location.href = 'inicio_admin.html';
        }
      }
      else //si no esta registrado, terminar el registro
      {
        //registrar el correo en la tabla auth_user
        $.ajax({
           url: 'http://148.220.52.76:8080/registro/normal',
           type: 'GET',
           datatype : "application/json",
           contentType: "application/json; charset=utf-8",
           data:{password:'', email: localStorage.getItem('correoRegistro')},
           success: function(data){
             if(data.mensaje == "Ok"){
              window.location.href = 'completar_registro.html';
             }else{
               notificacion("Datos Incorrectos");
             }
           },
         });
      }
    },
  });
}
