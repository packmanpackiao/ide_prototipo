/**
* function cargarDatosAlumno
* Cargar los datos del usuario
* Los obtiene del local storage
*/
function cargarDatosUsuario(){
  expediente = localStorage.getItem('expediente');
  correo = localStorage.getItem('correoSesion');
  nombre = localStorage.getItem('nombre');
  apellido = localStorage.getItem('apellidos')
  con = '<img src="http://148.220.52.76:8080/static/perfil/'+correo+'/user.jpg" alt=""><h2>'+nombre+'</h2><h1>'+apellido+'</h1><h5>Expediente '+expediente+'</h5>';
  $("#datos_Usuario").html(con);
  $("#fotoPerfilM").attr("src","http://148.220.52.76:8080/static/perfil/"+correo+"/user.jpg");
}

$(function(){
  /**
  * Funcion para desplegar
  * u ocultar el menu de configuracion
  */
    $("#menu").click(function(){
        menu = document.getElementById("menu_desp");
        if(menu.style.display === "block"){
          $("#menu_desp").css("display","none");
        }else{
          $("#menu_desp").css("display","block");
        }
    });

    $("#generalGrupo").click(function(){
        generalGrupojs();
    });

    /**
    * Funcion que despliega en la notificacion
    * el formulario para ingresar la clave
    * del grupo a inscribirse
    */
    $("#unir_grupo").click(function(){
        cont = '<div class="notif_sup"><img src="../static/img/cancel.svg" onclick="closeNoti();" alt="">'+
            '<h1>Nueva Inscripción</h1></div><div class="notif_cont">'+
            '<input type="text" id="clave_Grupo" placeholder="Código de clase"></div><div class="noti_footer">'+
            '<button onclick="buscarClaveGrupo();">Buscar</button></div>';
        $("#notificaciones").html(cont);
        desplegarNoti();
    });
});


function cargarAdmin(){
    id_usuario = localStorage.getItem("idUser");
    cont_abajo = "";
    cont_izq = "";
    $.ajax({
       url: 'http://148.220.52.76:8080/usuario/obtener_datos_usuario',
       type: 'GET',
       data:{id_usuario:id_usuario},
       success: function(data){
         for(datos in data){
           cont_izq = '<img id="img_perfil_com" src="http://148.220.52.76:8080/static/perfil/'+data[datos][1]+'/user.jpg" alt="">'+
             '<div id="cont_edit_img" onclick="activarInput();"><img src="../static/img/paint-brush.svg"></div><input style="display: none;" type="file" id="perfil_img"><h1>'+data[datos][2]+'</h1>'+
             '<h2>'+data[datos][3]+'</h2>'+
                '<h3>Institución:</h3><h4>'+data[datos][4]+'</h4><h3>Correo Electrónico:</h3>'+
                '<h4>'+data[datos][1]+'</h4><h5>Expediente:</h5><h5>Semestre:</h5><h6>'+data[datos][5]+'</h6><h6>'+data[datos][6]+'</h6>';
            cont_abajo += '<div class="titulo_conf"><h1>Configuración</h1></div><div class="conf_perfil"><h1>Perfil</h1>'+
            '<h2>Intitución: </h2><select name="institucion" id=""><option value="Universidad Autonoma De Queretaro">'+data[datos][4]+'</option></select>'+
            '<h2>Nombre(s): </h2><input id="nombre" type="text" value="'+data[datos][2]+'">'+
            '<h2>Apellidos: </h2><input id="apellidos" type="text" value="'+data[datos][3]+'">'+
            '<h2>Semestre: </h2><select id="semestre" name="semestre" id="">'+
            '<option value="sem">Semestre</option>'+
            '<option value="Octavo">Octavo</option>'+
            '<option value="Septimo">Septimo</option>'+
            '<option value="Sexto">Sexto</option>'+
            '<option value="Quinto">Quinto</option>'+
            '<option value="Cuarto">Cuarto</option>'+
            '<option value="Tercero">Tercero</option>'+
            '<option value="Segundo">Segundo</option>'+
            '<option value="Primero">Primero</option></select>'+
            '<h2>Password: </h2><input id="password" type="password" value="********"></div><div class="conf_perfil_ide">'+
           '<h1>IDE</h1><h2>Tema: </h2><select name="tema" id="">'+
           '<option value="tema">Tema</option>'+
           '<option value="1">1</option>'+
           '<option value="2">2</option>'+
           '<option value="3">3</option>'+
           '<option value="4">4</option>'+
           '<option value="5">5</option>'+
           '<option value="6">6</option>'+
           '<option value="7">7</option>'+
           '<option value="8">8</option>'+
           '<option value="9">9</option>'+
           '<option value="10">10</option>'+
           '<option value="11">11</option>'+
           '<option value="12">12</option>'+
           '<option value="13">13</option>'+
           '<option value="14">14</option>'+
           '</select></div>'+
           '<button onclick="guardarConf();">Guardar Configuración</button><button onclick="cancelarConf();">Cancelar</button>';
         }
           $("#perfil_izquierda").html(cont_izq);
           $("#cont_configuracion").html(cont_abajo);
           $('select[name="semestre"]').val(data[datos][6]);
           $('select[name="tema"]').val(data[datos][7]);
       },
     });
}

/**
* Function cambiar Imagen Front
* identifica el tipo de id que va a tomar y Cambia la imagen del usuario solamente en el front
*/
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $("#img_perfil_com").css("opacity","0");
        $("#fotoPerfilM").css("opacity","0");
        reader.onload = function (e) {
            setTimeout(function(){$('#img_perfil_com').attr('src', e.target.result);
            $("#img_perfil_com").css("transition","1s");
            $("#img_perfil_com").css("opacity","1");
          },1000);
            setTimeout(function(){$('#fotoPerfilM').attr('src', e.target.result);
            $("#fotoPerfilM").css("transition","1s");
            $("#fotoPerfilM").css("opacity","1");
          },1000);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function activarInput(){
 $("#perfil_img").click();
    document.getElementById("perfil_img").addEventListener("change", function(){
        var val = $("#perfil_img").val();
        elemento = document.getElementById("perfil_img");
        var file_type = val.substr(val.lastIndexOf('.')).toLowerCase();
        if (file_type  === '.jpg') {
            notificacion("PROCESANDO...");
            var formData = new FormData();
            correo = localStorage.getItem("correoSesion");
            formData.append("file",elemento.files[0]);
            formData.append("email", correo);
          var elemento = document.getElementById("file_img");
            $.ajax({
              url: 'http://148.220.52.76:8080/usuario/modificar_foto',
              type: 'POST',
              processData : false,
              contentType: false,
              data:formData,
              success: function(data){
                if (data == "OK"){
                    elemento = document.getElementById("perfil_img");
                    readURL(elemento);
                }
              }
            });
        }else{
            notificacion("Subir.jpg");
            $("#perfil_img").val("");
        }
    });

}

function notificacion(texto){
  $("#notifi").css("transition","0.8s");
  $("#notifi").css("left","80%");
  $("#notifi").find("h1").html(texto);
  setTimeout(function(){
    $("#notifi").css("left","110%");
  },4000);
}

function guardarConf(){
  id = localStorage.getItem('idUser');
  institucion = $('select[name="institucion"]').val();
  nombre = $("#nombre").val();
  apellidos = $("#apellidos").val();
  semestre = $('select[name="semestre"]').val();
  password = $("#password").val();
  tema = $('select[name="tema"]').val();
  localStorage.setItem('nombre',nombre);
  localStorage.setItem('apellidos',apellidos)
  localStorage.setItem('ide',tema)
  if (password=='********'){
    password = null;
  }
  $.ajax({
     url: 'http://148.220.52.76:8080/usuario/modificar_datos_usuario',
     type: 'GET',
     data:{id:id, institucion:institucion, nombre:nombre, apellidos:apellidos, semestre:semestre, password:password, tema:tema},
     success: function(data){
       if(data == "OK"){
         notificacion("Cambios Realizados");
         window.location = "inicio_alumno.html";
       }
     },
   });
}

function cancelarConf(){
    window.location = "inicio_alumno.html";
}

function generalGrupojs(){
  id = localStorage.getItem('idUser');
  $.ajax({
     url: 'http://148.220.52.76:8080/maestro/obtener_datos_index',
     type: 'GET',
     data:{id:id},
     success: function(data){
       cont2 = '<div class="info_general"><div class="info_general_sup"><img src="../static/img/grid.svg" alt=""><h1>General</h1></div>';
       //ajax
       cont2 += '<div class="cuadro_info"><h1>'+data['totalGrupos']+'</h1><h2>Total Grupos</h2></div>'+
           '<div class="cuadro_info"><h1>'+data['gruposActivos']+'</h1><h2>Grupos Activos</h2></div>'+
           '<div class="cuadro_info"><h1>'+data['evalPendientes']+'</h1><h2>Evaluaciones Pendientes</h2></div>'+
           '<div class="cuadro_info"><h1>'+data['alumnosInscritos']+'</h1><h2>Alumnos Inscritos</h2></div>'+
           '<div class="cuadro_info"><h1>'+data['tareasCreadas']+'</h1><h2>Tareas Creadas</h2></div></div>'+
           '<div class="info_grupos_general"><div class="info_general_sup"><img src="../static/img/group.svg" alt="">'+
               '<h1>Mis Grupos</h1></div>';
       //for para recorrer los grupos
       for(datos in data['datosGrupos']){
           cont2 += '<div class="general_grupo"><div class="superior_grupo"><h1>'+data['datosGrupos'][datos]['nombre']+'</h1></div>'+
               '<div class="cuadro_grupo"><h1>'+data['datosGrupos'][datos]['participantes']+'</h1><h2>Participantes</h2><div class="img_cuadro_grupo">'+
           '<img src="../static/img/avatar.svg" alt=""></div></div>'+
               '<div class="cuadro_grupo"><h1>'+data['datosGrupos'][datos]['pendientes']+'</h1><h2>Pendientes</h2><div class="img_cuadro_grupo">'+
                       '<img src="../static/img/alert.svg" alt=""></div></div>'+
               '<div class="cuadro_grupo"><h1>'+data['datosGrupos'][datos]['enviadas']+'</h1><h2>Recibidas</h2><div class="img_cuadro_grupo">'+
                       '<img src="../static/img/inbox.svg" alt=""></div></div>'+
               '<div class="cuadro_grupo" style="border-right: none;"><h1>'+data['datosGrupos'][datos]['clave']+'</h1><h2>Clave</h2>'+
                   '<div class="img_cuadro_grupo"><img src="../static/img/key.svg" alt=""></div></div></div>';
       }


       cont2 += '</div>';
       $("#conte_general").css("display", "block");
       $("#contenido").css("display", "none");
       $("#contenido_maestros").css("display", "none");
       $("#conte_general").html(cont2);
     },
   });
}

/**
* function desplegarNoti
* Muestra las notificaciones o formularios flotantes
*/
function desplegarNoti(){
    $("#obs_not").css("display","block");
    $("#notificaciones").css("display","block");
}

/**
* function desplegarNoti
* Oculta las notificaciones o formularios flotantes
*/
function closeNoti(){
    $(':input').val('');
    $("#obs_not").css("display","none");
    $("#notificaciones").css("display","none");
}

/**
* function desplegarNoti
* Muestra las notificaciones o formularios flotantes de Tareas
*/
function desplegarNotiTarea(){
    $("#obs_not").css("display","block");
    $("#notificaciones_tareas").css("display","block");
}

/**
* function desplegarNoti
* Oculta las notificaciones o formularios flotantes de Tareas
*/
function closeNotiTarea(){
    $(':input').val('');
    $("#obs_not").css("display","none");
    $("#notificaciones_tareas").css("display","none");
}

/**
* Funcion que despliega el formulario para
* registrar un nuevo grupo
*/
$(function(){
    $("#agregarGrupo").click(function(){
        cont = '<div class="notif_sup"><img src="../static/img/cancel.svg" onclick="closeNoti();" alt="">'+
                '<h1>Crear Grupo</h1></div><div class="notif_cont"><input type="text" id="nombreGrupo" placeholder="Nombre Grupo">'+
               '<textarea placeholder="Descripcion" id="descripcionGrupo"></textarea></div><div class="noti_footer">'+
                '<button onclick="crearGrupo();">Crear</button><h4 onclick="closeNoti();">Cancelar</h4></div>';
        $("#notificaciones").html(cont);
        desplegarNoti();
    });
});

/**
* Funcion que carga los grupos del alumno
* Y los muestra en el template
*/
$(function(){
    $("#ver_grupos").click(function(){
        cargar_alumno();
    });
});

/**
* function menu_tareas
* @param tipo
* @param idGrupo
* @param claveGrupo
* Muestra las tareas por cada grupo y muestra cuales son los que entregaron y cuales
* No se han revisado
*/
function menu_tareas(tipo, idGrupo, claveGrupo){
  cont = '<div class="inferior_menu"><div class="ico_menu" onclick=\'menuMiembros(\"'+idGrupo+'\", \"'+claveGrupo+'\")\'><img src="../static/img/grupos1.svg"><h1>Miembros</h1></div>'+
      '<div class="ico_menu ico_activo" onclick=\'menu_tareas(\"actual\",\"'+idGrupo+'\", \"'+claveGrupo+'\")\'><img src="../static/img/tareas1.svg"><h1>Tareas</h1></div>'+
      '<div class="ico_menu" onclick=\'menuAjustesGrupo(\"'+idGrupo+'\", \"'+claveGrupo+'\")\'><img src="../static/img/ajustes1.svg"><h1>Ajustes</h1></div></div><div id="cont_maestro">'+
  '</div>';
  $("#contenido_maestros").html(cont);
    if(tipo == "actual"){
      cont = '<div class="inferior_agregar"><h1>Tareas</h1><div onclick=\'agregarTareaGrupo(\"'+idGrupo+'\", \"'+claveGrupo+'\")\'"><img src="../static/img/add.svg"><h3>Nueva</h3></div>'+
          '</div><div class="tareas_estado_contenedor"><button onclick=\'menu_tareas(\"pasada\", \"'+idGrupo+'\", \"'+claveGrupo+'\")\'">Pasadas</button><button class="activo" onclick=\'menu_tareas(\"actual\", \"'+idGrupo+'\", \"'+claveGrupo+'\")\'">Actuales</button></div><div class="contenido_miembros">';
    }else{
      cont = '<div class="inferior_agregar"><h1>Tareas</h1><div onclick=\'agregarTareaGrupo(\"'+idGrupo+'\", \"'+claveGrupo+'\")\'"><img src="../static/img/add.svg"><h3>Nueva</h3></div>'+
          '</div><div class="tareas_estado_contenedor"><button class="activo" onclick=\'menu_tareas(\"pasada\", \"'+idGrupo+'\", \"'+claveGrupo+'\")\'">Pasadas</button><button onclick=\'menu_tareas(\"actual\", \"'+idGrupo+'\", \"'+claveGrupo+'\")\'">Actuales</button></div><div class="contenido_miembros">';
    }
        $.ajax({
           url: 'http://148.220.52.76:8080/tareas/get_tareas_grupo',
           type: 'GET',
           data:{grupo:idGrupo},
           success: function(data){
             for(datos in data){
               if(tipo == data[datos][5]){
                 cont += '<div class="tarea"><h1>'+data[datos][1]+'</h1><div class="tarea_fechas"><img src="../static/img/date.svg"><h2>'+data[datos][2]+'</h2></div>'+
                        '<div class="tarea_recibidas"><img src="../static/img/inbox.svg"><h2>'+data[datos][3]+'</h2></div><div class="notif_salir notif_salir_tareas notif_salir_tareas_recibidas">'+
                            '<div class="triangle"></div><h1>Tareas Recibidas</h1></div><div class="tarea_faltantes"><img src="../static/img/alert.svg"><h2>'+data[datos][4]+'</h2></div>'+
                        '<div class="notif_salir notif_salir_tareas notif_salir_tareas_nocalificadas"><div class="triangle"></div>'+
                            '<h1>Tareas Sin Calificar</h1></div><img onclick=\'detallesTarea(\"'+data[datos][0]+'\", \"'+data[datos][1]+'\", \"'+data[datos][2]+'\",\"'+idGrupo+'\",\"'+claveGrupo+'\")\'" class="next_tareas" src="../static/img/next.svg"></div>';
               }
             }
            $("#cont_maestro").html(cont);
           },
         });
}

function detallesTarea(id , titulo , fechaFin, idGrupo, claveGrupo){
    id_tarea = id
    cont = '<div class="inferior_agregar"><img onclick=\'menu_tareas(\"actual\",\"'+idGrupo+'\",\"'+claveGrupo+'\")\' src="../static/img/next.svg" class="next_invert">';
    //Ajax obtener datos
    var titulo =titulo;
    var descripcion =descripcion;
    var fechaFin = fechaFin;
    var fechaInicio = ""
    $.ajax({
       url: 'http://148.220.52.76:8080/tareas/detalles_tarea',
       type: 'GET',
       data:{idTarea:id_tarea, idGrupo:idGrupo},
       success: function(data){

        cont += '<h1>'+titulo+'</h1><div onclick=\'editarTarea(\"'+id_tarea+'\",\"'+idGrupo+'\", \"'+claveGrupo+'\")\'"><img src="../static/img/edit.svg"><h3>Editar</h3></div></div>'+
                '<div class="contenido_miembros"><div class="infor_general_tarea"><h2>Información General</h2>'+
                '<h3>Fecha de creación: '+data[0][3]+'</h3><h3>Fecha de entrega: '+fechaFin+'</h3><div class="descr_aux">'+data[0][2]+'</div>'+
                   '</div><div class="entregas_tarea"><h2>Entregas</h2><img src="../static/img/next.svg" id="flecha_entregas"'+
            'onclick=\'desplegar_entregas(\"'+id_tarea+'\",\"'+id +'\",\"'+titulo+'\",\"'+fechaFin+'\",\"'+idGrupo+'\", \"'+claveGrupo+'\");\'>'+
                    '<div class="desp_entregas" id="desp_entregas" style="display: none;">';

        $("#cont_maestro").html(cont);

       },
     });
}

function editarTarea(idTarea, idGrupo, claveGrupo){
    //ajax para cargar los datos
    var datos
    dataArchivos = new FormData();
    $.ajax({
       url: 'http://148.220.52.76:8080/tareas/getTarea',
       type: 'GET',
       data:{idTarea: idTarea , idGrupo: idGrupo},
       success: function(data){
         datos = data
         cont = '<div class="notif_sup"><img src="../static/img/cancel.svg" onclick="closeNotiTarea();" alt="">'+
              '<h1>Modificar Tarea</h1></div><div class="cuerpo_not_tarea"><input type="text" id="titulo" placeholder="Titulo" value="'+datos[0][1]+'">'+
              '<h2>Fecha Limite:</h2><input type="date" id="fechaLimite" value="'+datos[0][3]+'">'+
              '<h3>Archivos Adjuntos:</h3><div class="cont_archivos_tarea">';



        cont += '<div class="cont_tareas_archivos" id="archivos">'+
                '<input type="file" style="display: none;" id="addTarea" onchange="agregarArchivo()" id="addTarea" multiple size="10">';
        for (archivo in datos[0][4]){
          flag = datos[0][4][archivo];
          cont += '<div class="archivo" id="'+flag+'"><h4>'+flag+'</h4><img src="../static/img/cancel.svg" onclick=eliminarArchivoDir("'+flag+'",'+idGrupo+','+idTarea+')></div>';
        }
        cont += '<label class="archivo_add" for="addTarea">+</label></div>';
        cont += '<textarea placeholder="" id="descripcion" ></textarea>'+
                '<div class="noti_footer"><button onclick=\'ajaxModTareaGrupo(\"'+idTarea+'\",\"'+idGrupo+'\", \"'+claveGrupo+'\")\'">Modificar</button>'+
                '<h4 onclick="closeNotiTarea();">Cancelar</h4></div>';
        $("#notificaciones_tareas").html(cont);
        desplegarNotiTarea();
        CKEDITOR.replace( 'descripcion');
        CKEDITOR.instances.descripcion.setData(datos[0][2]);
       },
     });
}
function eliminarArchivoDir(nombreArchivo,idGrupo, idTarea){
  $.ajax({
     url: 'http://148.220.52.76:8080/tareas/removeArchivo',
     type: 'GET',
     data:{archivo:nombreArchivo,idGrupo:idGrupo,idTarea:idTarea},
     success: function(data){
       if(data=="OK"){
         notificacion("Archivo Eliminado");
         document.getElementById(nombreArchivo).remove()
       }
     },
   });
}

function desplegar_entregas(id_tarea, id , titulo , fechaFin, idGrupo, claveGrupo){
    elemento = document.getElementById("desp_entregas");
    if(elemento.style.display === "none"){
        $("#desp_entregas").css("display","block");
        $("#flecha_entregas").addClass("girar_flecha");
        var datosEntregas;
        $.ajax({
           url: 'http://148.220.52.76:8080/tareas/get_detalles_entrega',
           type: 'GET',
           data:{idTarea:id_tarea},
           success: function(data){
             cont = "";
             for (var i = 0; i < data.length; i++) {
               if(data[i][4] == "entregado" && data[i][5] == 0){
                 cont += '<div class="entrega"><img class="entrega_perfilF" src="http://148.220.52.76:8080/static/perfil/'+data[i][7]+'/user.jpg">'+
                         '<h2>'+data[i][0]+'</h2><h3>'+data[i][1]+" "+data[i][2]+'</h3><h2>'+'Pendiente de calificar'+'</h2>'+
                         '<img class="alert_entregas" src="../static/img/alert.svg"><img onclick=\'calificarTarea(\"'+id_tarea+'\", \"no_calificada\",\"'+id+'\",\"'+titulo+'\",\"'+fechaFin+'\", \"'+idGrupo+'\",\"'+claveGrupo+'\" ,\"'+data[i][6]+'\")\' class="next_entregas" src="../static/img/next.svg"></div>';
               }
               if(data[i][4] == "calificado"){
                 cont += '<div class="entrega"><img class="entrega_perfilF" src="http://148.220.52.76:8080/static/perfil/'+data[i][7]+'/user.jpg">'+
                         '<h2>'+data[i][0]+'</h2><h3>'+data[i][1]+" "+data[i][2]+'</h3><h2>'+'Calificada'+'</h2>'+
                         '<img onclick=\'calificarTarea(\"'+id_tarea+'\",\"calificada\",\"'+id+'\",\"'+titulo+'\",\"'+fechaFin+'\", \"'+idGrupo+'\",\"'+claveGrupo+'\" ,\"'+data[i][6]+'\")\' class="next_entregas" src="../static/img/next.svg"></div>';
               }

             }
              $("#cont_maestro").append(cont);
           },
         });

    }else{
        $("#desp_entregas").css("display","none");
        $("#flecha_entregas").removeClass("girar_flecha");
        $(".entrega").remove()
    }
}

function cargarGrupoMetricas(cont, tipo, tipoUsuario){
    tipo2=tipo
    idUsuario = localStorage.getItem("idUser");
    tipo = localStorage.getItem("tipo");
    url_aux = "";
    if(tipo == "maestro"){
       url_aux = 'http://148.220.52.76:8080/grupos/getAllGruposMaestro';
    }else{
       url_aux = 'http://148.220.52.76:8080/grupos/getAllGruposAlumno';
    }
    cont += '<div class="inferior_metricas"><h1>Evaluaciones por Grupo</h1></div><div class="contenido_res_metricas" id="contenido_res_metricas">';
    flag = "";
    if(tipoUsuario == "maestro"){
      $.ajax({
        url: url_aux,
        type: 'GET',
         data:{id_user:idUsuario},
         success: function(data){
           for(i=0; i<data.length; i++){
               flag += '<div class="res_metrica"><h1>'+data[i][1]+'</h1><img onclick=\'verMetrica(\"'+data[i][0]+'\", \"'+data[i][1]+'\", \"'+tipo2+'\","idGrupo")\' src="../static/img/next.svg" alt=""></div>';
           }
           cont += flag+'</div>';
           $("#contenido").html(cont);
         },
       });
    }
    else if(tipoUsuario == "estudiante"){
      $.ajax({
         url: url_aux,
         type: 'GET',
         data:{id_user:idUsuario},
         success: function(data){
           for(i=0; i<data.length; i++){
               cont += '<div class="res_metrica">'+
                   '<h1>'+data[i][1]+'</h1><img onclick=\'verMetricaA(\"'+data[i][0]+'\",\"'+idUsuario+'\", \"'+data[i][1]+'\", \"'+tipo2+'\", "idGrupo")\'  src="../static/img/next.svg" alt=""></div>';
           }
           $("#contenido").html(cont);
         },
       });
    }
}


function filtrarPorGrupo(idUsuario)
{
  //ajax mostrar filtro
  criterioBusqueda = $("#busqueda").val()
    tipo = localStorage.getItem("tipo");
     url_aux = "";
    if(tipo == "maestro"){
       url_aux = 'http://148.220.52.76:8080/grupos/filtrarGruposPorMaestro';
    }else{
       url_aux = 'http://148.220.52.76:8080/grupos/filtrarGruposPorAlumno';
    }
  //$("#contenido").removeClass("contenido_res_metricas");
  cont += '<div class="inferior_metricas"><h1>Evaluaciones por Grupo</h1></div><div class="contenido_res_metricas" id="contenido_res_metricas">';
  flag = "";
   $.ajax({
      url: url_aux,
      type: 'GET',
      data:{idUsuario:idUsuario , criterioBusqueda:criterioBusqueda},
      success: function(data){
          cont = "";
        datos = data;
        if(datos != ""){
            for(i=0; i<datos.length; i++){
                cont += '<div class="res_metrica"><h1>'+datos[i][1]+'</h1><img onclick=\'verMetrica() src="../static/img/next.svg" alt=""></div>';
            }
             cont += '</div>';
        }else{
            cont += "</div>";
        }
        $("#contenido_res_metricas").html(cont);
      }
    });

}

function filtrarPorTarea(idUsuario)
{
  //ajax mostrar filtro
  criterioBusqueda = $("#busqueda").val()
  //$("#contenido").removeClass("contenido_res_metricas");
  cont += '<div class="inferior_metricas"><h1>Evaluaciones por Tareas</h1></div><div class="contenido_res_metricas" id="contenido_res_metricas">';
   $.ajax({
      url: 'http://148.220.52.76:8080/grupos/filtrarTareasPorAlumno',
      type: 'GET',
      data:{idUsuario:idUsuario , criterioBusqueda:criterioBusqueda},
      success: function(data){
        datos = data
        console.log(datos);
        for(i=0; i<datos.length; i++){
            cont += '<div class="res_metrica"><h1>'+datos[i][1]+'</h1><img onclick=\'verMetrica() src="../static/img/next.svg" alt=""></div>';
        }
         cont += +'</div>';
        $("#contenido_res_metricas").html(cont);
      }
    });

}

function cargarTareasMetricas(cont, tipo, tipoUsuario){
    idUsuario = localStorage.getItem("idUser");
    cont += '<div class="inferior_metricas"><h1>Evaluaciones por Tareas</h1></div><div class="contenido_res_metricas" id="contenido_res_metricas">';
    flag = "";
    if(tipoUsuario == "maestro"){
      $.ajax({
         url: 'http://148.220.52.76:8080/tareas/getAllTareasMaestro',
         type: 'GET',
         data:{id_user:idUsuario},
         success: function(data){
           for(i=0; i<data.length; i++){
               flag += '<div class="res_metrica"><h1>'+data[i][1]+'</h1><img onclick=\'verMetrica(\"'+data[i][0]+'\", \"'+data[i][1]+'\", \"'+tipo+'\","idTarea")\' src="../static/img/next.svg" alt=""></div>';
           }
           cont += flag+'</div>';
           $("#contenido").html(cont);
         },
       });
    }
    else if(tipoUsuario == "estudiante"){
      $.ajax({
         url: 'http://148.220.52.76:8080/tareas/getTareasByAlumno',
         type: 'GET',
         data:{idAlumno:idUsuario},
         success: function(data){
           for(i=0; i<data.length; i++){
               cont += '<div class="res_metrica">'+
                     '<h1>'+data[i][2]+'</h1><img onclick=\'verMetricaA(\"'+data[i][0]+'\",\"'+idUsuario+'\", \"'+data[i][2]+'\", \"'+tipo+'\", "idTarea")\'  src="../static/img/next.svg" alt=""></div>';
           }
           $("#contenido").html(cont);
         },
       });
    }
}

function cargarAlumnosMetricas(cont, tipo){
    idUsuario = localStorage.getItem("idUser");
    cont += '<div class="inferior_metricas"><h1>Evaluaciones por Alumnos</h1><div class="busqueda">'+
                '<img src="../static/img/search.svg" alt=""><input id="busqueda" type="text" placeholder="Buscar..."></div></div><div class="contenido_res_metricas">';
    $.ajax({
       url: 'http://148.220.52.76:8080/grupos/get_alumnos_maestro',
       type: 'GET',
       data:{id_usuario:idUsuario},
       success: function(data){
         for(i=0; i<data.length; i++){
            cont += '<div class="res_metrica"><img src="http://148.220.52.76:8080/static/perfil/'+data[i]["email"]+'/user.jpg" class="perfil_metrica">'+
                 '<h1>'+data[i]["nombre"]+'</h1><img onclick=\'verMetrica(\"'+data[i]["id"]+'\", \"'+data[i]["nombre"]+'\", \"'+tipo+'\", "idAlumno")\'  src="../static/img/next.svg" alt=""></div>';
         }
         cont += '</div>';
         $("#contenido").html(cont);
       },
     });
}

function cargarAlumnosMetricas(cont, tipo){
    idUsuario = localStorage.getItem("idUser");
    //eliminar despues de tener ajax
    idAlumno = 1;
    nombreAlumno = "Jesús Fernando";
    cont += '<div class="inferior_metricas"><h1>Evaluaciones por Alumnos</h1></div><div class="contenido_res_metricas">';
    // obtner idUsuario
     $.ajax({
       url: 'http://148.220.52.76:8080/grupos/get_alumno_evaluacion',
       type: 'GET',
       data:{id_user:idUsuario},
       success: function(data){
         for(i in data){
             nombre = data[i][2]+' '+data[i][3];
             cont += '<div class="res_metrica"><img src="http://148.220.52.76:8080/static/perfil/'+data[i][1]+'/user.jpg" class="perfil_metrica">'+
            '<h1>'+nombre+'</h1><img onclick=\'verMetrica(\"'+data[i][0]+'\", \"'+nombre+'\", \"'+tipo+'\")\'  src="../static/img/next.svg" alt=""></div>';
         }
         cont += '</div>';
         $("#contenido").html(cont);
       },
     });
}

function mostrarFormasMetricas(tipo){
    tipoUsuario = localStorage.getItem("tipo");
    $("#contenido").css("display","block");
    $("#conte_general").css("display","none");
    $("#cont_maestro").css("display","none");
    $("#contenido_maestros").css("display","none");
    cont = "";
    if(tipo == "grupo" && tipoUsuario == "maestro"){
           cont = '<div class="sup_metricas"><div class="menu_metri ico_activo"  onclick=\'mostrarFormasMetricas(\"grupo\");\'><img class="img_activo" src="../static/img/grupos1.svg" alt=""><h1>Por Grupo</h1></div>'+
            '<div class="menu_metri" onclick=\'mostrarFormasMetricas(\"tarea\");\'><img src="../static/img/tareas1.svg" alt=""><h1>Por Tarea</h1></div>'+
            '<div class="menu_metri" onclick=\'mostrarFormasMetricas(\"alumno\");\'><img src="../static/img/person.svg" alt=""><h1>Por Alumno</h1></div></div>';
            cargarGrupoMetricas(cont, tipo, tipoUsuario);
       }
    if(tipo == "grupo" && tipoUsuario == "estudiante"){
           cont = '<div class="sup_metricas"><div class="menu_metri ico_activo" style="left: 30%;" onclick=\'mostrarFormasMetricas(\"grupo\");\'><img class="img_activo" src="../static/img/grupos1.svg" alt=""><h1>Por Grupo</h1></div>'+
            '<div class="menu_metri" style="left: 30%;" onclick=\'mostrarFormasMetricas(\"tarea\");\'><img src="../static/img/tareas1.svg" alt=""><h1>Por Tarea</h1></div></div>';
            cargarGrupoMetricas(cont, tipo, tipoUsuario);
       }
    if(tipo == "tarea" && tipoUsuario == "maestro"){
        cont = '<div class="sup_metricas"><div class="menu_metri"  onclick=\'mostrarFormasMetricas(\"grupo\");\'><img src="../static/img/grupos1.svg" alt=""><h1>Por Grupo</h1></div>'+
            '<div class="menu_metri ico_activo" onclick=\'mostrarFormasMetricas(\"tarea\");\'><img class="img_activo" src="../static/img/tareas1.svg" alt=""><h1>Por Tarea</h1></div>'+
            '<div class="menu_metri" onclick=\'mostrarFormasMetricas(\"alumno\");\'><img src="../static/img/person.svg" alt=""><h1>Por Alumno</h1></div></div>';
        cargarTareasMetricas(cont, tipo, tipoUsuario);
    }
    if(tipo == "tarea" && tipoUsuario == "estudiante"){
        cont = '<div class="sup_metricas"><div class="menu_metri" style="left: 30%;" onclick=\'mostrarFormasMetricas(\"grupo\");\'><img src="../static/img/grupos1.svg" alt=""><h1>Por Grupo</h1></div>'+
            '<div class="menu_metri ico_activo" style="left: 30%;" onclick=\'mostrarFormasMetricas(\"tarea\");\'><img class="img_activo" src="../static/img/tareas1.svg" alt=""><h1>Por Tarea</h1></div></div>';
        cargarTareasMetricas(cont, tipo, tipoUsuario);
    }
    if(tipo == "alumno" && tipoUsuario == "maestro"){
        cont = '<div class="sup_metricas"><div class="menu_metri"  onclick=\'mostrarFormasMetricas(\"grupo\");\'><img src="../static/img/grupos1.svg" alt=""><h1>Por Grupo</h1></div>'+
            '<div class="menu_metri " onclick=\'mostrarFormasMetricas(\"tarea\");\'><img src="../static/img/tareas1.svg" alt=""><h1>Por Tarea</h1></div>'+
            '<div class="menu_metri ico_activo" onclick=\'mostrarFormasMetricas(\"alumno\");\'><img class="img_activo" src="../static/img/person.svg" alt=""><h1>Por Alumno</h1></div></div>';
        cargarAlumnosMetricas(cont, tipo);
    }
}

function calificarTarea(idTarea, tipo, id , titulo , fechaFin, idGrupo, claveGrupo, idUser){
       $.ajax({
       type: "GET",
       url: "http://148.220.52.76:8080/tareas/mostrarCalificacionTarea",
       data:{idTarea:idTarea, idGrupo:idGrupo, idAlumno:idUser},
       success: function (data) {

         cont = '<div class="inferior_agregar"><img src="../static/img/double-arrow.svg"  onclick=\'detallesTarea(\"'+id +'\",\"'+titulo+'\",\"'+fechaFin+'\",\"'+idGrupo+'\", \"'+claveGrupo+'\");\' class="next_invert"><h1>'+localStorage.getItem("nombre")+' '+localStorage.getItem("apellidos")+'</h1></div>'+
                 '<div class="contenido_miembros cont_detalles_tare"><div class="infor_general_tarea"><h2>Información General</h2>';
         cont += '<h3>Nombre Tarea: '+titulo+'</h3><h3>Fecha de entrega: '+fechaFin+'</h3><h4>FINAL</h4>';
         if(tipo == "calificada"){
             cont += '<input type="text" value="'+data.calificacionIndividual.calificacion+'" readonly><h5>Observaciones</h5><textarea id="observacionesTextArea" readonly></textarea>';
            }else{
                cont += '<input type="text" placeholder="0" id="calificacion"><h5>Observaciones</h5><textarea placeholder="Observaciones" id="observaciones"></textarea>'+
                    '<button onclick=terminarEvaluacion(\"'+idTarea +'\",\"'+idGrupo+'\",\"'+idUser+'\")>Terminar Evaluación</button>';
            }
         cont += '<div class="tabla_metrica">'+
                  '<table><caption>Codificación</caption><tr><th class="tabla_uno">Métrica</th><th class="tabla_dos">Resultado Individual</th>'+
                  '<th class="tabla_uno">Indicador Alto</th><th class="tabla_dos">Indicador Bajo</th></tr>';

            for(i=0;i<=15;i++){
                 cont += '<tr><td class="tabla_uno">'+data.calificacionMetricas[i][2]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][9]+'</td><td class="tabla_uno">'+data.calificacionMetricas[i][4]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][5]+'</td></tr>';
            }
            cont += '</table></div><div class="tabla_metrica"><table><caption>Ejecución</caption><tr>'+
                             '<th class="tabla_uno">Métrica</th><th class="tabla_dos">Resultado Individual</th><th class="tabla_uno">Indicador Alto</th><th class="tabla_dos">Indicador Bajo</th></tr>';
             for(i=16;i<=30;i++){
                 cont += '<tr><td class="tabla_uno">'+data.calificacionMetricas[i][2]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][9]+'</td><td class="tabla_uno">'+data.calificacionMetricas[i][4]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][5]+'</td></tr>';
             }
             cont += '</table></div><div class="tabla_metrica"><table><caption>Compilación</caption><tr>'+
                             '<th class="tabla_uno">Métrica</th><th class="tabla_dos">Resultado Individual</th><th class="tabla_uno">Indicador Alto</th><th class="tabla_dos">Indicador Bajo</th></tr>';
             for(i=31;i<=33;i++){
                cont += '<tr><td class="tabla_uno">'+data.calificacionMetricas[i][2]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][9]+'</td><td class="tabla_uno">'+data.calificacionMetricas[i][4]+'</td><td class="tabla_dos">'+data.calificacionMetricas[i][5]+'</td></tr>';
             }
             cont += '</div>';
             $("#cont_maestro").html(cont);
             $("#observacionesTextArea").val(data.calificacionIndividual.observaciones)
       }
   });
}

/**
* function agregarTareaGrupo
* @param idGrupo
* @param claveGrupo
* Despliega el menu para poder crear una nueva tarea
*/
var dataArchivos = new FormData();
function agregarTareaGrupo(idGrupo, claveGrupo){
   dataArchivos = new FormData();
   cont = '<div class="notif_sup"><img src="../static/img/cancel.svg" onclick="closeNotiTarea();" alt="">'+
          '<h1>Agregar Tarea</h1></div><div class="cuerpo_not_tarea"><input type="text" id="titulo" placeholder="Titulo">'+
          '<h2>Fecha Inicio:</h2><input type="date" id="fechaInicio"><h2>Fecha Limite:</h2><input type="date" id="fechaLimite">'+
          '<h3>Archivos Adjuntos:</h3><div id="archivos" class="cont_archivos_tarea">'+
             '<input type="file" onchange="agregarArchivo()" style="display: none;" id="addTarea" multiple size="10">'+
             '<label class="archivo_add" for="addTarea">+</label></div><textarea placeholder="Descripción" id="descripcion"></textarea>'+
      '</div><div class="noti_footer"><button onclick=\'ajaxAddTareaGrupo(\"'+idGrupo+'\", \"'+claveGrupo+'\")\'">Crear</button>'+
        '<h4 onclick="closeNotiTarea();">Cancelar</h4></div>';
    $("#notificaciones_tareas").html(cont);
    desplegarNotiTarea();
     CKEDITOR.replace('descripcion');
}
function agregarArchivo(){
  var z = document.getElementById('addTarea');
  if ('files' in z) {
      if (z.files.length == 0) {
          notificacion("Seleccione 1 o mas archivos.");
      } else {
          for (var i = 0; i < z.files.length; i++) {
            var file = z.files[i];
            dataArchivos.append(file.name,z.files[i]);
            $("#archivos").append('<div class="archivo" id="'+file.name+'"><h4>'+file.name+'</h4><img src="../static/img/cancel.svg" onclick=eliminarArchivo("'+file.name+'")></div>');
          }
      }
  }
}

function eliminarArchivo(nombre){
  dataArchivos.delete(nombre);
  document.getElementById(nombre).remove()
}

function modCongIde(idGrupo, claveGrupo, idTarea){
    //Ajax saber cuales opciones selecciono
    $.ajax({
       url: 'http://148.220.52.76:8080/tareas/getConfigIde',
       type: 'GET',
       data:{idTarea: idTarea},
       success: function(data){

         cont = '<div class="notif_sup"><img src="../static/img/cancel.svg" onclick="closeNotiTarea();" alt="">'+
               '<h1>Modificar Opciones IDE</h1></div><div class="cuerpo_not_tarea">'+
               '<div class="opciones_ide">'
        if(data.highlihtActiveLine == true)
        {
          cont += '<div class="opcion_conf"><input name="conf_ide" id="highlihtActiveLine" type="checkbox" checked><h1>HighlihtActiveLine</h1></div>'
        }
        else
        {
          cont += '<div class="opcion_conf"><input name="conf_ide" id="highlihtActiveLine" type="checkbox"><h1>HighlihtActiveLine</h1></div>'
        }
        if(data.bahaviousEnabled == true)
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="bahaviousEnabled" type="checkbox" checked><h1>BahaviousEnabled</h1></div>'
        }
        else
        {
          cont += '<div class="opcion_conf"><input name="conf_ide" id="bahaviousEnabled" type="checkbox"><h1>BahaviousEnabled</h1></div>'
        }
        if(data.autocompletado_basico == true)
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="autocompletadoBasico" type="checkbox" checked><h1>Autocompletado basico</h1></div>'
        }
        else
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="autocompletadoBasico" type="checkbox"><h1>Autocompletado basico</h1></div>'
        }
        if(data.autocompletado_live == true)
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="autocompletadoLive" type="checkbox" checked><h1>Autocompletado live</h1></div>'
        }
        else
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="autocompletadoLive" type="checkbox"><h1>Autocompletado live</h1></div>'
        }
        if(data.snippets == true)
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="snippets" type="checkbox" checked><h1>Snippets</h1></div>'
        }
        else
        {
          cont+='<div class="opcion_conf"><input name="conf_ide" id="snippets" type="checkbox"><h1>Snippets</h1></div>'
        }
        if(data.spellcheck == true)
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="spellcheck" type="checkbox" checked><h1>Spellcheck</h1></div>'
        }
        else
        {
          cont+='<div class="opcion_conf"><input name="conf_ide" id="spellcheck" type="checkbox"><h1>Spellcheck</h1></div>'
        }
        if(data.useElasticTabstops == true)
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="useElasticTabstops" type="checkbox" checked><h1>UseElasticTabstops</h1></div>'
        }
        else
        {
          cont+='<div class="opcion_conf"><input name="conf_ide" id="useElasticTabstops" type="checkbox"><h1>UseElasticTabstops</h1></div>'
        }
        if(data.infoToken == true)
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="infoToken" type="checkbox" checked><h1>InfoToken</h1></div>'
        }
        else
        {
          cont+='<div class="opcion_conf"><input name="conf_ide" id="infoToken" type="checkbox"><h1>InfoToken</h1></div>'
        }
        if(data.minLines >0)
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="minLinesCheck" type="checkbox" checked><h1>MinLines</h1><input id="minLines" type="text" value="'+data.minLines+'"></div>'
        }
        else
        {
          cont+='<div class="opcion_conf"><input name="conf_ide" id="minLinesCheck" type="checkbox"><h1>MinLines</h1><input id="minLines" type="text"></div>'
        }
        if(data.maxLines >0)
        {
          cont +='<div class="opcion_conf"><input name="conf_ide" id="maxLInesCheck" type="checkbox" checked><h1>MaxLInes</h1><input id="maxLines" type="text" value="'+data.maxLines+'"></div>'
        }
        else
        {
          cont+='<div class="opcion_conf"><input name="conf_ide" id="maxLInesCheck" type="checkbox"><h1>MaxLInes</h1><input id="maxLines" type="text"></div>'
        }
        cont+='</div>'+
        '</div><div class="noti_footer"><button onclick=\'ajaxModTareaGrupoConfIde(\"'+idGrupo+'\", \"'+claveGrupo+'\" , \"'+idTarea+'\")\'">Configurar</button></div>';
         $("#notificaciones_tareas").html(cont);
         desplegarNotiTarea();
       },
     });

}

function agregarCongIde(idGrupo, claveGrupo, idTarea){
    cont = '<div class="notif_sup"><img src="../static/img/cancel.svg" onclick="closeNotiTarea();" alt="">'+
          '<h1>Opciones IDE</h1></div><div class="cuerpo_not_tarea">'+
           '<div class="opciones_ide">'+
                '<div class="opcion_conf"><input name="conf_ide" id="highlihtActiveLine" type="checkbox"><h1>HighlihtActiveLine</h1></div>'+
                '<div class="opcion_conf"><input name="conf_ide" id="bahaviousEnabled" type="checkbox"><h1>BahaviousEnabled</h1></div>'+
                '<div class="opcion_conf"><input name="conf_ide" id="autocompletadoBasico" type="checkbox"><h1>Autocompletado basico</h1></div>'+
                '<div class="opcion_conf"><input name="conf_ide" id="autocompletadoLive" type="checkbox"><h1>Autocompletado live</h1></div>'+
                '<div class="opcion_conf"><input name="conf_ide" id="snippets" type="checkbox"><h1>Snippets</h1></div>'+
                '<div class="opcion_conf"><input name="conf_ide" id="spellcheck" type="checkbox"><h1>Spellcheck</h1></div>'+
                '<div class="opcion_conf"><input name="conf_ide" id="useElasticTabstops" type="checkbox"><h1>UseElasticTabstops</h1></div>'+
                '<div class="opcion_conf"><input name="conf_ide" id="infoToken" type="checkbox"><h1>InfoToken</h1></div>'+
                '<div class="opcion_conf"><input name="conf_ide" id="minLinesCheck" type="checkbox"><h1>MinLines</h1><input id="minLines" type="text"></div>'+
                '<div class="opcion_conf"><input name="conf_ide" id="maxLInesCheck" type="checkbox"><h1>MaxLInes</h1><input id="maxLines" type="text"></div>'+
           '</div>'+
      '</div><div class="noti_footer"><button onclick=\'ajaxAddTareaGrupoConfIde(\"'+idGrupo+'\", \"'+claveGrupo+'\" , \"'+idTarea+'\"  )\'">Configurar</button></div>';
    $("#notificaciones_tareas").html(cont);
    desplegarNotiTarea();
}

function menuAjustesGrupo(idGrupo, claveGrupo){
    cont = '<div class="inferior_menu"><div class="ico_menu" onclick=\'menuMiembros(\"'+idGrupo+'\",\"'+claveGrupo+'\")\'>'+
            '<img src="../static/img/grupos1.svg"><h1>Miembros</h1></div>'+
      '<div class="ico_menu" onclick=\'menu_tareas(\"actual\",\"'+idGrupo+'\",\"'+claveGrupo+'\")\'><img src="../static/img/tareas1.svg"><h1>Tareas</h1></div>'+
      '<div class="ico_menu ico_activo" onclick=\'menuAjustesGrupo(\"'+idGrupo+'\",\"'+claveGrupo+'\")\'><img src="../static/img/ajustes1.svg"><h1>Ajustes</h1></div></div>'+
      '<div id="cont_maestro">';
    $("#contenido_maestros").html(cont);
    cont = '<div class="inferior_agregar"><h1>Ajustes</h1></div><div class="contenido_edit_grupo">'+
              '<div class="opcion_del_grupo"><h2>Nombre del Grupo: </h2><input type="text"></div>'+
              '<div class="opcion_del_grupo"><h2>Clave de Grupo: </h2><input type="text" readonly><img src="../static/img/edit.svg"></div>'+
              '<div class="opcion_del_grupo"><h3>Descripción del Grupo: </h3><textarea></textarea></div>'+
              '<button class="eliminar_grupo"><img src="../static/img/delete.svg" alt="">Eliminar Grupo</button>'+
              '<button>Guardar Cambios</button>'+
          '</div>';
      cont += '</div>';
    $("#cont_maestro").html(cont);
}

$(function(){
    $("#cerrar_sesion").click(function(){
      tipoLogin = localStorage.getItem('tipoLogin');
      if(tipoLogin == "normal"){
        localStorage.clear();
        window.location.href = 'login.html';
      }
      else if(tipoLogin == "gmail"){
        localStorage.clear();
        window.location.href = 'login.html';
      }
      else if (tipoLogin == "Facebook") {
        cerrarSesionFacebook();
      }
    });
});

function moverMascara(tipo, idAux, tipoAux, idAlumno){
    $("#mascara").css("visibility","visible");
    if(tipo == "codificacion"){
       $("#mascara").css("left","25.2%");
        $("#codi").addClass("activo_metrica_men");
        $("#eje").removeClass("activo_metrica_men");
        $("#com").removeClass("activo_metrica_men");
    }
    if(tipo == "ejecucion"){
       $("#mascara").css("left","41%");
        $("#codi").removeClass("activo_metrica_men");
        $("#eje").addClass("activo_metrica_men");
        $("#com").removeClass("activo_metrica_men");
    }
    if(tipo == "compilacion"){
        $("#mascara").css("left","57.3%");
        $("#codi").removeClass("activo_metrica_men");
        $("#eje").removeClass("activo_metrica_men");
        $("#com").addClass("activo_metrica_men");
    }
    cargarMetricasCategoria(tipo, idAux, tipoAux, idAlumno);
}

function menuAlumnosMetrica(idAlumno, nombre, tipo, tipoAux){
    cont = '<div class="res_metrica_sup"><img onclick=\'mostrarFormasMetricas(\"'+tipo+'\");\' src="../static/img/next.svg" alt=""><h1>'+nombre+'</h1></div>';
    cont += '<h6 class="detalles_grupo_nuevo">Grupos</h6>';
    $.ajax({
       url: 'http://148.220.52.76:8080/grupos/getGrupos',
       type: 'GET',
       data:{idUsuario:idAlumno},
       success: function(data){
         for(i=0; i<data.length; i++){
             cont += '<div class="res_metrica">'+
                 '<h1>'+data[i][1]+'</h1><img onclick=\'verMetricaA(\"'+data[i][0]+'\",\"'+idAlumno+'\", \"'+data[i][1]+'\", \"'+tipo+'\", "idGrupo")\'  src="../static/img/next.svg" alt=""></div>';
         }
         $("#contenido").html(cont);
       },
     });
     $.ajax({
        url: 'http://148.220.52.76:8080/tareas/getTareasByAlumno',
        type: 'GET',
        data:{idAlumno:idAlumno},
        success: function(data){
          cont += '<h6 class="detalles_grupo_nuevo">Tareas</h6>';
          for(i=0; i<data.length; i++){
              cont += '<div class="res_metrica">'+
                    '<h1>'+data[i][2]+'</h1><img onclick=\'verMetricaA(\"'+data[i][0]+'\",\"'+idAlumno+'\", \"'+data[i][2]+'\", \"'+tipo+'\", "idTarea")\'  src="../static/img/next.svg" alt=""></div>';
          }
          $("#contenido").html(cont);
        },
      });
    // cont += '<h6 class="detalles_grupo_nuevo">Tareas</h6>';
    // for(i=0; i<5; i++){
    //     cont += '<div class="res_metrica">'+
    //         '<h1>Nombre Tarea</h1><img onclick=\'verMetricaA(\"'+idAlumno+'\", \"'+nombreTarea+'\", \"'+tipo+'\", "idAlumno")\'  src="../static/img/next.svg" alt=""></div>';
    // }
    // $("#contenido").html(cont);
}

function verMetricaA(idAux, idAlumno, nombre, tipo, tipoAux){
    cont = '<div class="res_metrica_sup"><img onclick=\'mostrarFormasMetricas(\"'+tipo+'\");\' src="../static/img/next.svg" alt=""><h1>'+nombre+'</h1></div>'+
    '<div class="grupo_metricas"><div class="mascara_seccionado" id="mascara" style="transition: 1s; visibility: hidden;"></div>'+
        '<h2 id="codi" onclick=\'moverMascara(\"codificacion\",\"'+idAux+'\",\"'+tipoAux+'\", \"'+idAlumno+'\");\'>Codificación</h2>'+
        '<h2 onclick=\'moverMascara(\"ejecucion\",\"'+idAux+'\",\"'+tipoAux+'\", \"'+idAlumno+'\");\' id="eje">Ejecución</h2>'+
        '<h2 onclick=\'moverMascara(\"compilacion\",\"'+idAux+'\",\"'+tipoAux+'\", \"'+idAlumno+'\");\' id="com">Compilación</h2></div><div class="metrica_indi_cont" id="metrica_indi_cont">';
    $("#contenido").html(cont);
}

function verMetrica(idAux, nombre, tipo, tipoAux){
    if(tipo == "alumno"){
          menuAlumnosMetrica(idAux, nombre, tipo, tipoAux);
       }else{
        cont = '<div class="res_metrica_sup"><img onclick=\'mostrarFormasMetricas(\"'+tipo+'\");\' src="../static/img/next.svg" alt=""><h1>'+nombre+'</h1></div>'+
        '<div class="grupo_metricas"><div class="mascara_seccionado" id="mascara" style="transition: 1s; visibility: hidden;"></div>'+
            '<h2 id="codi" onclick=\'moverMascara(\"codificacion\",\"'+idAux+'\",\"'+tipoAux+'\");\'>Codificación</h2>'+
            '<h2 onclick=\'moverMascara(\"ejecucion\",\"'+idAux+'\",\"'+tipoAux+'\");\' id="eje">Ejecución</h2>'+
            '<h2 onclick=\'moverMascara(\"compilacion\",\"'+idAux+'\",\"'+tipoAux+'\");\' id="com">Compilación</h2></div><div class="metrica_indi_cont" id="metrica_indi_cont">';
       }
    $("#contenido").html(cont);
}

function cargarMetricasCategoria(tipo, idAux, tipoAux, idAlumno){
    $.ajax({
       url: 'http://148.220.52.76:8080/metricas/getAllMetricas',
       type: 'GET',
       data:{tipo:tipo},
       success: function(data){
         cont = "";
         for(i=0;i<data.length;i++){
          cont += '<div class="metrica_indi" id="'+data[i][0]+'" onclick=\'detallesMetrica(\"'+data[i][0]+'\", \"'+idAux+'\",\"'+tipoAux+'\", \"'+idAlumno+'\");\'><h1>'+data[i][1]+'</h1></div>';
         }
         cont += '</div><div class="info_metricas" id="info_metricas">';
         $("#metrica_indi_cont").html(cont);
       },
     });
}

function detallesMetrica(idMetrica, idAux, tipoAux, idAlumno){
    $(".metrica_indi").removeClass("metrica_activo");
    $("#"+idMetrica).addClass("metrica_activo");
    $.ajax({
       url: 'http://148.220.52.76:8080/metricas/getDetallesMetrica',
       type: 'GET',
       data:{idMetrica:idMetrica, idAux:idAux, tipoAux:tipoAux, idAlumno:idAlumno},
       success: function(data){
         tipoUsuario = localStorage.getItem('tipo');
         if(tipoUsuario != "estudiante"){
           cont = '<h1>'+data[0][1]+'</h1><p>'+data[0][2]+'</p>'+
                   '<table><tr><th>Indicador Máximo</th><th>Indicador Mínimo</th></tr>'+
                   '<tr><td>'+data[0][3]+'</td><td>'+data[0][4]+'</td></tr></table>'+
                   '<table><tr><th>Mejor Alumno</th><th>Peor Alumno</th></tr><tr>'+
                   '<td>'+data[0][7]+'</td><td>'+data[0][8]+'</td>'+
                   '</tr></table></div><div class="cont_graficas"><div class="grafica1" id="grafica1"></div></div>';
         }
         else{
           cont = '<h1>'+data[0][1]+'</h1><p>'+data[0][2]+'</p>'+
                   '<table><tr><th>Indicador Máximo</th><th>Indicador Mínimo</th></tr>'+
                   '<tr><td>'+data[0][3]+'</td><td>'+data[0][4]+'</td></tr></table>'+
                   '<table><tr><th>Mejor Promedio</th><th>Peor Promedio</th></tr><tr>'+
                   '<td>'+data[0][5]+'</td><td>'+data[0][6]+'</td>'+
                   '</tr></table></div><div class="cont_graficas"><div class="grafica1" id="grafica1"></div></div>';
         }

          $("#info_metricas").html(cont);
          datosGrafica = [];
          if(idAlumno != "undefined"){
            datosGrafica.push(data[0][1],data[0][3],data[0][4],data[0][5],data[0][6],data[0][9]);
          }
          else{
            datosGrafica.push(data[0][1],data[0][3],data[0][4],data[0][5],data[0][6],"nel");
          }
          graficar(datosGrafica);


       },
     });
}

function graficar(data){
  console.log(data);
  if(data[5]!="nel"){
      Highcharts.chart('grafica1', {
        chart: {
            type: 'column'
        },
        title: {
            text: data[0]
        },
        // subtitle: {
        //     text: 'Por Grupo'
        // },
        credits: {
           enabled: false
       },
        xAxis: {
            categories: [
                'Maximo Metrica',
                'Mejor Alumno',
                'Alumno',
                'Peor Alumno',
                'Minimo Metrica'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            tickInterval: 0.5,
            title: {
                text: 'Promedio'
            },
            plotBands: [{ // High wind
                from: data[2],
                to: data[1],
                color: 'rgba(68, 170, 213, 0.1)',
                label: {
                    text: 'Valores de Metrica',
                    align: 'right',
                    style: {
                        color: '#606060'
                    }
                }
            },{ // High wind
                from: data[4],
                to: data[3],
                color: 'rgba(50, 120, 255, 0.3)',
                label: {
                    text: 'Valores de Grupo',
                    align: 'right',
                    style: {
                        color: '#606060'
                    }
                }
            }]
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
          series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.2f}'
                }
            },
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Promedio',
            colorByPoint: true,
            data: [data[1],data[3],data[5],data[4],data[2]]


        }]
    });
  }
  else{
    Highcharts.chart('grafica1', {
      chart: {
          type: 'column'
      },
      title: {
          text: data[0]
      },
      // subtitle: {
      //     text: 'Por Grupo'
      // },
      credits: {
         enabled: false
     },
      xAxis: {
          categories: [
              'Maximo Metrica',
              'Mejor Alumno',
              'Peor Alumno',
              'Minimo Metrica'
          ],
          crosshair: true
      },
      yAxis: {
          min: 0,
          tickInterval: 0.5,
          title: {
              text: 'Promedio'
          },
          plotBands: [{ // High wind
              from: data[2],
              to: data[1],
              color: 'rgba(68, 170, 213, 0.1)',
              label: {
                  text: 'Valores de Metrica',
                  align: 'right',
                  style: {
                      color: '#606060'
                  }
              }
          },{ // High wind
              from: data[4],
              to: data[3],
              color: 'rgba(50, 120, 255, 0.3)',
              label: {
                  text: 'Valores de Grupo',
                  align: 'right',
                  style: {
                      color: '#606060'
                  }
              }
          }]
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
        series: {
              borderWidth: 0,
              dataLabels: {
                  enabled: true,
                  format: '{point.y:.2f}'
              }
          },
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [{
          name: 'Promedio',
          colorByPoint: true,
          data: [data[1],data[3],data[4],data[2]]


      }]
  });
  }
}


function terminarEvaluacion(idTarea , idGrupo , idUser)
{
  calificacion = $("#calificacion").val()
  observaciones = $("#observaciones").val()

  if(calificacion =="" || observaciones =="")
  {
    alert('error, debes de llenar todos los campos')
  }

  $.ajax({
     url: 'http://148.220.52.76:8080/tareas/calificarTarea',
     type: 'GET',
     data:{idTarea:idTarea , idAlumno:idUser , idGrupo:idGrupo , calificacion:calificacion , observaciones:observaciones},
     success: function(data){
       if(data.mensaje == "haz calificado la tarea")
       {
         mostrarGrupoMaestro(idGrupo)
       }
     }
   });


}
