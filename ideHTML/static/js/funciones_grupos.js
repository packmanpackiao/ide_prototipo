/**
* function crearGrupo
* Obtiene el nombre del grupo y la descripcion del grupo
* manda los datos por ajax para realizar el insert en la BD
*/
function crearGrupo(){
  nombreGrupo = $("#nombreGrupo").val();
  descripcion = $("#descripcionGrupo").val();
  if(nombreGrupo != "" && descripcion != ""){
    usuario = localStorage.getItem("idUser");
    $.ajax({
       url: 'http://148.220.52.76:8080/grupos/insertGrupo',
       type: 'GET',
       data:{
         nombre:nombreGrupo, descripcion:descripcion, id_user:usuario
       },
       success: function(data){
         if(data.mensaje == "OK"){
             cargarGruposMaestro();
           notificacion("GRUPO CREADO");
           closeNoti();
         }else{
           notificacion("DATOS INCORRECTOS");
         }
       },
     });
  }else{
    notificacion("COMPLETAR CAMPOS");
  }
}

/**
* function modificarGrupo
* @param idGrupo
* @param idUsuario
* Funcion que modifica los datos del grupo en especifico
*/
function modificarGrupo(idGrupo, idUsuario){
  nombreGrupo = $("#nombreGrupo").val();
  detallesGrupo = $("#detallesGrupo").val();
  cveGrupos = $("#cveGrupos").val();
  $.ajax({
     url: 'http://148.220.52.76:8080/grupos/update_grupo',
     type: 'GET',
     data:{id_grupo:idGrupo, nombreGrupo:nombreGrupo,
        detallesGrupo:detallesGrupo, cveGrupo:cveGrupos},
     success: function(data){
        if(data["mensaje"] == "Listo"){
          cargarGruposMaestro();
          mostrarGrupoMaestro(idGrupo);
        }else{
          notificacion("ERROR MODIFICANDO");
        }
     },
   });
}

/**
* function mostrarGrupoMaestro
* @param id
* Obtiene la informacion del grupo especifico
* Manda llamar la funcion menuMiembros para mostrarGrupoMaestro
* los miembros de dicho grupo
*/
function mostrarGrupoMaestro(id){
  usuario = localStorage.getItem("idUser");
  $.ajax({
     url: 'http://148.220.52.76:8080/grupos/get_datos_grupo',
     type: 'GET',
     data:{id_grupo:id, id_user:usuario},
     success: function(data){
       $("#conte_general").css("display", "none");
       $("#contenido_maestros").css("display","block");
       $("#contenido").css("display","block");
       cont = '<div class="info_group_sup"><h1>'+data["nombre"]+'</h1><p>'+data["detalles"]+'</p></div>';
       $("#contenido").html(cont);
       menuMiembros(data["id_grupo"], data["clave"]);
     },
   });
}

/**
* function menuMiembros
* @param id
* @param clave
* Obtiene los miembros del grupo y los muestra en el template
*/
function menuMiembros(id, clave){
  cont = '<div class="inferior_menu"><div class="ico_menu ico_activo" onclick=\'menuMiembros(\"'+id+'\",\"'+clave+'\")\'><img src="../static/img/grupos1.svg"><h1>Miembros</h1></div>'+
      '<div class="ico_menu" onclick=\'menu_tareas(\"actual\",\"'+id+'\",\"'+clave+'\")\'><img src="../static/img/tareas1.svg"><h1>Tareas</h1></div>'+
      '<div class="ico_menu" onclick=\'menuAjustesGrupo(\"'+id+'\",\"'+clave+'\")\'><img src="../static/img/ajustes1.svg"><h1>Ajustes</h1></div></div><div id="cont_maestro">'+
  '</div>';
  $("#contenido_maestros").html(cont);
  cont = '<div class="inferior_agregar"><h1>Miembros</h1><div onclick=\'agregarAlumnoGrupo(\"'+id+'\",\"'+clave+'\")\'><img src="../static/img/add.svg"><h3>Agregar</h3></div>'+
      '</div><div class="contenido_miembros">';
      $.ajax({
         url: 'http://148.220.52.76:8080/grupos/get_alumnos_grupo',
         type: 'GET',
         data:{id_grupo:id},
         success: function(data){
          for(datos in data){
            cont += '<div class="miembro"><img src="http://148.220.52.76:8080/static/perfil/'+data[datos][0]+'/user.jpg"class="fotoPerfil"><h1>'+data[datos][1]+' '+data[datos][2]+'</h1>'+
                    '<h4>'+data[datos][0]+'</h4><h5>'+data[datos][3]+'</h5><img src="../static/img/cancel.svg" onclick=\'eliminarAlumnoGrupo(\"'+id+'\",\"'+data[datos][4]+'\",\"'+clave+'\")\' class="del"></div>';
          }
          cont += '</div>'
          $("#cont_maestro").html(cont);
         },
       });
}

/**
* function eliminarAlumnoGrupo
* @param idGrupo
* @param idAlumno
* @param clave
* Elimina a un alumno de un grupo en especifico
*/
function eliminarAlumnoGrupo(idGrupo, idAlumno, clave){
    notificacion("Eliminando...");
  $.ajax({
     url: 'http://148.220.52.76:8080/grupos/eliminar_alumno_grupo',
     type: 'GET',
     data:{id_grupo:idGrupo, id_alumno:idAlumno},
     success: function(data){
      if(data == "OK"){
        menuMiembros(idGrupo, clave);
      }else{
        notificacion("ERROR ELIMINAR ALUMNO");
      }
     },
   });
}

/**
* function agregarAlumnoGrupo
* @param id
* @param clave
* Muestra el menu para agregar un alumno al grupo
*/
function agregarAlumnoGrupo(id, clave){
    cont = '<div class="notif_sup"><img src="../static/img/cancel.svg" onclick="closeNoti();" alt="">'+
    '<h1>Agregar Participante</h1></div><div class="notif_agregarAlumno"><h5>Agregar participantes mediante el Código de Grupo</h5>'+
        '<h6>'+clave+'</h6><h4>Agregar participantes manualmente</h4><div class="busqueda"><input style="text-align: center;" type="text" id="expediente" placeholder="Escribe Expediente Alumno">'+
        '</div></div><div class="noti_footer"><button onclick=\'agregarParticipanteGrupo(\"'+id+'\", \"'+clave+'\")\'>Agregar</button>'+
        '<h4 onclick="closeNoti();">Cancelar</h4></div>';
    $("#notificaciones").html(cont);
    desplegarNoti();
}

/**
* function agregarParticipanteGrupo
* @param id
* @param clave
* Manda el expediente del alumno y el id del grupo
* realiza un insert en la BD para registrar al usuario_id
* a dicho grupo
*/
function agregarParticipanteGrupo(id, clave){
    notificacion("Agregando...");
  expediente = $("#expediente").val();
  $.ajax({
     url: 'http://148.220.52.76:8080/grupos/agregar_participante_grupo',
     type: 'GET',
     data:{expediente:expediente, grupo:id},
     success: function(data){
       if(data == "OK"){
         closeNoti();
         menuMiembros(id, clave);
       }else if(data == "EXISTE"){
         notificacion("USUARIO YA EN GRUPO");
       }else{
         notificacion("NO EXISTE EL USUARIO");
       }
     },
   });
}

/**
* function cargarGruposMaestro
* Obtiene los grupos a los cuales registro el maestro
* los muestra en el template
*/
function cargarGruposMaestro(){
    contw = '<ul>';
    usuario = localStorage.getItem("idUser");
    $.ajax({
       url: 'http://148.220.52.76:8080/grupos/getAllGruposMaestro',
       type: 'GET',
       data:{id_user:usuario},
       success: function(data){
         if(data == ""){
           $("#grupos_maestro").css("display", "none");
         }else{
            $("#grupos_maestro").css("display", "block");
           for(datos in data){
             contw += "<li onclick=\"mostrarGrupoMaestro(\'"+data[datos][0]+"\')\"><h1>"+data[datos][1]+"</h1></li>";
           }
           contw += '</ul>';
           $("#grupos_maestro").html(contw);
         }
       },
     });
}

/**
* funcion ajaxAddTareaGrupo
* esta funcion crea una tarea
* @param Titulo
*@param fechaInicio
*@param fechaLimite
 @param descripcionTarea
*/
function ajaxAddTareaGrupo(id, claveGrupo){
  idGrupo = id
  //obener los datos del formulario
  titulo = $("#titulo").val();
  fechaInicio = $("#fechaInicio").val();
  fechaLimite = $("#fechaLimite").val();
  descripcion = CKEDITOR.instances.descripcion.getData();;
  if(titulo== "" || fechaInicio == "" || fechaLimite =="" || descripcion == "")
  {
    notificacion('COMPLETAR CAMPOS')
  }else{
      dataArchivos.append("nombreTarea",titulo);
      dataArchivos.append("fechaInicio",fechaInicio);
      dataArchivos.append("fechaFin",fechaLimite);
      dataArchivos.append("descripcionTarea",descripcion);
      dataArchivos.append("grupoId",idGrupo);
      $.ajax({
        url: 'http://148.220.52.76:8080/tareas/insertTarea',
        type: 'POST',
        processData : false,
        contentType: false,
        data:dataArchivos,
        success: function(data){
          if(data.mensaje == "se ha insertado la tarea"){
              agregarCongIde(id, claveGrupo, data.idTarea);
          }
          else{
           notificacion(data.mensaje);
          }
        },
      });
  }
}

/**
* function menuAjustesGrupo
* @param idGrupo
* @param claveGrupo
* Obtiene los datos actuales del grupo para poder modificarlos
*/
function menuAjustesGrupo(idGrupo, claveGrupo){
    cont = '<div class="inferior_menu"><div class="ico_menu" onclick=\'menuMiembros(\"'+idGrupo+'\",\"'+claveGrupo+'\")\'>'+
            '<img src="../static/img/grupos1.svg"><h1>Miembros</h1></div>'+
      '<div class="ico_menu" onclick=\'menu_tareas(\"actual\",\"'+idGrupo+'\",\"'+claveGrupo+'\")\'><img src="../static/img/tareas1.svg"><h1>Tareas</h1></div>'+
      '<div class="ico_menu ico_activo" onclick=\'menuAjustesGrupo(\"'+idGrupo+'\",\"'+claveGrupo+'\")\'><img src="../static/img/ajustes1.svg"><h1>Ajustes</h1></div></div>'+
      '<div id="cont_maestro">';
    $("#contenido_maestros").html(cont);
    usuario = localStorage.getItem("idUser");
    $.ajax({
       url: 'http://148.220.52.76:8080/grupos/get_datos_grupo',
       type: 'GET',
       data:{id_grupo:idGrupo, id_user:usuario},
       success: function(data){
          cont = '<div class="inferior_agregar"><h1>Ajustes</h1></div><div class="contenido_edit_grupo">'+
                    '<div class="opcion_del_grupo"><h2>Nombre del Grupo: </h2><input type="text" style="width: 60%;" id="nombreGrupo" value="'+data["nombre"]+'"></div>'+
                    '<div class="opcion_del_grupo"><h2>Clave de Grupo: </h2><input type="text" id="cveGrupos" value="'+data["clave"]+'" readonly><img onclick="generarClaveGrupo();" src="../static/img/edit.svg"></div>'+
                    '<div class="opcion_del_grupo"><h3>Descripción del Grupo: </h3><textarea id="detallesGrupo">'+data["detalles"]+'</textarea></div>'+
                    '<button class="eliminar_grupo" onclick=\'eliminarGrupo(\"'+idGrupo+'\")\' ><img src="../static/img/delete.svg" alt="">Eliminar Grupo</button>'+
                    '<button onclick=\'modificarGrupo(\"'+idGrupo+'\",\"'+usuario+'\")\'>Guardar Cambios</button>'+
                '</div>';
            cont += '</div>';
          $("#cont_maestro").html(cont);
       },
     });
}

/**
* function eliminarGrupo
* @param idGrupo
* Marca como no activo al grupo especificado
**/
function eliminarGrupo(idGrupo){
  $.ajax({
     url: 'http://148.220.52.76:8080/grupos/deleteGrupo',
     type: 'GET',
     data: {id_grupo:idGrupo},
     success: function(data){
       cargarGruposMaestro();
       $("#contenido_maestros").css("display","none");
       $("#contenido").css("display","none");
     },
   });
}

/**
* function generarClaveGrupo
* Genera una nueva clave para el grupo
*/
function generarClaveGrupo(){
  $.ajax({
     url: 'http://148.220.52.76:8080/grupos/renovar_clave_grupo',
     type: 'GET',
     success: function(data){
        $("#cveGrupos").val(data);
     },
   });
}

function ajaxModTareaGrupo(idTarea, id, claveGrupo){
  idGrupo = id
  //obener los datos del formulario
  titulo = $("#titulo").val();
  fechaLimite = $("#fechaLimite").val();
  descripcion = CKEDITOR.instances.descripcion.getData();
  permiteRetraso = 0;

  if(titulo== "" || fechaLimite =="" || descripcion== "")
  {
    notificacion('COMPLETAR CAMPOS')
    return
  }
  dataArchivos.append("nombreTarea",titulo);
  dataArchivos.append("fechaFin",fechaLimite);
  dataArchivos.append("descripcionTarea",descripcion);
  dataArchivos.append("idGrupo",id);
  dataArchivos.append("idTarea",idTarea);
    //modCongIde(id, claveGrupo , idTarea);
  $.ajax({
     url: 'http://148.220.52.76:8080/tareas/modificarTarea',
     type: 'POST',
     processData : false,
     contentType: false,
     data:dataArchivos,
     success: function(data){
       if(data.mensaje == "se ha modificado la tarea"){
          modCongIde(id, claveGrupo , idTarea);
       }
       else{
        notificacion(data.mensaje)
       }
     },
   });
}

function ajaxAddTareaGrupoConfIde(idGrupo, claveGrupo, idTarea){
    //ajax para gaurdar config de ide
    var id_tarea = idTarea
    var highlihtActiveLine ="False"
    var bahaviousEnabled ="False"
    var autocompletado_basico ="False"
    var autocompletado_live ="False"
    var snippets ="False"
    var spellcheck = "False"
    var useElasticTabstops = "False"
    var infoToken = "False"
    var maxLInesCheck =  "False"
    var minLinesCheck ="False"
    var numberMaxLines = 0
    var numberMinLines = 0
    if($("#highlihtActiveLine").is(':checked')){
      highlihtActiveLine = "True"
    }
    if($('#bahaviousEnabled').is(':checked')){
      bahaviousEnabled = "True"
    }
    if($('#autocompletadoBasico').is(':checked')){
      autocompletado_basico ="True"
    }
    if($('#autocompletadoLive').is(':checked')){
      autocompletado_live ="True"
    }
    if($('#snippets').is(':checked')){
      snippets ="True"
    }
    if($('#spellcheck').is(':checked')){
      spellcheck ="True"
    }
    if($('#useElasticTabstops').is(':checked')){
      useElasticTabstops ="True"
    }
    if($('#infoToken').is(':checked')){
      infoToken ="True"
    }
    if($('#minLinesCheck').is(':checked')){
      maxLInesCheck ="True"
      numberMinLines= $('#minLines').val()
    }
    if($('#maxLInesCheck').is(':checked')){
      minLinesCheck ="True"
      numberMaxLines = $('#maxLines').val()
    }

    if( (maxLInesCheck == true && maxLines =="") || (minLinesCheck == true && minLines == "")  )
    {
      notificacion('INGRESAR NUMERO LINEAS')
    }

    $.ajax({
       url: 'http://148.220.52.76:8080/tareas/configurarIde',
       type: 'GET',
       data:{
         id_tarea:id_tarea,
         highlihtActiveLine : highlihtActiveLine,
         bahaviousEnabled : bahaviousEnabled,
         autocompletado_basico : autocompletado_basico,
         autocompletado_live : autocompletado_live,
         snippets: snippets,
         spellcheck : spellcheck,
         useElasticTabstops : useElasticTabstops,
         infoToken : infoToken,
         numberMaxLines : numberMaxLines,
         numberMinLines : numberMinLines,
       },
       success: function(data){
         if(data.mensaje == "se ha configurado el ide"){
           closeNotiTarea();
           menu_tareas("actual", idGrupo, claveGrupo);
         }
         else{
          notificacion(data.mensaje)
         }
       },
     });
}


function ajaxModTareaGrupoConfIde(idGrupo, claveGrupo, idTarea){
    //closeNotiTarea();
    //menu_tareas("actual", idGrupo, claveGrupo);
    //ajax para gaurda conf

    var id_tarea = idTarea
    var highlihtActiveLine ="False"
    var bahaviousEnabled ="False"
    var autocompletado_basico ="False"
    var autocompletado_live ="False"
    var snippets ="False"
    var spellcheck = "False"
    var useElasticTabstops = "False"
    var infoToken = "False"
    var maxLInesCheck =  "False"
    var minLinesCheck ="False"
    var numberMaxLines = 0
    var numberMinLines = 0

    if($("#highlihtActiveLine").is(':checked')){
      highlihtActiveLine = "True"
    }
    if($('#bahaviousEnabled').is(':checked')){
      bahaviousEnabled = "True"
    }
    if($('#autocompletadoBasico').is(':checked')){
      autocompletado_basico ="True"
    }
    if($('#autocompletadoLive').is(':checked')){
      autocompletado_live ="True"
    }
    if($('#snippets').is(':checked')){
      snippets ="True"
    }
    if($('#spellcheck').is(':checked')){
      spellcheck ="True"
    }
    if($('#useElasticTabstops').is(':checked')){
      useElasticTabstops ="True"
    }
    if($('#infoToken').is(':checked')){
      infoToken ="True"
    }
    if($('#minLinesCheck').is(':checked')){
      maxLInesCheck ="True"
      numberMinLines= $('#minLines').val()
    }
    if($('#maxLInesCheck').is(':checked')){
      minLinesCheck ="True"
      numberMaxLines = $('#maxLines').val()
    }

    if( (maxLInesCheck == true && maxLines =="") || (minLinesCheck == true && minLines == "")  )
    {
      notificacion('INGRESAR NUMERO LINEAS')
    }
    $.ajax({
       url: 'http://148.220.52.76:8080/tareas/modificarIde',
       type: 'GET',
       data:{
         id_tarea:id_tarea,
         highlihtActiveLine : highlihtActiveLine,
         bahaviousEnabled : bahaviousEnabled,
         autocompletado_basico : autocompletado_basico,
         autocompletado_live : autocompletado_live,
         snippets: snippets,
         spellcheck : spellcheck,
         useElasticTabstops : useElasticTabstops,
         infoToken : infoToken,
         numberMaxLines : numberMaxLines,
         numberMinLines : numberMinLines,
       },
       success: function(data){
         if(data.mensaje == "se ha modificado el ide"){
           closeNotiTarea();
           menu_tareas("actual", idGrupo, claveGrupo);
         }
         else{
          notificacion(data.mensaje)
         }
       },
     });
    //ajax
}
