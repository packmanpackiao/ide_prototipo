"""IDE URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
     url(r'^', include('app.sistema.urls')),
     url(r'^', include('app.usuarios.urls')),
     url(r'^', include('app.grupos.urls')),
     url(r'^', include('app.tareas.urls')),
     url(r'^ide/', include('app.IDE.urls')),
     url(r'^api/', include('app.API.urls')),
     url(r'^compiler/', include('app.Compiler.urls')),
     url(r'^diagram/', include('app.Diagrams.urls')),
]
