from django.views.generic import TemplateView
from django.shortcuts import render
from django.conf import settings
from app.sistema import models
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseBadRequest, HttpResponse
import json


def IDEView(request):
    id_usuario = request.GET["idUsuario"]
    id_grupo = request.GET["idGrupo"]
    id_tarea = request.GET["idTarea"]
    tema = request.GET["idTema"]
    return render(request, "views/partials/ide.html")

@csrf_exempt
def insertMetricas(request, exp, gpo, tarea):
    if request:
        in_data= json.loads(request.body)
        met1=  in_data['met1']
        met2 = in_data['met2']
        met3 = in_data['met3']
        met4 = in_data['met4']
        met5 = in_data['met5']
        met6 = in_data['met6']
        met7 = in_data['met7']
        met8 = in_data['met8']
        met9 = in_data['met9']
        met10 = in_data['met10']
        met11 = in_data['met11']
        met12 = in_data['met12']
        met13= in_data['met13']
        met14 = in_data['met14']
        met15 = in_data['met15']
        met16 = in_data['met16']
        met17 = in_data['met17']
        met18 = in_data['met18']
        met19 = in_data['met19']
        met20 = in_data['met20']
        met21 = in_data['met21']
        met22 = in_data['met22']
        met23 = in_data['met23']
        met24 = in_data['met24']
        met25 = in_data['met25']
        met26 = in_data['met26']
        met27 = in_data['met27']
        met28 = in_data['met28']
        met29 = in_data['met29']
        met30 = in_data['met30']
        met31 = in_data['met31']
        met32 = in_data['met32']
        met33 = in_data['met33']
        met34 = in_data['met34']
        arrayMet = [met1, met2, met3, met4, met5, met6, met7,met8,met9,met10,met11,met12,met13,met14,met15,met16,met17,met18,met19,met20,
                    met21,met22,met23,met24,met25,met26,met27,met28,met29,met30,met31,met32,met33,met34]
        for x in range(0, len(arrayMet)):
            segura = x + 1
            p = models.Metricas.objects.filter(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea, id_metrica_id = segura).first()
            if p is not None:
                p.calificacion = arrayMet[x]
                p.save()
            else:
                p = models.Metricas.objects.create(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea, id_metrica_id =segura, calificacion=arrayMet[x])
        return HttpResponse('variables ingresadas', status=200)

def entregar(request, exp, gpo, tarea):
    if request:
        p = models.Usuario_Tareas.objects.filter(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea).first()
        if p is not None:
            p.estatus = "entregado"
            p.save()
            return HttpResponse('Estatus actualizado', status=200)
        else:
            return HttpResponse('Error', status=501)

def salir(request, exp, gpo, tarea):
    if request:
        p = models.Usuario_Tareas.objects.filter(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea).first()
        if p is not None:
            p.estatus = "curso"
            p.save()
            return HttpResponse('Estatus actualizado', status=200)
        else:
            return HttpResponse('Error', status=501)
