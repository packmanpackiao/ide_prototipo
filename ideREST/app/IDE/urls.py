from django.conf.urls import url
from .views import IDEView, insertMetricas, entregar, salir

urlpatterns = [
    url(r'^main$', IDEView, name='ide'),
    url(r'^insertmetricas/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', insertMetricas, name='insertMetricas'),
    url(r'^entregar/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', entregar, name='entregar'),
    url(r'^salir/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', salir, name='salir'),

]
