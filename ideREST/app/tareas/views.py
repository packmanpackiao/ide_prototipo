import json
import os
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.core import serializers
from app.sistema.models import Usuarios
from app.sistema.models import Grupos, Tarea, Usuario_Tareas,Tarea_Opciones_IDE, Metricas, Catalogo_Metricas
from django.db.models import Avg, Max, Min, Count
from app.sistema.models import Usuario_Grupos
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from IDE.settings import BASE_DIR
from django.core.files.base import ContentFile
from app.sistema.views import enviarCorreo
from django.db import connection


#obtener todos las tareas de la base de datos
def getAllTareas(request):
    if request.method == "GET":
        try:
            tareas = Tarea.objects.all()
            if tareas.exists():
                tareasToJson = {}
                cont =0
                for i in tareas:
                    tareasToJson[cont] = tareas.values()[cont]
                    cont += 1
                return JsonResponse(tareasToJson)
            else:
                return JsonResponse({"mensaje": "no hay tareas registradas"})
        except ObjectDoesNotExist:
            return JsonResponse({"mensaje" : "no hay tareas"})

    else:
        return JsonResponse({"mensaje": "no se hizo por get"})
#obtener todos las tareas de la base de datos

def getAllTareasMaestro(request):
    tareas = []
    usuario = request.GET["id_user"]
    grupo = Grupos.objects.filter(usuario_id=usuario)
    for data in grupo:
        tarea = Tarea.objects.filter(grupo_id=data.pk)
        for data2 in tarea:
            nombre_tarea = data2.nombre_tarea
            tareas.append([data2.pk, nombre_tarea])
    return JsonResponse(tareas, safe=False)


#metodo para insertar una nueva tarea
@csrf_exempt
def insertTarea(request):
    if request.method == "POST":
        nombreTarea = request.POST.get("nombreTarea")
        descripcionTarea = request.POST.get("descripcionTarea")
        fechaInicio = request.POST.get("fechaInicio")
        fechaFin = request.POST.get("fechaFin")
        grupoId = request.POST.get("grupoId")
        try:
            #crear le registro de la tarea
            tarea = Tarea(nombre_tarea = nombreTarea , descripcion_tarea = descripcionTarea , fecha_inicio = fechaInicio , fecha_fin = fechaFin, grupo_id = grupoId)
            tarea.save()
            nuevo = os.path.join(BASE_DIR, 'static/grupos/'+grupoId+'/'+str(tarea.id))
            os.makedirs(nuevo, 0777)

            for filename, archivos in request.FILES.iteritems():
                nombreArchivo = request.FILES[filename].name;
                full_filename = os.path.join(BASE_DIR, 'static/grupos/'+str(grupoId)+'/'+str(tarea.id)+'/', nombreArchivo)
                archivo = request.FILES.get(nombreArchivo)
                f = open(full_filename, 'wb+')
                file_content = ContentFile(archivo.read())
                for chunk in file_content.chunks():
                    f.write(chunk)
                f.close()
            #obtener los alumnos incritos al grupo
            alumnosInscritos = Usuario_Grupos.objects.filter(id_grupo_id = grupoId)

            #crear un registro en la tabla Usuario_Tareas para cada alumno
            try:
                for alumno in alumnosInscritos:
                    usuarioTarea = Usuario_Tareas(fecha_entrega = fechaFin , estatus='pendiente' , id_grupo_id=grupoId , id_usuario_id= alumno.id_usuario_id , id_tarea_id=tarea.id, calificacion=0 )
                    usuarioTarea.save()
                    #mandar correo de notificacion a los alumnos Inscritos
                    user = User.objects.get(id = alumno.id_usuario_id)
                    grupo = Grupos.objects.get(id = alumno.id_grupo_id)
                    asunto = "IDE: se ha creado una tarea"
                    mensaje = "Hola este correo es para informarte que se ha agregado la tarea "+tarea.nombre_tarea+" al grupo "+grupo.grupos + " no contestar este correo"
                    remitente = [user.username]
                    correo = enviarCorreo(asunto , mensaje , remitente)
            except ObjectDoesNotExist:
                return JsonResponse({"mensaje": "error al insertar en Usuario_Tareas"})

            return JsonResponse({"mensaje" : "se ha insertado la tarea" , "idTarea":tarea.id})
        except ObjectDoesNotExist:
            return JsonResponse({"mensaje": "error"})
    else:
        return JsonResponse({"mensaje": request.method})

#metodod para borrar una tarea
def deleteTarea(request):
    if request.method == "GET":
        idTarea = request.GET["idTarea"]
        instance = Tareas.objects.get(id = idTarea)
        instance.delete()
    else:
        return JsonResponse({"mensaje":"no se hizo por get"})

def get_tareas_grupo(request):
    """
        Controlador que obtiene las tareas del grupo e indica si con
        actuales o pasadas dependiendo de la fecha fin
    """
    array_tareas = []
    array = []
    id_grupo = request.GET["grupo"]
    tareas_grupo = Tarea.objects.filter(grupo_id=id_grupo)
    for data in tareas_grupo:
        tipo = tiempo_tarea(data.fecha_fin)
        fecha = data.fecha_fin.strftime("%d-%m-%Y %H:%M %p")
        tareas_recibidas = Usuario_Tareas.objects.filter(id_grupo_id=id_grupo,
             estatus="entregado", id_tarea_id=data.pk).count()
        tareas_recibidas_dos = Usuario_Tareas.objects.filter(id_grupo_id=id_grupo,
              estatus="calificado", id_tarea_id=data.pk).count()
        total_aux = tareas_recibidas + tareas_recibidas_dos
        tareas_sin_calificar = Usuario_Tareas.objects.filter(id_grupo_id=id_grupo,
             estatus="entregado", calificacion=0, id_tarea_id=data.pk).count()
        array_tareas.append([data.pk, data.nombre_tarea, fecha, total_aux,
            tareas_sin_calificar, tipo])
    return JsonResponse(array_tareas, safe=False)

def tiempo_tarea(fecha):
    """
        Funcion que retorna un actual o pasada dependiendo
        de la fecha fin
    """
    fecha = str(fecha).split(" ")
    actual = timezone.now()
    actual = str(actual).split(" ")
    if fecha[0] >= actual[0]:
        return "actual"
    else:
        return "pasada"

def get_tareas_alumno_grupo(request):
    """
        Obtiene las tareas por alumnos de un grupo en especifico de igual manera
        Obtiene el nombre del grupo de la tarea al que pertenece
    """
    if request.method == "GET":
        arreglo_tareas = []
        id_grupo = request.GET["id_grupo"]
        id_alumno = request.GET["id_alumno"]
        tareas_de_usuario = Usuario_Tareas.objects.filter(id_usuario_id=id_alumno, id_grupo_id=id_grupo)
        for data in tareas_de_usuario:
            detalles_tarea = Tarea.objects.filter(pk=data.id_tarea_id).first()
            detalles_grupo = Grupos.objects.filter(pk=detalles_tarea.grupo_id).first()
            if detalles_grupo.activo == True:
                if data.estatus != "entregado":
                    arreglo_tareas.append([detalles_tarea.pk,
                        data.estatus, detalles_tarea.nombre_tarea, detalles_grupo.grupos, detalles_grupo.pk])
        return JsonResponse(arreglo_tareas, safe=False)
    else:
        return JsonResponse({"mensaje": "no se hizo por get"})

def get_tareas_by_alumno(request):
    """
        Obtiene las tareas por alumnos de igual manera Obtiene
        el nombre del grupo de la tarea al que pertenece
    """
    if request.method == "GET":
        idAlumno = request.GET["idAlumno"]
        array_tareas = []
        tareasDeUsuario = Usuario_Tareas.objects.filter(id_usuario_id = idAlumno)
        for tarea in tareasDeUsuario:
            detalles_tarea = Tarea.objects.filter(pk = tarea.id_tarea_id).first()
            detalles_grupo = Grupos.objects.filter(pk = detalles_tarea.grupo_id).first()
            if detalles_grupo.activo == True:
                array_tareas.append([detalles_tarea.pk,
                    tarea.estatus, detalles_tarea.nombre_tarea, detalles_grupo.grupos, detalles_grupo.pk])
        return JsonResponse(array_tareas, safe=False)
    else:
        return JsonResponse({"mensaje": "no se hizo por get"})

def detalles_tarea(request):
    """
        Funcion que obtiene los detalles de una tarea en especifico
        manda llamar la funcion de obtener_fecha para conseguir el formato
        que se le mostrara al usuario
    """
    arreglo_tarea = []
    idGrupo = request.GET['idGrupo']
    idTarea = request.GET['idTarea']
    tarea = Tarea.objects.filter(pk=idTarea).first()
    fecha = tarea.fecha_inicio.strftime("%d-%m-%Y %H:%M %p")
    fecha = obtener_fecha(str(fecha))
    nueva = os.path.join(BASE_DIR, 'static/grupos/'+idGrupo+'/'+idTarea)
    archivos = os.listdir(nueva)
    arreglo_tarea.append([tarea.pk, tarea.nombre_tarea, tarea.descripcion_tarea,
        fecha,archivos])
    return JsonResponse(arreglo_tarea, safe=False)

def obtener_fecha(fecha):
    """
        Funcion que genera la fecha en un formato
        el cual el usuario pueda entender mas facilmente
    """
    mes = ("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
    fecha = fecha.split("-")
    mes_aux = fecha[1][:1]
    if mes_aux == "0":
        mes_aux = fecha[1][1:2]
    else:
        mes_aux = fecha[1]
    return fecha[0] + " de " + mes[int(mes_aux)] + " del " + fecha[2]

#metodo para calificar una tarea
def calificarTarea(request):
    if request.method == "GET":
        #obtener datos
        idTarea = request.GET["idTarea"]
        idAlumno = request.GET["idAlumno"]
        idGrupo = request.GET["idGrupo"]
        calificacion = request.GET["calificacion"]
        observaciones = request.GET["observaciones"]
        try:
            #obtener el registro de la tabla user_tareas donde se guardan las calificaciones
            #hacer el update del registro con la calficacion y cambiar su estatus
            userTarea = Usuario_Tareas.objects.get(id_usuario_id  =idAlumno , id_tarea_id = idTarea , id_grupo_id = idGrupo)
            userTarea.observaciones = observaciones
            userTarea.calificacion = calificacion
            userTarea.estatus = "calificado"
            #userTarea.estatus= 1
            userTarea.save()
            #mandar correo de notificacion de que una tarea ha sido calificada
            user = User.objects.get(id = idAlumno)
            tarea = Tarea.objects.get(id = idTarea)
            asunto = "IDE: se ha calificado una tarea"
            mensaje = "Hola este correo es para informarte que se ha calificado la tarea "+tarea.nombre_tarea+". Tu calificacion es: "+calificacion+" no contestar este correo"
            remitente = [user.username]
            correo = enviarCorreo(asunto , mensaje , remitente)
            return JsonResponse({"mensaje" : "haz calificado la tarea" })
        except ObjectDoesNotExist:
            return JsonResponse({"mensaje": "error"})

    else:
        return JsonResponse({"mensaje": "no se hizo por get"})

def get_detalles_entrega(request):
    if request.method == "GET":
        idTarea = request.GET["idTarea"]
        try:
            tareas = Usuario_Tareas.objects.filter(id_tarea_id=idTarea)
            if tareas.exists():
                tareasToJson = []
                cont =0
                for i in tareas:
                    userInfo = User.objects.get(id = i.id_usuario_id)
                    usuarios = Usuarios.objects.get(user_id = userInfo.id)
                    fecha = i.fecha_entrega.strftime("%d-%m-%Y %H:%M %p")
                    fecha = obtener_fecha(str(fecha))
                    tareasToJson.append([ usuarios.expediente , userInfo.first_name , userInfo.last_name , fecha, i.estatus, i.calificacion , userInfo.id, userInfo.username])
                return JsonResponse(tareasToJson , safe=False)
            else:
                return JsonResponse({"mensaje": "no hay tareas registradas"})
        except ObjectDoesNotExist:
            return JsonResponse({"mensaje" : "no hay tareas"})
    else:
        return JsonResponse({"mensaje": "no se hizo por get"})

def entregarTarea(request):
    if request.method == "GET":
        idAlumno =request.GET["idAlumno"]
        idGrupo = request.GET["idGrupo"]
        idTarea = request.GET["idTarea"]
        try:
            usuarioTarea = Usuario_Tareas.objects.get(id_usuario_id = idAlumno , id_grupo_id = idGrupo , id_tarea_id = idTarea)
            usuarioTarea.estatus = "entregada"
            usuarioTarea.save()
            return JsonResponse({"mensaje": "se ha entregado la tarea"})
        except ObjectDoesNotExist:
            return JsonResponse({"mensaje": "error"})
    else:
        return JsonResponse({"mensaje": "no se hizo por get"})

def configurarIde(request):
    if request.method== "GET":
        id_tarea = request.GET["id_tarea"]
        highlihtActiveLine = request.GET["highlihtActiveLine"]
        bahaviousEnabled = request.GET["bahaviousEnabled"]
        autocompletado_basico = request.GET["autocompletado_basico"]
        autocompletado_live = request.GET["autocompletado_live"]
        snippets = request.GET["snippets"]
        spellcheck = request.GET["spellcheck"]
        useElasticTabstops = request.GET["useElasticTabstops"]
        infoToken = request.GET["infoToken"]
        maxLines = request.GET["numberMaxLines"]
        minLines = request.GET["numberMinLines"]

        opcionesIde = Tarea_Opciones_IDE(
        id_tarea_id = id_tarea,
        highlihtActiveLine = highlihtActiveLine,
        bahaviousEnabled = bahaviousEnabled,
        autocompletado_basico = autocompletado_basico,
        autocompletado_live = autocompletado_live,
        snippets = snippets,
        spellcheck = spellcheck,
        useElasticTabstops = useElasticTabstops,
        infoToken = infoToken,
        maxLines = maxLines,
        minLines = minLines
        )
        opcionesIde.save()

        if opcionesIde.pk is None:
            return JsonResponse({"mensaje":"error al crear el registro"})
        return JsonResponse({"mensaje": "se ha configurado el ide"})

    else:
        return JsonResponse({"mensaje": "error, no se hizo por GET"})

@csrf_exempt
def modificarTarea(request):
    if request.method == "POST":
        nombreTarea = request.POST.get("nombreTarea")
        descripcionTarea = request.POST.get("descripcionTarea")
        idTarea = request.POST.get("idTarea")
        fechaFin = request.POST.get("fechaFin")
        grupoId = request.POST.get("idGrupo")
        try:
            #modificar el registro de la tarea
            tarea = Tarea.objects.get(id = idTarea)
            tarea.nombre_tarea = nombreTarea
            tarea.descripcion_tarea = descripcionTarea
            tarea.fecha_fin = fechaFin
            tarea.save()
            for filename, archivos in request.FILES.iteritems():
                nombreArchivo = request.FILES[filename].name;
                full_filename = os.path.join(BASE_DIR, 'static/grupos/'+str(grupoId)+'/'+str(tarea.id)+'/', nombreArchivo)
                archivo = request.FILES.get(nombreArchivo)
                f = open(full_filename, 'wb+')
                file_content = ContentFile(archivo.read())
                for chunk in file_content.chunks():
                    f.write(chunk)
                f.close()
            #modificar los registros en la tabla Usuario_Tareas para cada alumno
            usuarioTareas = Usuario_Tareas.objects.filter(id_tarea_id = idTarea , id_grupo_id = grupoId)
            for i in usuarioTareas:
                i.fecha_entrega = fechaFin
                i.save()

            return JsonResponse({"mensaje" : "se ha modificado la tarea"})
        except ObjectDoesNotExist:
            return JsonResponse({"mensaje": "error"})
    else:
        return JsonResponse({"mensaje" : "no se hizo por get"})

def get_config_ide(request):
    idTarea = request.GET["idTarea"]
    opciones_ide = Tarea_Opciones_IDE.objects.get(id_tarea_id = idTarea)
    diccionarioIde ={
    "id_tarea" : opciones_ide.id_tarea_id ,
    "highlihtActiveLine" : opciones_ide.highlihtActiveLine ,
    "bahaviousEnabled" : opciones_ide.bahaviousEnabled ,
    "maxLines" : opciones_ide.maxLines ,
    "minLines" : opciones_ide.minLines ,
    "autocompletado_basico" : opciones_ide.autocompletado_basico ,
    "autocompletado_live" : opciones_ide.autocompletado_live ,
    "snippets": opciones_ide.snippets ,
    "infoToken": opciones_ide.infoToken ,
    }
    return JsonResponse(diccionarioIde)

def getConfigIde(request):
    if request.method == "GET":
        idTarea = request.GET["idTarea"]
        opciones_ide = Tarea_Opciones_IDE.objects.get(id_tarea_id = idTarea)
        diccionarioIde ={
        "id_tarea" : opciones_ide.id_tarea_id ,
        "highlihtActiveLine" : opciones_ide.highlihtActiveLine ,
        "bahaviousEnabled" : opciones_ide.bahaviousEnabled ,
        "maxLines" : opciones_ide.maxLines ,
        "minLines" : opciones_ide.minLines ,
        "autocompletado_basico" : opciones_ide.autocompletado_basico ,
        "autocompletado_live" : opciones_ide.autocompletado_live ,
        "snippets": opciones_ide.snippets ,
        "infoToken": opciones_ide.infoToken ,
        "spellcheck" : opciones_ide.spellcheck ,
        "useElasticTabstops" : opciones_ide.useElasticTabstops
        }
        return JsonResponse(diccionarioIde)
    else:
        return JsonResponse({"mensaje" : "no se hizo por get"})

def modificarIde(request):
    if request.method== "GET":
        id_tarea = request.GET["id_tarea"]
        highlihtActiveLine = request.GET["highlihtActiveLine"]
        bahaviousEnabled = request.GET["bahaviousEnabled"]
        autocompletado_basico = request.GET["autocompletado_basico"]
        autocompletado_live = request.GET["autocompletado_live"]
        snippets = request.GET["snippets"]
        spellcheck = request.GET["spellcheck"]
        useElasticTabstops = request.GET["useElasticTabstops"]
        infoToken = request.GET["infoToken"]
        maxLines = request.GET["numberMaxLines"]
        minLines = request.GET["numberMinLines"]

        try:
            opcionesIde = Tarea_Opciones_IDE.objects.get(id_tarea_id = id_tarea)
            opcionesIde.highlihtActiveLine = highlihtActiveLine
            opcionesIde.bahaviousEnabled = bahaviousEnabled
            opcionesIde.autocompletado_basico = autocompletado_basico
            opcionesIde.autocompletado_live = autocompletado_live
            opcionesIde.snippets = snippets
            opcionesIde.spellcheck = spellcheck
            opcionesIde.useElasticTabstops = useElasticTabstops
            opcionesIde.infoToken = infoToken
            opcionesIde.maxLines = maxLines
            opcionesIde.minLines = minLines
            opcionesIde.save()
            return JsonResponse({"mensaje": "se ha modificado el ide"})
        except ObjectDoesNotExist:
            return JsonResponse({"mensaje": "error"})

        opcionesIde.save()

        if opcionesIde.pk is None:
            return JsonResponse({"mensaje":"error al crear el registro"})
        return JsonResponse({"mensaje": "se ha configurado el ide"})

    else:
        return JsonResponse({"mensaje": "error, no se hizo por GET"})


def getTarea(request):
    """
        Funcion que obtiene los detalles de una tarea en especifico
        manda llamar la funcion de obtener_fecha para conseguir el formato
        que se le mostrara al usuario
    """
    arreglo_tarea = []
    idGrupo = request.GET['idGrupo']
    idTarea = request.GET['idTarea']
    tarea = Tarea.objects.filter(pk=idTarea).first()
    fecha = tarea.fecha_fin.strftime("%Y-%m-%d" )
    nueva = os.path.join(BASE_DIR, 'static/grupos/'+idGrupo+'/'+idTarea)
    archivos = os.listdir(nueva)
    arreglo_tarea.append([tarea.pk, tarea.nombre_tarea, tarea.descripcion_tarea,
        fecha,archivos])
    return JsonResponse(arreglo_tarea, safe=False)


def removeArchivo(request):
    """
        Funcion para eliminar archivo
    """
    idGrupo = request.GET['idGrupo']
    idTarea = request.GET['idTarea']
    nombreArchivo = request.GET['archivo']
    os.remove('static/grupos/'+idGrupo+'/'+idTarea+'/'+nombreArchivo)
    return HttpResponse("OK")


def mostrarCalificacionTarea(request):
    if request.method=="GET":
        idUser = request.GET["idAlumno"]
        idTarea = request.GET["idTarea"]
        idGrupo = request.GET["idGrupo"]

        #como me dio hueva buscar joins en ORM, hice directamente el query alv
        cursor = connection.cursor()
        cursor.execute("select c.id , c.categoria, c.nombre_metrica , c.tieneIndicadores , c.indicadorAlto, c.indicadorBajo, m.id_grupo_id , m.id_tarea_id , m.id_usuario_id , m.calificacion from ide.sistema_catalogo_metricas c join ide.sistema_metricas m on c.id = m.id_metrica_id where m.id_usuario_id="+idUser+" AND m.id_tarea_id="+idTarea+" AND id_grupo_id="+idGrupo+";")
        solution = cursor.fetchall()
        calificacionIndividual = Usuario_Tareas.objects.filter(id_grupo_id=idGrupo , id_tarea_id=idTarea , id_usuario_id=idUser)
        return JsonResponse({"calificacionMetricas":solution , "calificacionIndividual": calificacionIndividual.values()[0]} , safe=False)
    else:
        return JsonResponse({"mensaje":"no fue por get"})

#obtener todos las metricas de la base de datos
def getAllMetricas(request):
    if request.method == "GET":
        tipo = request.GET["tipo"]
        metricas = []
        metrica = Catalogo_Metricas.objects.filter(categoria=tipo)
        for data in metrica:
            nombre_metrica = data.nombre_metrica
            if len(nombre_metrica) > 14:
                nombre_metrica = nombre_metrica[:14] + "..."
            metricas.append([data.pk, nombre_metrica])
        return JsonResponse(metricas, safe=False)
    else:
        return JsonResponse({"mensaje": "no se hizo por get"})

def getDetallesMetrica(request):
    if request.method == "GET":
        metricas = []
        idMetrica = request.GET["idMetrica"]
        tipoAux = request.GET["tipoAux"]
        idAux = request.GET["idAux"]
        idAlumno = request.GET["idAlumno"]
        if tipoAux == "idGrupo":
            promedios = Metricas.objects.filter(id_grupo_id=idAux, id_metrica_id=idMetrica).values("id_usuario_id").annotate(prom=Avg("calificacion"))
        if tipoAux == "idTarea":
            promedios = Metricas.objects.filter(id_tarea_id=idAux, id_metrica_id=idMetrica).values("id_usuario_id").annotate(prom=Avg("calificacion"))
        metrica = Catalogo_Metricas.objects.filter(pk=idMetrica)
        for data in metrica:
            nombre_metrica = data.nombre_metrica
            descripcion_metrica = data.descripcion_metrica
            indicadorAlto = data.indicadorAlto
            indicadorBajo = data.indicadorBajo
            metrica = Catalogo_Metricas.objects.filter(pk=idMetrica)
            cont = 0
            minimo = 0
            maximo = 0
            alumnoMin = 0
            alumnoMax = 0
            alumnoProm = 0
            for data2 in promedios:
                if str(data2["id_usuario_id"]) == str(idAlumno):
                    alumnoProm = data2['prom']
                if cont == 0:
                    minimo = data2['prom']
                    maximo = data2['prom']
                    alumnoMin = data2['id_usuario_id']
                    alumnoMax = data2['id_usuario_id']
                else:
                    if data2['prom'] < minimo:
                        minimo = data2['prom']
                        alumnoMin = data2['id_usuario_id']
                    if data2['prom'] > maximo:
                        maximo = data2['prom']
                        alumnoMax = data2['id_usuario_id']
                cont=+1
            maxAlumno = User.objects.filter(pk=alumnoMax).values("first_name", "last_name")
            minAlumno = User.objects.filter(pk=alumnoMin).values("first_name", "last_name")
            alumnoMin = minAlumno[0]['first_name'] + " "+ minAlumno[0]['last_name']
            alumnoMax = maxAlumno[0]['first_name'] + " "+ maxAlumno[0]['last_name']
            metricas.append([data.pk, nombre_metrica, descripcion_metrica, indicadorAlto, indicadorBajo, maximo, minimo, alumnoMax, alumnoMin, alumnoProm])
        return JsonResponse(metricas, safe=False)
    else:
        return JsonResponse({"mensaje": "no se hizo por get"})


def filtrarTareasPorAlumno(request):
    if request.method == "GET":
        idUsuario = request.GET["idUsuario"]
        criterioBusqueda = request.GET["criterioBusqueda"]

        arrayTareas = []
        #obtener los grupos a los que esta inscrito el usuario_id
        tareasRegistradas = Usuario_Tareas.objects.filter(id_usuario_id = idUsuario)
        tareasFiltradas = Tarea.objects.filter(nombre_tarea__startswith = criterioBusqueda)

        for i in tareasRegistradas:
            for x in tareasFiltradas:
                if i.id_tarea_id == x.id:
                    arrayTareas.append([x.id , x.nombre_tarea])
        return JsonResponse(arrayTareas , safe=False)
    else:
        return JsonResponse({"mensaje" : "no se hizo por get"})
