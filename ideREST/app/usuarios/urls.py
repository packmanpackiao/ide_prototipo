"""ide URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from app.usuarios import views
urlpatterns = [
    url(r'^usuario/getDatosUsuario$', views.getDatosUsuario, name="usuario_getDatosUsuario"),
    url(r'^usuario/getAllUsuarios$', views.getAllUsuarios, name="usuario_getAllUsuarios"),
    url(r'^usuario/cambiarTipo$', views.cambiarTipo, name="usuario_cambiarTipo"),
    url(r'^usuario/obtener_datos_usuario$', views.obtener_datos_usuario, name="obtener_datos_usuario"),
    url(r'^usuario/modificar_datos_usuario$', views.modificar_datos_usuario, name="modificar_datos_usuario"),
    url(r'^usuario/modificar_foto$', views.modificar_foto, name="modificar_foto"),
    url(r'^maestro/obtener_maestros$', views.obtener_maestros, name="obtener_maestros"),
    url(r'^maestro/cambiar_maestros$', views.cambiar_maestros, name="cambiar_maestros"),
    url(r'^maestro/registrar_maestro$', views.registrar_maestro, name="registrar_maestro"),
    url(r'^maestro/registrar_maestro_correo$', views.registrar_maestro_correo, name="registrar_maestro_correo"),
    url(r'^maestro/obtener_datos_index$', views.obtener_datos_index, name="obtener_datos_index"),
]
