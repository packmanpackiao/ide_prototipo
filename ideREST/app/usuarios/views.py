import json
import shutil
import os

from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.core import serializers
from app.sistema.models import Usuarios, Grupos, Tarea, Usuario_Tareas, Usuario_Grupos
from django.core.exceptions import ObjectDoesNotExist
from app.sistema.views import enviarCorreo
from django.views.decorators.csrf import csrf_exempt
from django.core.files.base import ContentFile
from IDE.settings import BASE_DIR

# Create your views here.

def getDatosUsuario(request):
    if request.method == "GET":
        correoUsuario = request.GET['email']
        try:
            datosUsuario = User.objects.get(username = request.GET['email'])
            datosUsuario2 = Usuarios.objects.get(user_id = datosUsuario.id)
            data = {
            'email': datosUsuario.username,
            'nombreUsuario' : datosUsuario.first_name,
            'expediente': datosUsuario2.expediente,
            }
            return JsonResponse(data)
        except ObjectDoesNotExist:
            return JsonResponse({'mensaje':'no existe el user'})

    else:
        return JsonResponse({'mensaje':'no se hizo por get'})

#obtener todos los usuarios de la base de datos
def getAllUsuarios(request):
    if request.method == "GET":
        usuarios = Usuarios.objects.all()
        usuariosToJson = {}
        cont = 0
        for i in usuarios:
            usuariosToJson[cont] = Usuarios.values()[cont]
            cont += 1
        return JsonResponse(usuariosToJson)
    else:
        return JsonResponse({"mensaje": "no se hizo por get"})

# Funcion para cambier el tipo de usuario entre:
def cambiarTipo(request):
    if request.method == "GET":
        id_usuario = request.GET['id']
        tipo_usuario = request.GET['tipo']
        try:
            datosUsuario = Usuarios.objects.get(user_id = id_usuario)
            if(datosUsuario.tipoUser_id==tipo_usuario):
                return JsonResponse({"mensaje" : "Este ya es su tipo de usuario"})
            datosUsuario.tipoUser_id = tipo_usuario
            if datosUsuario.tipoUser_id == 1:
                tipo = "estudiante"
            elif datosUsuario.tipoUser_id == 2:
                tipo = "maestro"
            elif datosUsuario.tipoUser_id == 3:
                tipo = "admin"
            return JsonResponse({"mensaje" : "se ha modificado el tipo de usuario correctamente","tipo":tipo})
        except ObjectDoesNotExist:
            return JsonResponse({'mensaje':'no existe el user'})

def registrar_maestro_correo(request):
    """
        Modifica de tipo un usuario ya registrado por medio de su correo
    """
    email = request.GET["email"]
    usuario = User.objects.filter(username=email).first()
    if usuario is not None:
        detalles_usuario = Usuarios.objects.filter(user_id=usuario.pk).first()
        detalles_usuario.tipoUser_id = 2
        detalles_usuario.save()
        asunto = "IDE: Cambio Privilegios"
        mensaje = "Cambio de privilegios a maestro en IDE, no contestar este correo"
        remitente = [usuario.username]
        correo = enviarCorreo(asunto , mensaje , remitente)
        return HttpResponse("OK")
    else:
        return HttpResponse("ERROR")

def obtener_datos_usuario(request):
    arreglo_usuario = []
    usuario = request.GET["id_usuario"]
    user = User.objects.filter(pk=usuario).first()
    user_detail = Usuarios.objects.filter(user_id=user.pk).first()
    arreglo_usuario.append([user.pk, user.username, user.first_name, user.last_name,
                           user_detail.institucion, user_detail.expediente,
                           user_detail.semestre, user_detail.tema_ide])
    return JsonResponse(arreglo_usuario, safe=False)

def registrar_maestro(request):
    """
        Controlador que registra un nuevo usuario
        como maestro
    """
    nombre = request.GET["nombre"]
    apellido = request.GET["apellido"]
    email = request.GET["email"]
    expediente = request.GET["expediente"]
    password = request.GET["password"]
    existe = User.objects.filter(username=email).first()
    if existe is None:
        usuario = User.objects.create_user(username=email, first_name=nombre, last_name=apellido, password=password)
        newUser = Usuarios(user_id=usuario.pk, institucion="Universidad Autonoma de Queretaro", semestre="MAESTRO", expediente=expediente, estatus=0 , tipoUser_id=2)
        newUser.save(force_insert=True)
        nueva = os.path.join(BASE_DIR, 'static/perfil/'+request.GET['email'])
        antigua = os.path.join(BASE_DIR, 'static/perfil/default')
        os.makedirs(nueva, 0777)
        for file in os.listdir(antigua):
            srcfile = os.path.join(BASE_DIR, 'static/perfil/default', file)
            dstfile = os.path.join(BASE_DIR, 'static/perfil/'+email, file)
            if os.path.isdir(srcfile):
                dir_copy(srcfile, dstfile)
            else: #if the source file path is just a file, copy the file
                shutil.copyfile(srcfile, dstfile)
        asunto = "IDE: Registro"
        mensaje = "Cuenta de maestro Creada en IDE, tu password provisional es "+password+", no contestar este correo"
        remitente = [email]
        correo = enviarCorreo(asunto , mensaje , remitente)
        return HttpResponse("OK")
    else:
        return HttpResponse("ERROR")

def obtener_maestros(request):
    """
        Funcion que obtiene los detalles de todos
        los maestros registrados
    """
    arreglo_maestro = []
    maestro = Usuarios.objects.filter(tipoUser_id=2)
    for data in maestro:
        detalles = User.objects.filter(pk=data.user_id).first()
        arreglo_maestro.append([detalles.pk, detalles.first_name, detalles.last_name,
                                data.expediente, detalles.username])
    return JsonResponse(arreglo_maestro, safe=False)

@csrf_exempt
def modificar_foto(request):
    """
        Controlador para modificar la foto del usuario
    """
    email = request.POST["email"]
    name = str("user.jpg")
    filename = os.path.join(BASE_DIR, 'static/perfil/'+email, name)
    archivo = request.FILES.get('file')
    f = open(filename, 'wb+')
    file_content = ContentFile(archivo.read())
    try:
        for chunk in file_content.chunks():
            f.write(chunk)
        f.close()
        return HttpResponse("OK")
    except:
        return HttpResponse("Error Archivo")


def cambiar_maestros(request):
    """
        Funcion que cambia el nivel de maestro
        a un nivel de alumno
    """
    maestro = request.GET["idMaestro"]
    maestro = Usuarios.objects.filter(user_id=maestro).first()
    if maestro is not None:
        maestro.tipoUser_id = 1
        maestro.save()
        return HttpResponse("OK")
    else:
        return HttpResponse("ERROR")

def modificar_datos_usuario(request):
    """
        Funcion que cambia los datos de un usuario
    """
    idUser = request.GET["id"]
    institucion = request.GET["institucion"]
    nombre = request.GET["nombre"]
    apellidos = request.GET["apellidos"]
    semestre = request.GET["semestre"]
    password = request.GET["password"]
    tema = request.GET["tema"]
    usuario = User.objects.filter(id=idUser).first()
    datos = Usuarios.objects.filter(user_id=idUser).first()
    if usuario is not None:
        if datos is not None:
            usuario.first_name = nombre
            usuario.last_name = apellidos
            datos.institucion = institucion
            datos.semestre = semestre
            datos.tema_ide = tema
            if password != "":
                usuario.set_password(password)
            usuario.save()
            datos.save()
            return HttpResponse("OK")
    else:
        return HttpResponse("ERROR")

def obtener_datos_index(request):
    if request.method == "GET":
        idUser = request.GET["id"]
        totalGrupos = Grupos.objects.filter(usuario_id=idUser).count()
        grupos = Grupos.objects.filter(usuario_id=idUser, activo=1)
        grupo = {}
        datosGrupos = {}
        array_aux = []
        tareasCreadas = 0
        alumnosInscritos = 0
        evalPendientes = 0
        cont = 0
        if grupos.exists():
            for i in grupos:
                grupo[cont] = grupos.values()[cont]
                usuarios_aux = Usuario_Grupos.objects.filter(id_grupo_id=grupo[cont]['id'])
                for datos in usuarios_aux:
                    if datos.id_usuario_id not in array_aux:
                        array_aux.append(datos.id_usuario_id)
                        alumnosInscritos += 1
                tareasCreadas += Tarea.objects.filter(grupo_id=grupo[cont]['id']).count()
                participantes = Usuario_Grupos.objects.filter(id_grupo_id=grupo[cont]['id']).count()
                pendientes = Usuario_Tareas.objects.filter(id_grupo_id=grupo[cont]['id'], estatus="entregado", calificacion__gt=0).count()
                enviadas = Usuario_Tareas.objects.filter(id_grupo_id=grupo[cont]['id']).count()
                evalPendientes += pendientes
                datosGrupos[cont] = {"nombre":grupo[cont]['grupos'],"participantes":participantes,"pendientes":pendientes,"enviadas":enviadas,"clave":grupo[cont]['pass_grupo']}
                cont += 1
        gruposActivos = Grupos.objects.filter(usuario_id=idUser,activo=1).count()
        # tareasCreadas = Tarea.objects.filter(usuario_id=idUser,activo=1).count()
        return JsonResponse({"totalGrupos":totalGrupos, "gruposActivos":gruposActivos, "tareasCreadas":tareasCreadas,
                "alumnosInscritos":alumnosInscritos,"evalPendientes":evalPendientes,"datosGrupos":datosGrupos})
