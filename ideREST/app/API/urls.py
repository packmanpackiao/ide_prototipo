from django.conf.urls import url
from .views import  createproject, deleteproject,getproject, renamefile, getfile, savefile, getpath, createfile, deletefile, getcode
urlpatterns = [
    url(r'^createproject/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', createproject, name='createProject'),
    url(r'^deleteproject/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', deleteproject, name='deleteProject'),
    url(r'^getproject/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$',getproject, name='getProject'),
    url(r'^renamefile/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)/(?P<fileName>[\w\-]+)/(?P<newFileName>[\w\-]+)$', renamefile, name='renameFile'),
    url(r'^getcode/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)/(?P<fileName>[\w\-]+)$', getfile, name='getFile'),
    url(r'^getfile/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)/(?P<fileName>[\w\-]+)$', getcode, name='getCode'),
    url(r'^savecode/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)/(?P<fileName>[\w\-]+)$', savefile, name='saveFile'),
    url(r'^getpath/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', getpath, name='getPath'),
    url(r'^createfile/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)/(?P<fileName>[\w\-]+)$', createfile, name='createfile'),
    url(r'^deletefile/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)/(?P<fileName>[\w\-]+)$', deletefile, name='deletefile'),
]
