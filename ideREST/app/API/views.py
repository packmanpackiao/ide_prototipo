from django.shortcuts import render
from django.conf import settings
from .models import Proyectos
from django.http import HttpResponse , HttpResponseBadRequest
from django.http import JsonResponse
import os
import json
from django.views.decorators.csrf import csrf_exempt


def createproject(request, exp, gpo, tarea):
    if request and request.method == 'GET':
        dirname = settings.MEDIA_ROOT+"/documents/"+exp+"/"+gpo+"/"+tarea+"/"
        if not os.path.exists(dirname):
            project = Proyectos.objects.filter(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea)).first()
            if project is not None:
                return HttpResponse('Tarea Ya Existe', status=501)
            else:
                os.makedirs(dirname)
                fileName = str(dirname + "file1.py")
                f = open(fileName, "w+")
                f.close()
                Proyectos.objects.create(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea), path=str(dirname), status=1)
                return HttpResponse('Proyecto Creado', status=201)
        else:
            project = Proyectos.objects.filter(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea), status=1).first()
            if project is not None:
                return HttpResponse('Tarea Ya Existe', status=201)
            else:
                return HttpResponse('Nombre de Proyecto ya Usado', status=501)

def deleteproject(request, exp, gpo, tarea):
    if request and request.method == 'GET':
        dirname = settings.MEDIA_ROOT+"/documents/"+exp+"/"+gpo+"/"+tarea+"/"
        if not os.path.exists(dirname):
            return HttpResponse('Nombre de Proyecto no encontrado', status=501)
        else:
            project = Proyectos.objects.get(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea))
            project.status = 0
            project.save()
            return HttpResponse('Proyecto Borrado', status=200)

def getproject(request, exp, gpo, tarea):
    if request and request.method == 'GET':
        dirname = settings.MEDIA_ROOT+"/documents/"+exp+"/"+gpo+"/"+tarea+"/"
        if not os.path.exists(dirname):
            return HttpResponse('Nombre de Proyecto no encontrado', status=501)
        else:
            try:
                project = Proyectos.objects.get(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea))
                if project.status == 0:
                    return HttpResponse('Proyecto No Accesible', status=501)
                else:
                    files = []
                    for file in os.listdir(str(dirname)):
                        fileName = os.path.splitext(file)[0]
                        files.append(fileName)
                    data = {"records":[{"project":files}]}
                    return JsonResponse(data, status=200, safe=False)
            except:
                files = []
                for file in os.listdir(str(dirname)):
                    fileName = os.path.splitext(file)[0]
                    files.append(fileName)
                data = {"records":[{"project":files}]}
                return JsonResponse(data, status=200, safe=False)


def renamefile(request, exp, gpo, tarea, fileName, newFileName):
    if request and request.method == 'GET':
        dirname = settings.MEDIA_ROOT + "/documents/"+exp+"/"+gpo+"/"+tarea+"/"
        if not os.path.exists(os.path.join(dirname, str(fileName+".py"))):
            return HttpResponse('Archivo no encontrado', status=501)
        else:
            try:
                project = Proyectos.objects.get(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea))
                if project.status == 0:
                    return HttpResponse('Proyecto No Accesible', status=501)
                else:
                    for filename in os.listdir(dirname):
                        if filename.startswith(str(fileName)):
                            os.rename(os.path.join(dirname, str(fileName + ".py")),
                                      os.path.join(dirname, str(newFileName + ".py")))
                            return HttpResponse('Nombre de Archivo Cambiado', status=200)

            except:
                for filename in os.listdir(dirname):
                    if filename.startswith(str(fileName)):
                        os.rename(os.path.join(dirname, str(fileName+".py")) , os.path.join(dirname, str(newFileName+".py")))
                        return HttpResponse('Nombre de Archivo Cambiado', status=200)


def getfile(request, exp, gpo, tarea, fileName):
    if request and request.method == 'GET':
        dirname = settings.MEDIA_ROOT+"/documents/"+exp+"/"+gpo+"/"+tarea+"/"
        if not os.path.exists(os.path.join(dirname, str(fileName+".py"))):
            return HttpResponse('Archivo no existe', status=501)
        else:
            try:
                project = Proyectos.objects.get(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea))
                if project.status == 0:
                    return HttpResponse('Proyecto No Accesible', status=501)
            except:
                for file in os.listdir(dirname):
                    if file.startswith(str(fileName)):
                        f = open(os.path.join(dirname, str(fileName+".py")), "r")
                        code = str(f.readlines())
                        f.close()
                        return HttpResponse(code, status=200)

def getcode(request, exp, gpo, tarea, fileName):
    if request and request.method == 'GET':
        dirname = settings.MEDIA_ROOT+"/documents/"+exp+"/"+gpo+"/"+tarea+"/"
        if not os.path.exists(os.path.join(dirname, str(fileName+".py"))):
            return HttpResponse('Archivo no existe', status=501)
        else:
            try:
                project = Proyectos.objects.get(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea))
                if project.status == 0:
                    return HttpResponse('Proyecto No Accesible', status=501)
                else:
                    for file in os.listdir(dirname):
                        if file.startswith(str(fileName)):
                            f = open(os.path.join(dirname, str(fileName + ".py")), "r")
                            code = f.read()
                            f.close()
                            data = {"records": [{"code": code}]}
                            return JsonResponse(data, status=200, safe=False)
            except:
                for file in os.listdir(dirname):
                    if file.startswith(str(fileName)):
                        f = open(os.path.join(dirname, str(fileName+".py")), "r")
                        code = f.read()
                        data = {"records": [{"code": code}]}
                        return JsonResponse(data, status=200, safe=False)

@csrf_exempt
def savefile(request, exp, gpo, tarea,fileName):
    in_data= json.loads(request.body)
    code =  in_data.get('c')
    dirname = settings.MEDIA_ROOT + "/documents/"+exp+"/"+gpo+"/"+tarea+"/"
    if not os.path.exists(os.path.join(dirname, str(fileName+".py"))):
        return HttpResponse('Archivo no existe', status=501)
    else:
        try:
            project = Proyectos.objects.get(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea))
            if project.status == 0:
                return HttpResponse('Proyecto No Accesible', status=501)
            else:
                for file in os.listdir(dirname):
                    if file.startswith(str(fileName)):
                        f = open(os.path.join(dirname, str(fileName+".py")), "w")
                        f.write(code)
                        f.close()
                        return HttpResponse('Archivo Guardado', status=200)
        except:
            for file in os.listdir(dirname):
                if file.startswith(str(fileName)):
                    f = open(os.path.join(dirname, str(fileName+".py")), "w")
                    f.write(code)
                    f.close()
                    return HttpResponse('Archivo Guardado', status=200)

def createfile(request, exp, gpo, tarea, fileName):
    if request and request.method == 'GET':
        dirname = settings.MEDIA_ROOT+"/documents/"+exp+"/"+gpo+"/"+tarea+"/"
        if os.path.exists(os.path.join(dirname, str(fileName+".py"))):
            return HttpResponse('Archivo ya existe', status=501)
        else:
            try:
                project = Proyectos.objects.get(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea))
                if project.status == 0:
                    return HttpResponse('Proyecto No Accesible', status=501)
                else:
                    fileN = str(dirname + fileName +".py")
                    f = open(fileN, "w+")
                    f.close()
                    return HttpResponse('Archivo Creado', status=200)

            except:
                fileN = str(dirname + fileName +".py")
                f = open(fileN, "w+")
                f.close()
                return HttpResponse('Archivo Creado', status=200)

def deletefile(request, exp, gpo, tarea, fileName):
    if request and request.method == 'GET':
        dirname = settings.MEDIA_ROOT+"/documents/"+exp+"/"+gpo+"/"+tarea+"/"
        if not os.path.exists(os.path.join(dirname, str(fileName+".py"))):
            return HttpResponse('Archivo no existe', status=501)
        else:
            try:
                project = Proyectos.objects.get(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea))
                if project.status == 0:
                    return HttpResponse('Proyecto No Accesible', status=501)
                else:
                    for file in os.listdir(dirname):
                        if file.startswith(str(fileName)):
                            os.remove(os.path.join(dirname, str(fileName + ".py")))
                            return HttpResponse('Archivo Borrado', status=200)

            except:
                for file in os.listdir(dirname):
                    if file.startswith(str(fileName)):
                        os.remove(os.path.join(dirname, str(fileName + ".py")))
                        return HttpResponse('Archivo Borrado', status=200)


def getpath(request, exp, gpo, tarea):
    if request and request.method == 'GET':
        dirname = settings.MEDIA_ROOT+"/documents/"+exp+"/"+gpo+"/"+tarea+"/"
        if os.path.exists(dirname):
            data= {"records":[{"path":dirname}]}
            return JsonResponse(data, status=200, safe=False)
        else:
            return HttpResponse('Tarea No Creada', status=501)
