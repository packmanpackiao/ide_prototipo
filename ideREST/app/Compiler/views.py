from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from os import listdir
from os.path import isfile, join
import json, shutil, subprocess

# Create your views here.

@csrf_exempt
def VistaAnalisis(request):
	if request.method == "POST":
		datos = json.loads(request.body)
		directorio = datos['dir']
		listaanalisis = []
		strcompilacion = ""
		listacompilaciones = []
		tiempos = []

		for archivo in listdir(directorio):
			if isfile(join(directorio, archivo)):
				if archivo[-2:] == "py":
					analisis = subprocess.Popen('/bin/bash', stdin = subprocess.PIPE, stdout = subprocess.PIPE)
					analisisout, analisiserr = analisis.communicate('cd ' + directorio + '&& pylint ' + archivo)
					listaanalisis.append([archivo, analisisout])

		analisislimpios = []

		for analisis in listaanalisis:
			analisislimpios.append([analisis[0], analisis[1].splitlines()])

		stranalisis = ""
		errores = 0
		for analisis in analisislimpios:
			stranalisis = stranalisis + "Archivo: " + analisis[0] + "<br>"
			for linea in analisis[1][1:-4]:
				stranalisis = stranalisis + linea + "<br>"
				if linea[:1] == "E":
					errores = errores + 1

		if errores == 0:
			for archivo in listdir(directorio):
				if isfile(join(directorio, archivo)):
					if archivo[-2:] == "py":
						compilacion = subprocess.Popen('/bin/bash', stdin = subprocess.PIPE, stdout = subprocess.PIPE)
						compilacionout, compilacionerr = compilacion.communicate('cd ' + directorio + '&& python -m cProfile ' + archivo)
						listacompilaciones.append([archivo, compilacionout])

			compilacioneslimpios = []
			for compilacion in listacompilaciones:
				compilacioneslimpios.append([compilacion[0], compilacion[1].splitlines()]);

			for compilacion in compilacioneslimpios:
				flag = True
				resultados = ""
				for linea in compilacion[1]:
					if linea[-7:] == "seconds":
						tiempos.append(linea)
						flag = False
					if flag == True:
						resultados = resultados + linea + "<br>"
				strcompilacion = strcompilacion + resultados
			indice = 0
			tiempocompilacion = 0
			for tiempo in tiempos:
				if tiempo[9:].split(" ")[0] > indice:
					indice = tiempo[9:].split(" ")[0]
					tiempocompilacion = tiempo[9:].split(" ")[4]

			tiempos = tiempocompilacion



		data = [{
			"analisis":stranalisis,
			"compilacion":strcompilacion,
			"errores": errores,
			"tiempo": tiempos

		}]
		return JsonResponse(data, safe=False, status=200)
	else:
		data = "Error"
		return HttpResponse(json.dumps(data))
