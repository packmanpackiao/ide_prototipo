from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

class Tipo_User(models.Model):
    tipo = models.CharField(max_length=30)

class Usuarios(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    tipoUser = models.ForeignKey(Tipo_User, on_delete=models.CASCADE)
    institucion = models.CharField(max_length=100)
    semestre = models.CharField(max_length=30)
    termino_registro = models.DateTimeField(auto_now_add=True)
    expediente = models.CharField(max_length=30)
    estatus = models.IntegerField()
    tema_ide = models.IntegerField(default=14)

class Grupos(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    grupos = models.CharField(max_length=150)
    descripcion = models.CharField(max_length=500)
    pass_grupo = models.CharField(max_length=30)
    activo = models.BooleanField(default=True)

class Usuario_Grupos(models.Model):
    id_usuario = models.ForeignKey(User , on_delete=models.CASCADE)
    id_grupo = models.ForeignKey(Grupos , on_delete=models.CASCADE)

class Tarea(models.Model):
    nombre_tarea = models.CharField(max_length=30)
    descripcion_tarea = models.TextField()
    fecha_inicio = models.DateTimeField()
    fecha_fin = models.DateTimeField()
    grupo = models.ForeignKey(Grupos, on_delete=models.CASCADE)

class Usuario_Tareas(models.Model):
    id_usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    id_tarea = models.ForeignKey(Tarea, on_delete=models.CASCADE)
    id_grupo = models.ForeignKey(Grupos, on_delete=models.CASCADE)
    calificacion = models.FloatField()
    observaciones = models.CharField(max_length=200)
    estatus = models.CharField(max_length=20, default ='')
    fecha_entrega = models.DateTimeField()


class Catalogo_Metricas(models.Model):
    categoria = models.CharField(max_length=100)
    nombre_metrica = models.CharField(max_length=100)
    descripcion_metrica = models.TextField()
    tieneIndicadores = models.BooleanField(default = False)
    indicadorAlto = models.IntegerField( default=0)
    indicadorBajo = models.IntegerField(default =0)

class Metricas(models.Model):
    id_usuario = models.ForeignKey(User , on_delete=models.CASCADE)
    id_tarea = models.ForeignKey(Tarea, on_delete=models.CASCADE)
    id_grupo =  models.ForeignKey(Grupos, on_delete=models.CASCADE)
    id_metrica =  models.ForeignKey(Catalogo_Metricas, on_delete=models.CASCADE)
    calificacion = models.IntegerField(default = 0)
    registro = models.DateTimeField(auto_now_add=True)

class Tarea_Opciones_IDE(models.Model):
    id_tarea = models.ForeignKey(Tarea, on_delete=models.CASCADE)
    highlihtActiveLine = models.BooleanField()
    bahaviousEnabled = models.BooleanField()
    maxLines = models.IntegerField()
    minLines = models.IntegerField()
    autocompletado_basico = models.BooleanField()
    autocompletado_live = models.BooleanField()
    snippets = models.BooleanField()
    infoToken = models.BooleanField()
    spellcheck = models.BooleanField()
    useElasticTabstops = models.BooleanField()
