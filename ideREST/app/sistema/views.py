import json
import shutil
import os

from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.core import serializers
from app.sistema.models import Usuarios
from django.core.exceptions import ObjectDoesNotExist

from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.conf import settings
from IDE.settings import BASE_DIR

def login_view(request):
    """
        login_view despliega el template de login
    """
    return render(request, "login.html")

def registro_normal(request):
    """
        Registro_normal obtiene el correo y password
        y lo registra en la base de datos.
    """
    if request.method == 'GET':
        email = request.GET['email']
        password = request.GET['password']
        if not User.objects.filter(username=email).exists() :
            user = User.objects.create_user(username=email, password=password)
            return JsonResponse({'mensaje':'Ok'})
        else:
            return JsonResponse({'mensaje':'Correo ya registrado'})
        # user = authenticate(username=username, password=password)
        # if user is not None:
        #     rol = models.Rol.objects.get(usuario_id=user.id)
        #     request.session['usuario_tipo'] = rol.tipo_usuario
        #     return JsonResponse({'mensaje':'Acceso'})
        # else:
        #     return JsonResponse({'mensaje':'Error'})
    else:
        return redirect('/')

def completar_registro_view(request):
    """
        completar_registro redirige de la pagina de login a la pagina para completar el registro
    """
    return render(request, "completar_registro.html")

def completar_registro(request):
    """
        vista que hace la insercion del nuevo usuario
    """
    if request.method == 'GET':
        pnombre = request.GET['nombre']
        papellidos = request.GET['apellidos']
        pinstitucion = request.GET['institucion']
        psemestre = request.GET['semestre']
        pexpediente = request.GET['expediente']
        tipoUser = 1
        usuario = User.objects.get(username = request.GET['correo'])
        User.objects.filter(username = request.GET['correo']).update(first_name = pnombre,last_name = papellidos)
        newUser = Usuarios(user_id=usuario.id , institucion=pinstitucion, semestre=psemestre , expediente=pexpediente , estatus=0 , tipoUser_id=1)
        newUser.save(force_insert=True)
        nueva = os.path.join(BASE_DIR, 'static/perfil/'+request.GET['correo'])
        antigua = os.path.join(BASE_DIR, 'static/perfil/default')
        os.makedirs(nueva, 0777)
        for file in os.listdir(antigua):
            srcfile = os.path.join(BASE_DIR, 'static/perfil/default', file)
            dstfile = os.path.join(BASE_DIR, 'static/perfil/'+request.GET['correo'], file)
            if os.path.isdir(srcfile):
                dir_copy(srcfile, dstfile)
            else: #if the source file path is just a file, copy the file
                shutil.copyfile(srcfile, dstfile)
        #return JsonResponse({'mensaje':usuario.username})
        asunto = "Registro exitoso"
        mensaje = "Bienvenido al IDE, tu registro ha sido exitoso"
        remitente = [usuario.username]
        correo = enviarCorreo(asunto , mensaje , remitente)
        return JsonResponse({'mensaje':'OK'})
    else:
        return JsonResponse({'mensaje':'no se hizo por post'})



def login_autentificar(request):
    """
        login_autentificar obtiene el usuario y password
        busca si se eencuentra registrado o no
        si esta registrado, crea una variable de session
        si no retorna un mensaje de error
    """
    if request.method == 'GET':
        username = request.GET['email']
        password = request.GET['password']
        try:
            user = authenticate(username=username, password=password)
            usuario = User.objects.get(username = request.GET['email'])
            if user is not None:
                # rol = models.Rol.objects.get(usuario_id=user.id)
                # request.session['usuario_tipo'] = rol.tipo_usuario
                #request.session['usuario'] = user.id
                datosUsuario = Usuarios.objects.get(user_id  = usuario.id)
                if datosUsuario.tipoUser_id == 1:
                    tipo = "estudiante"
                elif datosUsuario.tipoUser_id == 2:
                    tipo = "maestro"
                elif datosUsuario.tipoUser_id == 3:
                    tipo = "admin"
                return JsonResponse({'mensaje':'Acceso' , 'idUser':usuario.id , 'tema':datosUsuario.tema_ide, 'correo':  usuario.username , "tipo" : tipo, "nombre" : usuario.first_name , "apellidos" : usuario.last_name , "expediente":datosUsuario.expediente })
            else:
                return JsonResponse({'mensaje':'Error'})
        except ObjectDoesNotExist:
            return JsonResponse({'mensaje':'Error'})
    else:
        return redirect('/')


def login_red_social(request):
    """
        login_autentificar obtiene el usuario y password
        busca si se eencuentra registrado o no
        si esta registrado, crea una variable de session
        si no retorna un mensaje de error
    """
    if request.method == 'GET':
        username = request.GET['email']
        try:
            checkUser = User.objects.get(username=username)
            datosUsuario = Usuarios.objects.get(user_id  = checkUser.id)
            if datosUsuario.tipoUser_id == 1:
                tipo = "estudiante"
            elif datosUsuario.tipoUser_id == 2:
                tipo = "maestro"
            elif datosUsuario.tipoUser_id == 3:
                tipo = "admin"
            return JsonResponse({'mensaje':'Acceso' , 'idUser':checkUser.id , 'correo':  checkUser.username , "tipo" : tipo, "nombre" : checkUser.first_name , "apellidos" : checkUser.last_name , "expediente":datosUsuario.expediente  })
        except ObjectDoesNotExist:
            return JsonResponse({'mensaje':'No Acceso'})
    else:
        return redirect('/')

def cerrar_sesion(request):
    """
        cerrar_sesion cierra la session actual
        eliminando la variable de session
    """
    if 'usuario_tipo' in request.session:
        del request.session['usuario']
    return redirect('/')



def enviarCorreo(asunto , mensaje , remitente):
    mail = EmailMessage(asunto , mensaje , settings.EMAIL_HOST_USER , remitente)
    mail.send()
    return "exito"

def sendMail(request):
    mail = EmailMessage('prueba' , 'prueba de correo' , settings.EMAIL_HOST_USER , to=['selbor43@gmail.com'])
    mail.send()
    return HttpResponse("ok")
