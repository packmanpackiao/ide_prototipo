"""ide URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from app.sistema import views
urlpatterns = [
    url(r'^$', views.login_view, name="login_view"),
    url(r'^registro/normal$', views.registro_normal, name="registro_normal"),
    url(r'^registro/completar_registro_view$', views.completar_registro_view, name="completar_registro_view"),
    url(r'^registro/completar_registro$', views.completar_registro, name="completar_registro"),
    url(r'^login/normal$', views.login_autentificar, name="login_autentificar"),
    url(r'^login/redSocial$', views.login_red_social, name="login_red_social"),
    url(r'^cerrar_sesion$', views.cerrar_sesion, name="cerrar_sesion"),
    url(r'^sendMail$', views.sendMail, name="sendMail"),
]
