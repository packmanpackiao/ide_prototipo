"""ide URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from app.grupos import views
urlpatterns = [
    url(r'^grupos/getGrupos$', views.getGrupos, name="grupos_getGrupos"),
    url(r'^grupos/getAllGrupos$', views.getAllGrupos, name="grupos_getAllGrupos"),
    url(r'^grupos/getAllGruposMaestro$', views.get_grupo_maestros, name="get_grupo_maestros"),
    url(r'^grupos/getAllGruposAlumno$', views.getAllGruposAlumno, name="getAllGruposAlumno"),
    url(r'^grupos/get_alumno_evaluacion$', views.get_alumno_evaluacion, name="get_alumno_evaluacion"),
    url(r'^grupos/get_datos_grupo$', views.datos_grupo, name="get_datos_grupo"),
    url(r'^grupos/renovar_clave_grupo$', views.renovar_clave_grupo, name="renovar_clave_grupo"),
    url(r'^grupos/detalles_grupo_cve$', views.detalles_grupo_cve, name="detalles_grupo_cve"),
    url(r'^grupos/get_alumnos_grupo$', views.alumnos_grupo, name="get_alumnos_grupo"),
    url(r'^grupos/get_alumnos_maestro$', views.get_alumnos_maestro, name="get_alumnos_maestro"),
    url(r'^grupos/eliminar_alumno_grupo$', views.eliminar_alumno_grupo, name="eliminar_alumno_grupo"),
    url(r'^grupos/agregar_participante_grupo$', views.agregar_participante_grupo, name="agregar_participante_grupo"),
    url(r'^grupos/insertGrupo$', views.insertGrupo, name="grupos_insertGrupo"),
    url(r'^grupos/deleteGrupo$', views.delete_grupo, name="grupos_deleteGrupo"),
    url(r'^grupos/update_grupo$', views.update_grupo, name="grupos_update_grupos"),
    url(r'^grupos/cambiarResponsableGrupo$', views.cambiarResponsableDeGrupo, name="grupos_cambiarResponsableDeGrupo"),
    url(r'^grupos/filtrarGruposPorAlumno$', views.filtrarGruposPorAlumno, name="grupos_filtrarGruposPorAlumno"),
    url(r'^grupos/filtrarGruposPorMaestro$', views.filtrarGruposPorMaestro, name="grupos_filtrarGruposPorMaestro"),
    
]
