angular.module('IDE.contIde', [])

.controller('ctrlIDE', function($scope, $http, $location, $timeout, GetFiles, $window)
{
  $scope.ContadorOperadoresDistintos = 0;
  $scope.ContadorOperandosDistintos = 0;
  $scope.SumaContadorOperadores = 0;
  $scope.SumaContadorOperandos = 0;
  $scope.vocabulario = 0;
  $scope.longitud = 0;
  $scope.volumen = 0;
  $scope.volumenPotencial = 0;
  $scope.nivel = 0;
  $scope.dificultad = 0;
  $scope.inteligencia = 0;
  $scope.esfuerzo = 0;
  $scope.elapsed = 0;
  $scope.tiempoPruebas = 0;
  $scope.tiempoCorreciones = 0;
  $scope.tiempoTotalProducto = 0;
  $scope.LOCbaseOriginal = 0;
  $scope.LOCagregado = 0;
  $scope.LOCmodificado = 0;
  $scope.LOCsuprimido = 0;
  $scope.LOCbaseActual = 0;
  $scope.defectos = 0;
  $scope.defectosCorregidos = 0;
  $scope.defectosNoCorregidos = 0;
  $scope.defectosPorTamano = 0;
  $scope.defectosPorHora = 0;
  $scope.defectosCorregidosHora = 0;
  $scope.defectosPorHoraEnPruebas = 0;
  $scope.densidadDeDefectos = 0;
  $scope.complejidadCiclo = 0;

   $scope.Acethemes = [
    "chrome",
    "clouds",
    "crimson_editor",
    "dawn",
    "dreamweaver",
    "eclipse",
    "github",
    "solarized_light",
    "textmate",
    "tomorrow",
    "xcode",
    "kuroir",
    "katzenmilch",
    "ambiance",
    "chaos",
    "clouds_midnight",
    "cobalt",
    "idle_fingers",
    "kr_theme",
    "merbivore",
    "merbivore_soft",
    "mono_industrial",
    "monokai",
    "pastel_on_dark",
    "solarized_dark",
    "terminal",
    "tomorrow_night",
    "tomorrow_night_blue",
    "tomorrow_night_bright",
    "tomorrow_night_eighties",
    "twilight",
    "vibrant_ink",
  ];
  $scope.themes = [
    {id:0, theme:"chrome"},
    {id:1, theme:"clouds"},
    {id:2, theme:"crimson_editor"},
    {id:3, theme:"dawn"},
    {id:4, theme:"dreamweaver"},
    {id:5, theme:"eclipse"},
    {id:6, theme:"github"},
    {id:7, theme:"solarized_light"},
    {id:8, theme:"textmate"},
    {id:9, theme:"tomorrow"},
    {id:10, theme:"xcode"},
    {id:11, theme:"kuroir"},
    {id:12, theme:"katzenmilch"},
    {id:13, theme:"ambiance"},
    {id:14, theme:"chaos"},
    {id:15, theme:"cobalt"},
    {id:16, theme:"idle_fingers"},
    {id:17, theme:"kr_theme"},
    {id:18, theme:"merbivore"},
    {id:19, theme:"merbivore_soft"},
    {id:20, theme:"mono_industrial"},
    {id:21, theme:"clouds_midnight"},
    {id:22, theme:"monokai"},
    {id:23, theme:"pastel_on_dark"},
    {id:24, theme:"solarized_dark"},
    {id:25, theme:"terminal"},
    {id:26, theme:"tomorrow_night"},
    {id:27, theme:"tomorrow_night_blue"},
    {id:28, theme:"tomorrow_night_bright"},
    {id:29, theme:"tomorrow_night_eighties"},
    {id:30, theme:"twilight"},
    {id:31, theme:"vibrant_ink"},
  ];

  var modes = [
    'abap',
    'actionscript',
    'ada',
    'apache_conf',
    'asciidoc',
    'assembly_x86',
    'autohotkey',
    'batchfile',
    'c9search',
    'c_cpp',
    'cirru',
    'clojure',
    'cobol',
    'coffee',
    'coldfusion',
    'csharp',
    'css',
    'curly',
    'd',
    'dart',
    'diff',
    'dockerfile',
    'dot',
    'dummy',
    'dummysyntax',
    'eiffel',
    'ejs',
    'elixir',
    'elm',
    'erlang',
    'forth',
    'ftl',
    'gcode',
    'gherkin',
    'gitignore',
    'glsl',
    'golang',
    'groovy',
    'haml',
    'handlebars',
    'haskell',
    'haxe',
    'html',
    'html_ruby',
    'ini',
    'io',
    'jack',
    'jade',
    'java',
    'javascript',
    'json',
    'jsoniq',
    'jsp',
    'jsx',
    'julia',
    'latex',
    'less',
    'liquid',
    'lisp',
    'livescript',
    'logiql',
    'lsl',
    'lua',
    'luapage',
    'lucene',
    'makefile',
    'markdown',
    'matlab',
    'mel',
    'mushcode',
    'mysql',
    'nix',
    'objectivec',
    'ocaml',
    'pascal',
    'perl',
    'pgsql',
    'php',
    'powershell',
    'praat',
    'prolog',
    'properties',
    'protobuf',
    'python',
    'r',
    'rdoc',
    'rhtml',
    'ruby',
    'rust',
    'sass',
    'scad',
    'scala',
    'scheme',
    'scss',
    'sh',
    'sjs',
    'smarty',
    'snippets',
    'soy_template',
    'space',
    'sql',
    'stylus',
    'svg',
    'tcl',
    'tex',
    'text',
    'textile',
    'toml',
    'twig',
    'typescript',
    'vala',
    'vbscript',
    'velocity',
    'verilog',
    'vhdl',
    'xml',
    'xquery',
    'yaml',
];

    $scope.tokenInformation = "";


    //if tiempo transcurrido existe entonces
    // $scope.star = tiempoTranscurrido;
    //si no existe se crea uno nuevo
    $scope.start = new Date(); // para timer
    //fin
    $scope.tiempoPruebas = 60; //sumatoria de lo obtenido del compilador
    $scope.tiempoTOTAL = 0;//solo del proyecto
    $scope.tiempoCorreciones = 60;
    $scope.tiempoTotalProducto = 0;//tiempo total


    $scope.codigo = "#" + $scope.usuario + " " + $scope.exp + "\nB = 1"
      +"\ny =2 "
      +"\nB =246524"
      +"\ny = 1"
      +"\nx = 246524"
      +"\nB = 2"
      +"\nBxy";

    $scope.operandos = []; // -> arrayOperandos
    $scope.operandos2 = []; // -> arrayOperandos
    $scope.ContadorOperandos = [];

    //guardar solo estos primeros 4
    $scope.SumaContadorOperadores      = 0;
    $scope.ContadorOperadoresDistintos = 0;
    $scope.SumaContadorOperandos       = 0;
    $scope.ContadorOperandosDistintos  = 0;

    $scope.vocabulario      = 0;
    $scope.longitud         = 0;
    $scope.volumen          = 0;
    $scope.volumenPotencial = 0;
    $scope.nivel            = 0;
    $scope.dificultad       = 0;
    $scope.inteligencia     = 0;
    $scope.esfuerzo         = 0;

    $scope.LOCbaseOriginal = 0; //si es un programa ya hecho
    $scope.LOCbaseActual   = 0; //se actualiza según se escriba
    $scope.LOCagregado     = 0;
    $scope.LOCsuprimido    = 0;
    $scope.modificado      = 0; //--falta


    //se tiene que obtener los datos de las salidas de cheko
    $scope.defectos                         = 10; // D
    $scope.defectosCorregidos               = 6;  // DC
    $scope.defectosNoCorregidos             = 0;  // DENC = D - DC
    $scope.defectosPorTamano                = 0;  // DT = D/LOC
    $scope.defectosPorHora                  = 0;  // DEH = D/TT*60
    $scope.defectosCorregidosHora           = 0;  // DCH = DC/TT*60
    $scope.defectosPorHoraEnPruebas         = 0;  // DEHprueba = D/TP*60
    $scope.defectosCorregidosEnCorrecciones = 0;  // DEHCorrecciones = DC/TC*60

    $scope.densidadDeDefectos = 0; //Dd = 1000 * D / LOC

    //tiempos
    $scope.elapsedOriginal    = 0; // si ya existía un tiempo transcurrido aquí se debe guardar
    $scope.elapsed            = 0;

    $scope.complejidadCiclo = 0;

    //si se define un número máximo de lineas
    $scope.maximoLineas = 10000;

    var tiposDeVariable=[];
    //var contarTipos=[];


    var _session = null;

    $scope.tokenInformation = "";
    $scope.aceLoaded = function (_editor)

    {
     var _session = _editor.getSession();
    var _renderer = _editor.renderer;

    // Options
     _editor.$blockScrolling = Infinity;
    _editor.setShowPrintMargin(false);

    // Events
     _editor.on('mousedown', function(e) {
      var row = e.getDocumentPosition().row;
      var col = e.getDocumentPosition().column;
      var token = _editor.session.getTokenAt(row, col);
      //$scope.STInfo(String(token.type));
    });

     //detecta cualquier cambio en el editor
     _editor.on("change", function(e)
     {

       $scope.uptdateCodigo(String(_editor.session.getValue()));

      $scope.LOCbaseActual = _editor.session.getLength();
      if(! $scope.LOCbaseActual != $scope.LOCbaseOriginal ) console.log("equisde");
        else $scope.LOCbaseOriginal = $scope.LOCbaseActual;


      var codigo = _editor.getValue();

      var currline = _editor.getSelectionRange().start.row;
      var lineaActual = _editor.session.getLine(currline);

      //console.log(codigo);

      //////////////////////////////////////////////////////////////////////////////
      /***
        Contador de operaciones
      */
      $scope.aCO = {}; // -> arrayContadorOperadores


      //console.clear();

      //guarda las variables
       lineaActual = lineaActual.replace(/^\s+|\s+$/g, "");
       if(lineaActual.charAt(0).trim() != "#"){ // si no es un comentario
         if(~lineaActual.indexOf("=") && !~lineaActual.indexOf("==")){ // detecta la asignación de un valor
           var variable = lineaActual.split("=")[0].trim(); // obtiene todo lo que está antes de un signo de igual

             //para obtener los tipos de variables
             var tipoVariable = lineaActual.split("=")[1].trim(); // obtiene todo lo que está después de un signo de igual
             var charAux = tipoVariable.charAt(0);
             if((charAux == "'" || charAux == '"') && !~tiposDeVariable.indexOf('string')) tiposDeVariable.push("string");
             else if((charAux == "[") && !~tiposDeVariable.indexOf('list')) tiposDeVariable.push("list");
             else if((charAux == "(") && !~tiposDeVariable.indexOf('tuple')) tiposDeVariable.push("tuple");
             else if((charAux == "{") && !~tiposDeVariable.indexOf('dictionary')) tiposDeVariable.push("dictionary");
             else if((!isNaN(charAux)) && !~tiposDeVariable.indexOf('numeric')) tiposDeVariable.push("numeric");


           var varTemp = variable.split(" ", -1); //-> obtiene cualquier cosa antes de un signo de igual
           varTemp = varTemp.pop();
           var a = $scope.operandos.indexOf(varTemp);  //busca la variable en el arreglo existente
             $scope.operandos2.push(varTemp);
             if(!~a && varTemp != "")                 // si no existe aún la variable, la agrega
                 $scope.operandos.push(varTemp);
         }else{
           //console.log("No variables found");
         }
       }

       tiposDeVariable.sort();
       console.log(tiposDeVariable);
       console.log("cantidad de variables: " + tiposDeVariable.length);
       $scope.ContadorOperandosDistintos = tiposDeVariable.length;


      //guarda las variables
      lineaActual = lineaActual.replace(/^\s+|\s+$/g, "");
      if(lineaActual.charAt(0).trim() != "#")
      { // si no es un comentario
        if(~lineaActual.indexOf("=") && !~lineaActual.indexOf("=="))
        { // detecta la asignación de un valor
          var variable = lineaActual.split("=")[0].trim(); // obtiene todo lo que está antes de un signo de igual
          var varTemp = variable.split(" ", -1); //-> obtiene cualquier cosa antes de un signo de igual
          varTemp = varTemp.pop();
          var a = $scope.operandos.indexOf(varTemp);  //busca la variable en el arreglo existente
            $scope.operandos2.push(varTemp);
            if(!~a && varTemp != "")                 // si no existe aún la variable, la agrega
                $scope.operandos.push(varTemp);
        }else{
          //console.log("No variables found");
        }
      }


      var contador;
      //esto busca las veces que se ha usado una variable guardada y las cuenta
      var aux = "";
      var cantidadOperandos = 0;
      $scope.ContadorOperandos = [];

      for(i = 0; i < $scope.operandos.length; i++ ){
        aux = $scope.operandos[i];
        cantidadOperandos = countInstances(codigo, aux);
        $scope.ContadorOperandos.push(cantidadOperandos);
      }

        var key = e.which;
        var text = String.fromCharCode(key);
        switch(key)
        {
            case 13: //Tab
                console.log('equisdedededede'); // manually add tab
                //Code to store this event for replay later
                break;
        }

        /////////////////////////////////////////////////////

            document.onkeyup = function(e){
                var keycode = (e === null) ? window.event.keyCode : e.which;
                if(keycode === 13) {
                  if($scope.LOCbaseActual > $scope.maximoLineas){
                    alert("Lineas excedidas");
                  }
                  else {
                    $scope.saveCode();
                  }


                } else{
                  console.log("No es enter :'v");
                }
            }

        /////////////////////////////////////////////////////




      $scope.aCO['ContadorFindMas']        = countInstances(codigo,"+");
      $scope.aCO['ContadorFindMenos']      = countInstances(codigo,"-");
      $scope.aCO['ContadorFindPor']        = countInstances(codigo,"*");
      $scope.aCO['ContadorFindEntre']      = countInstances(codigo,"/");
      $scope.aCO['ContadorFindPotencia']   = countInstances(codigo,"**");
      $scope.aCO['ContadorFindModulo']     = countInstances(codigo,"%");

      $scope.aCO['ContadorFindOR']         = countInstances(codigo," or ");
      $scope.aCO['ContadorFindAND']        = countInstances(codigo," and ");
      $scope.aCO['ContadorFindNOT']        = countInstances(codigo," not ");

      $scope.aCO['ContadorFindCompara']    = countInstances(codigo,"==");
      $scope.aCO['ContadorFindAsigna']     = countInstances(codigo,"=");
      $scope.aCO['ContadorFindMayor']      = countInstances(codigo,">");
      $scope.aCO['ContadorFindMenor']      = countInstances(codigo,"<");
      $scope.aCO['ContadorFindDiferente']  = countInstances(codigo,"!=");
      $scope.aCO['ContadorFindMayorIgual'] = countInstances(codigo,">=");
      $scope.aCO['ContadorFindMenorIgual'] = countInstances(codigo,"<=");

      imprimir();

      function countInstances(str, value) {
        return (str.split(value).length - 1);
      }

      // tiempo transcurrido
      $scope.elapsed = (new Date() - $scope.start) / 1000;

      //Complejidad Ciclomática
      if(lineaActual.charAt(0).trim() != "#")
      {
        var complejidadIf    = countInstances(codigo, "if");
        var complejidadElse  = countInstances(codigo, "else");
        var complejidadFor   = countInstances(codigo, "for");
        var complejidadWhile = countInstances(codigo, "while");
      }
      $scope.complejidadCiclo = complejidadIf + complejidadWhile + complejidadElse + complejidadFor;


      // defectos :v
      //los if son para evitar divisiones entre 0
      $scope.defectosNoCorregidos = $scope.defectos - $scope.defectosCorregidos;
      if($scope.LOCbaseActual != 0){
        $scope.defectosPorTamano = $scope.defectos / $scope.LOCbaseActual;
        $scope.densidadDeDefectos = 1000 * $scope.defectos / $scope.LOCbaseActual;
      }

      if($scope.elapsed + $scope.elapsedOriginal != 0)
        $scope.defectosPorHora = $scope.defectos/($scope.elapsed + $scope.elapsedOriginal) * 60;

      if($scope.elapsed + $scope.elapsedOriginal != 0)
        $scope.defectosCorregidosHora = $scope.defectosCorregidos/($scope.elapsed + $scope.elapsedOriginal) *60;

      if($scope.tiempoPruebas != 0)
        $scope.defectosPorHoraEnPruebas = $scope.defectos/$scope.tiempoPruebas*60;

      if($scope.tiempoCorreciones != 0)
        $scope.defectosCorregidosEnCorrecciones = $scope.defectosCorregidos/$scope.tiempoCorreciones*60;


      $scope.tiempoTotalProducto =  ($scope.elapsed + $scope.elapsedOriginal) + $scope.tiempoPruebas + $scope.tiempoCorreciones;






        function imprimir()
        {
          //console.clear();
        /*
          console.log("tiempo transcurrido:     " + ($scope.elapsed + $scope.elapsedOriginal) );
          console.log("Complejidad Ciclomática: " + $scope.complejidadCiclo);
          console.log("Defectos-----------------------------------------");
          console.log("Defectos: "+$scope.defectos);
          console.log("Corregidos: "+$scope.defectosCorregidos);
          console.log("No corregidos: "+$scope.defectosNoCorregidos);
          console.log("Por tamaño: "+$scope.defectosPorTamano);
          console.log("Defectos por hora: "+$scope.defectosPorHora);
          console.log("Corregidos por hora: "+$scope.defectosCorregidosHora);
          console.log("Defectos*Hora en Prueba: "+$scope.defectosPorHoraEnPruebas);
          console.log("Defectos corregidos en en tiempo de correciones: "+$scope.defectosCorregidosEnCorrecciones);
          console.log("Densidad De Defectos: "+$scope.densidadDeDefectos);
          console.log("Defectos-----------------------------------------");
*/
          //tiempo TOTAL
          //$scope.TOTAL = $scope.elapsed + $scope.tiempoPruebas + $scope.tiempoCorreciones

          //reset de valores
          $scope.SumaContadorOperadores      = 0;
          $scope.ContadorOperadoresDistintos = 0;
          $scope.SumaContadorOperandos       = 0;
          $scope.ContadorOperandosDistintos  = 0;

          $scope.vocabulario = 0;
          $scope.longitud    = 0;
          $scope.volumen     = 0;

          console.log($scope.operandos);
          console.log($scope.ContadorOperandos);

          if($scope.aCO['ContadorFindCompara']   >=1) $scope.aCO['ContadorFindAsigna'] -= 2*$scope.aCO['ContadorFindCompara'];
          if($scope.aCO['ContadorFindDiferente'] >=1) $scope.aCO['ContadorFindAsigna'] -= $scope.aCO['ContadorFindDiferente'];
          if($scope.aCO['ContadorFindPotencia']  >=1) $scope.aCO['ContadorFindPor']    -= 2*$scope.aCO['ContadorFindPotencia'];
          if($scope.aCO['ContadorFindMayorIgual']>=1){$scope.aCO['ContadorFindAsigna'] -= $scope.aCO['ContadorFindMayorIgual'];
                                               $scope.aCO['ContadorFindMayor']  -= $scope.aCO['ContadorFindMayorIgual'];}
          if($scope.aCO['ContadorFindMenorIgual']>=1){$scope.aCO['ContadorFindAsigna'] -= $scope.aCO['ContadorFindMenorIgual'];
                                               $scope.aCO['ContadorFindMenor']  -= $scope.aCO['ContadorFindMenorIgual'];}

          if($scope.LOCbaseOriginal == $scope.LOCbaseActual){
            $scope.LOCagregado  = 0;
            $scope.LOCsuprimido = 0;
          }else  if($scope.LOCbaseOriginal < $scope.LOCbaseActual)
              $scope.LOCagregado = $scope.LOCbaseActual - $scope.LOCbaseOriginal;
            else
              $scope.LOCsuprimido = $scope.LOCbaseOriginal - $scope.LOCbaseActual;
          if($scope.LOCbaseActual !== $scope.LOCbaseOriginal){
              $scope.LOCmodificado = Math.abs($scope.LOCbaseActual - $scope.LOCbaseOriginal);
          }

          //alert(codigo);
          /*
          console.log( "mas        =" + $scope.aCO['ContadorFindMas']);
          console.log( "menos      =" + $scope.aCO['ContadorFindMenos']);
          console.log( "por        =" + $scope.aCO['ContadorFindPor']);
          console.log( "entre      =" + $scope.aCO['ContadorFindEntre']);
          console.log( "potencia   =" + $scope.aCO['ContadorFindPotencia']);
          console.log( "modulo     =" + $scope.aCO['ContadorFindModulo']);
          console.log( "or         =" + $scope.aCO['ContadorFindOR']);
          console.log( "and        =" + $scope.aCO['ContadorFindAND']);
          console.log( "not        =" + $scope.aCO['ContadorFindNOT']);
          console.log( "compara    =" + $scope.aCO['ContadorFindCompara']);
          console.log( "asigna     =" + $scope.aCO['ContadorFindAsigna']);
          console.log( "mayor      =" + $scope.aCO['ContadorFindMayor']);
          console.log( "menor      =" + $scope.aCO['ContadorFindMenor']);
          console.log( "diferente  =" + $scope.aCO['ContadorFindDiferente']);
          console.log( "mayorigual =" + $scope.aCO['ContadorFindMayorIgual']);
          console.log( "menorigual =" + $scope.aCO['ContadorFindMenorIgual']);*/

          for( var contador in $scope.aCO ){
              $scope.SumaContadorOperadores += $scope.aCO[contador];
              if($scope.aCO[contador] != 0) $scope.ContadorOperadoresDistintos ++;
          }

          for(i = 0; i < $scope.ContadorOperandos.length; i++ ){
            $scope.SumaContadorOperandos += $scope.ContadorOperandos[i];
            if($scope.ContadorOperandos[i] != 0) $scope.ContadorOperandosDistintos ++;
          }


          $scope.vocabulario      = obtenerVocabulario();
          $scope.longitud         = obtenerLongitud();
          $scope.volumen          = obtenerVolumen();
          $scope.volumenPotencial = obtenerVolumenPotencial();
          $scope.nivel            = obtenerNivel();
          $scope.dificultad       = obtenerDificultad();
          $scope.inteligencia     = obtenerInteligencia();
          $scope.esfuerzo         = obtenerEsfuerzo();



          console.log("Total de operadores  = " +$scope.SumaContadorOperadores );     //n2
          console.log("Operadores distintos = " +$scope.ContadorOperadoresDistintos );//n1
          console.log("Total de operandos   = " +$scope.SumaContadorOperandos );      //N2
          console.log("Operandos distintos  = " +$scope.ContadorOperandosDistintos ); //N1

          /*console.log("LOC baseOriginal: " + $scope.LOCbaseOriginal);
          console.log("LOC baseActual:   " + $scope.LOCbaseActual);
          console.log("LOC agregado:     " + $scope.LOCagregado);
          console.log("LOC suprimido:    " + $scope.LOCsuprimido);*/
          /*
          console.log("Vocabulario          = " +$scope.vocabulario);
          console.log("longitud             = " +$scope.longitud);
          console.log("volumen              = " +$scope.volumen);
          console.log("volumen potencial    = " +$scope.volumenPotencial);
          console.log("Nivel                = " +$scope.nivel);
          console.log("Dificultad           = " +$scope.dificultad);
          console.log("Inteligencia         = " +$scope.inteligencia);
          console.log("Esfuerzo             = " +$scope.esfuerzo);

          */


        }



        //Vocabulario del programa
        // Número de los diferentes operandos y operadores usados para construir un programa
        // n1 + N1
        function obtenerVocabulario(){
          return $scope.ContadorOperadoresDistintos + $scope.ContadorOperandosDistintos;
        }

        //Logitud del programa
        //La longitud en términos del número total de operadores y operandos utilizados durante la implementación
        // n2 + N2
        function obtenerLongitud(){
          return $scope.SumaContadorOperadores + $scope.SumaContadorOperandos;
        }

        //Volumen del algoritmo
        //Calcula el volumen de un algoritmo.
        // V = N log2 n -> N: longitud, n: vocabulario
        function obtenerVolumen(){
          return $scope.longitud * ( Math.log($scope.vocabulario) / Math.log(2)); // 16.25 con N=7= n=5
        }

        //Volumen potencial
        //Volumen minimo potencial para un programa.
        // V = (n1 + n2) log 2 (n1 +n2)
          //n1: mimnimo potencial de operadores          -> número de diferentes operadores????
          //n2: diferentes parámetros de entrada/salida  -> ???s

        function obtenerVolumenPotencial(){
          return (($scope.ContadorOperadoresDistintos + $scope.SumaContadorOperandos)) * ( Math.log($scope.ContadorOperadoresDistintos + $scope.SumaContadorOperandos) / Math.log(2)); // 16.25 con N=7= n=5
          //return $scope.ContadorOperadoresDistintos + $scope.SumaContadorOperandos;
        }

        //Nivel de programa
        //Da una idea del nivel de detalle con que ha sido codificado.
        //Nivel =(2/n1 )(n2/𝑁_2) -> n1: #operadores distintos, n2: #operandos distintos, N2: total de operandos
        function obtenerNivel(){
          return  (2 / $scope.ContadorOperadoresDistintos) * ($scope.ContadorOperandosDistintos / $scope.SumaContadorOperandos);
        }

        //Dificultad del programa
        //Lo inverso al nivel de programa se le conoce como dificultad de programa.
        //D=1/L -> L: nivel de programa
        function obtenerDificultad(){
          return (1 / $scope.nivel);
        }

        //Inteligencia contenida en el programa
        //Permite estimar la complejidad desde el punto de vista del tiempo de programación y la depuración.
        // I = LV -> L: nivel, V: volumen
        function obtenerInteligencia(){
          return $scope.nivel * $scope.volumen;
        }

        //Esfuerzo
        //E puede ser usado como una medida de la claridad del programa.
        //E=V/L -> V: volumen, L: nivel
        function obtenerEsfuerzo(){
          return $scope.volumen / $scope.nivel;
        }

      });
    }

  $scope.aceChanged = function (_editor) {

  }





  $scope.actTheme = function()
  {
    $scope.editor.theme = $scope.Acethemes[$scope.selectedTheme.id];
  }

  $scope.split = function()
  {
    if ($scope.editor.splitMode)
    {
        $scope.editor.splitMode = false;
        $scope.create();
    }
    else
    {
        $scope.editor.splitMode = true
        $scope.dany();
    }

  }
  $scope.create =function()
  {
    cv = document.getElementById('gfx_holder')
    cv.parentNode.removeChild(cv)
      document.getElementById('preview').style.display="none"
  }
  $scope.dany = function()
  {
    canv = document.createElement('div');
    canv.id="gfx_holder"
    document.getElementById('preview').style.display="block"
    document.querySelector('.editor').appendChild(canv)

    ajaxUML('http://148.220.52.76:8080/diagram/getnotification_dos/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea);
  }
  $scope.letraG = function()
  {
    $scope.editor.fontS = 24;
  }

  $scope.letraN = function()
  {
    $scope.editor.fontS = 16;
  }

  $scope.letraC =  function()
  {
    $scope.editor.fontS = 10;
  }
   $scope.STInfo = function(tokenInfo)
   {
     $scope.$apply(function () {
            $scope.tokenInformation = tokenInfo;
        });
   }

  $scope.saveCode = function()
  {
    var code = $scope.codigo;
    var variablesToSend = {c: $scope.codigo};
    $http.post('http://148.220.52.76:8080/api/savecode/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea+'/'+ String($scope.files[$scope.tab - 1]), variablesToSend ).then(function(response){
       console.log(response);
       $scope.saveMetricas();
       $scope.dany();
       $scope.create();
   }, function(response){
      //console.log(response);
      //sweetAlert("Error", "Ocurrio un problema", "error");
   });
    }
    $scope.init = function()
    {
      var abs = String($location.absUrl());
      var parameters = abs.split('=');
      var usuario = parameters[1].split('&');
        $scope.exp = usuario[0];
      var gpo = parameters[2].split('&');
      $scope.gpo = gpo[0];
      var tarea = parameters[3].split('&');
      $scope.tarea = tarea[0];
      var tema = parameters[4].split('&');
      $scope.currentTheme  = tema[0];
      $scope.getConf();
        $scope.getFiles();
        $scope.blockedit =true;
        $scope.puedeeditar = true;
        $scope.shwAdmin = false;
    }
    $scope.getFiles = function()
    {
        var recieved_data = GetFiles.obtainfiles($scope.exp,$scope.gpo, $scope.tarea);

        recieved_data.then(
            function(fulfillment){
                if(String(fulfillment) != "Ocurrio un problema")
                        {
                            var files = fulfillment;
                            $scope.files = files.split(",");
                            $scope.numFiles = $scope.files.length;
                            $scope.tab=1;
                            $timeout($scope.getConf() , 2000);
                            $scope.getCodes();
                        }
                        else
                        {
                            sweetAlert("Aviso", String(fulfillment), "error");
                        }
            }, function(){
                 //sweetAlert("Error", "Ocurrio un problema", "error");
        });
    }
    $scope.getConf = function()
    {
      var recieved_data = GetFiles.obtainconfig($scope.tarea);

      recieved_data.then(
          function(fulfillment){
              $scope.conf = fulfillment;
              var editor = function() {
                this.theme = $scope.Acethemes[$scope.currentTheme];
                this.mode = 'python';
                this.opacity = 100;
                this.useWrapMode = true;
                this.gutter = true;
                this.splitMode = false;
                this.fontS =16;

                this.snippets = $scope.conf.data.snippets;
                this.bAuto = $scope.conf.data.autocompletado_basico;
                this.lAuto = $scope.conf.data.autocompletado_live;
                this.minL = Infinity;
                this.hihlightAL = $scope.conf.data.highlihtActiveLine;
                this.behaveE = $scope.conf.data.bahaviousEnabled;
                $scope.showToken = $scope.conf.data.infoToken;
              };
              $scope.editor = new editor();
          }, function(){
               //sweetAlert("Error", "Ocurrio un problema", "error");
      });
    }
    $scope.getCodes = function()
    {
        var recieved_code = GetFiles.obtaincode($scope.exp,$scope.gpo, $scope.tarea, String($scope.files[$scope.tab - 1]));
                recieved_code.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Ocurrio un problema")
                        {
                            $scope.codigo = fulfillment;

                        }
                        else
                        {
                            sweetAlert("Aviso", String(fulfillment), "error");
                        }

                    });
    }
    $scope.changeTab = function (value)
    {
        $scope.tab = value;
        var recieved_code = GetFiles.obtaincode($scope.exp,$scope.gpo, $scope.tarea, String($scope.files[$scope.tab - 1]));
                recieved_code.then(
                    function(fulfillment){
                            $scope.codigo = fulfillment;
                    });
    }
    $scope.uptdateCodigo = function (code)
    {
        $scope.codigo = code;
    }
    $scope.nuevoArchivo = function(filename)
    {
        var recieved_data = GetFiles.createfile($scope.exp,$scope.gpo, $scope.tarea, filename);
                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Archivo Creado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }

                    });

        $scope.newFileName ='';
    }
    $scope.eliminarArchivo = function(filename)
    {
        var recieved_data = GetFiles.deletefile($scope.exp,$scope.gpo, $scope.tarea, filename);
                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Archivo Borrado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }

                    });
    }
    $scope.editarNombre = function(fileName)
    {
        $scope.tempFilename = fileName;
    }
    $scope.guardarNombre = function(fileName)
    {
       var recieved_data = GetFiles.renamefile($scope.exp,$scope.gpo, $scope.tarea,  $scope.tempFilename,fileName);

                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Nombre de Archivo Cambiado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }

                    });
        $scope.tempFilename ='';
    }
    $scope.showAdmin = function()
    {
        $scope.shwAdmin = true;
    }
    $scope.SalirAdminProyecto = function()
    {
        $scope.shwAdmin = false;
    }

    $scope.compilar = function ()
    {
      $http.get('http://148.220.52.76:8080/api/getpath/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea).then(function(response){
        var resp =response.data.records;
          var path = resp[0].path;
          $http.post("http://148.220.52.76:8080/compiler/analisis", {dir:path}).then(function(data){
            var resp = data.data;
            console.log(resp[0]);
            document.getElementById("analisis").innerHTML = resp[0].analisis;
            document.getElementById("compilar").innerHTML = resp[0].compilacion;
            $scope.tiempoPruebas = resp[0].tiempo;
            $scope.corregidos = resp[0].errores;
          }, function(data){
            console.log(data);
          });
      }, function(response){
        var resp = "Ocurrio un problema"
        //sweetAlert("Error", "Ocurrio un problema", "error");
      });
    }

    $scope.saveMetricas = function()
  {
    var variablesToSend_aux = [
                            $scope.ContadorOperadoresDistintos,
                            $scope.ContadorOperandosDistintos,
                            $scope.SumaContadorOperadores,
                            $scope.SumaContadorOperandos,
                            $scope.vocabulario,
                            $scope.longitud,
                            $scope.volumen,
                            $scope.volumenPotencial,
                            $scope.nivel,
                            $scope.dificultad,
                            $scope.inteligencia,
                            $scope.esfuerzo,
                            $scope.elapsed,
                            $scope.tiempoPruebas,
                            $scope.tiempoCorreciones,
                            $scope.tiempoTotalProducto,
                            $scope.LOCbaseOriginal,
                            $scope.LOCagregado,
                            $scope.LOCmodificado,
                            $scope.LOCsuprimido,
                            0,
                            0,
                            $scope.LOCbaseActual,
                            $scope.defectos,
                            $scope.defectosCorregidos,
                            $scope.defectosNoCorregidos,
                            $scope.defectosPorTamano,
                            $scope.defectosPorHora,
                            $scope.defectosCorregidosHora,
                            $scope.defectosPorHoraEnPruebas,
                            0,
                            $scope.densidadDeDefectos,
                            0,
                            $scope.complejidadCiclo,
                          ];
    for(i=0; i<variablesToSend_aux.length;i++){
      if(isNaN(variablesToSend_aux[i])){
        variablesToSend_aux[i] = 0;
      }
    }
    var variablesToSend = {
                            "met1": variablesToSend_aux[0],
                            "met2": variablesToSend_aux[1],
                            "met3": variablesToSend_aux[2],
                            "met4": variablesToSend_aux[3],
                            "met5": variablesToSend_aux[4],
                            "met6": variablesToSend_aux[5],
                            "met7": variablesToSend_aux[6],
                            "met8": variablesToSend_aux[7],
                            "met9": variablesToSend_aux[8],
                            "met10": variablesToSend_aux[9],
                            "met11": variablesToSend_aux[10],
                            "met12": variablesToSend_aux[11],
                            "met13": variablesToSend_aux[12],
                            "met14": variablesToSend_aux[13],
                            "met15": variablesToSend_aux[14],
                            "met16": variablesToSend_aux[15],
                            "met17": variablesToSend_aux[16],
                            "met18": variablesToSend_aux[17],
                            "met19": variablesToSend_aux[18],
                            "met20": variablesToSend_aux[19],
                            "met21": variablesToSend_aux[20],
                            "met22": variablesToSend_aux[21],
                            "met23": variablesToSend_aux[22],
                            "met24": variablesToSend_aux[23],
                            "met25": variablesToSend_aux[24],
                            "met26": variablesToSend_aux[25],
                            "met27": variablesToSend_aux[26],
                            "met28": variablesToSend_aux[27],
                            "met29": variablesToSend_aux[28],
                            "met30": variablesToSend_aux[29],
                            "met31": variablesToSend_aux[30],
                            "met32": variablesToSend_aux[31],
                            "met33": variablesToSend_aux[32],
                            "met34": variablesToSend_aux[33],
                          };

    $http.post('http://148.220.52.76:8080/ide/insertmetricas/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea, variablesToSend ).then(function(response){
   }, function(response){
      //sweetAlert("Error", "Ocurrio un problema", "error");
   });
    }

  $scope.entregar = function()
  {
    $scope.saveMetricas();
    $scope.saveCode();
    $http.get('http://148.220.52.76:8080/ide/entregar/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea).then(function(response){
      var resp =response;
    }, function(response){
      var resp = "Ocurrio un problema"
        //sweetAlert("Error", "Ocurrio un problema", "error");
    });

  }
  $scope.salirIde = function()
  {
    $scope.saveMetricas();
    $scope.saveCode();
    $http.get('http://148.220.52.76:8080/ide/salir/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea).then(function(response){
      var resp =response;
      window.close();
    }, function(response){
      var resp = "Ocurrio un problema"
        //sweetAlert("Error", "Ocurrio un problema", "error");
    });

  }

});
