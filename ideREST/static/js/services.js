angular.module('IDE.services', [])
.factory('GetFiles', function($http) {

    var obtainfiles = function(exp,gpo,tarea) {
        return $http.get('http://148.220.52.76:8080/api/getproject/'+exp+'/'+gpo+'/'+tarea).then(function(response){
        	var resp =response.data.records;
       			return String(resp[0].project);
   			}, function(response){
   				var resp = "Ocurrio un problema"
       			return resp;
   			});
    };
    var obtainconfig = function(tarea) {
      return $http.get('http://148.220.52.76:8080/tareas/get_config_ide?idTarea='+tarea).then(function(response){
        return response;
     }, function(response){
        return response;
     });
    };

     var obtaincode = function(exp,gpo,tarea,filename) {
        return $http.get('http://148.220.52.76:8080/api/getfile/'+exp+'/'+gpo+'/'+tarea+'/'+filename).then(function(response){
        	var resp =response.data.records;
        	return String(resp[0].code);
   			}, function(response){
   				var resp = "Ocurrio un problema"
       			return resp;
   			});
    };

     var createfile = function(exp,gpo,tarea,filename) {
        return $http.get('http://148.220.52.76:8080/api/createfile/'+exp+'/'+gpo+'/'+tarea+'/'+filename).then(function(response){
        		return response.data;
   			}, function(response){
       			return response.data;
   			});
    };
    var deletefile = function(exp,gpo,tarea,filename) {
        return $http.get('http://148.220.52.76:8080/api/deletefile/'+exp+'/'+gpo+'/'+tarea+'/'+filename).then(function(response){
        		return response.data;
   			}, function(response){
       			return response.data;
   			});
    };
    var renamefile = function(exp,gpo,tarea,filename,newFileName) {
        return $http.get('http://148.220.52.76:8080/api/renamefile/'+exp+'/'+gpo+'/'+tarea+'/'+filename+'/'+newFileName).then(function(response){
        		return response.data;
   			}, function(response){
       			return response.data;
   			});
    };



    return { obtainfiles: obtainfiles,  obtaincode:obtaincode, createfile: createfile, deletefile: deletefile, renamefile: renamefile, obtainconfig: obtainconfig};
})
.service('BlankService', [function(){

}]);
