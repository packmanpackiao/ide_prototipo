angular.module("IDE", ['ui.ace','IDE.contIde','IDE.services','IDE.directives'])
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
})
.config(function($httpProvider){ 
    $httpProvider.defaults.headers.common['X-CSRFToken'] = 'csrf_token';
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
})
