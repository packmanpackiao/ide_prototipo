class Vehiculo:
    def __init__(self, nombre, color):
        self.__nombre = nombre
        self.__color = color
        
    def getColor(self):
        return self.__color
    
    def setColor(self, color):
        self.__color = color
        
    def getNombre(self):
        return self.__nombre
        
class Auto(Vehiculo):
    def __init__(self, nombre, color, modelo):
        Vehiculo.__init__(self, nombre, color)
        self.__modelo = modelo
        
    def getDescription(self):
        return self.getColor() + self.__modelo
        
    def imprimirAlgo():
        print "Hola que pez"
        
c = Auto('Chevy', 'Azul', 'Chevysad')
print c.getDescription()
print "Hola"